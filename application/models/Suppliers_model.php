<?php

class Suppliers_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function suppliers($supplierID = '') {
        $this->db->select('*');
        $this->db->from('tbl_pos_suppliers');
        $this->db->where('tbl_pos_suppliers.softDelete', 0);
        if (!empty($supplierID)) {
            $this->db->where('tbl_pos_suppliers.supplierID', $supplierID);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function allDue() {
        $this->db->select('sum(tbl_pos_purchases.netTotal) as total_due,tbl_pos_suppliers.supplierID,tbl_pos_suppliers.supplierName,tbl_pos_suppliers.supplierID');
        $this->db->from('tbl_pos_purchases');
        $this->db->join('tbl_pos_suppliers', 'tbl_pos_suppliers.supplierID = tbl_pos_purchases.supplierID', 'left');
        $this->db->where('tbl_pos_suppliers.softDelete', 0);
        $this->db->group_by('tbl_pos_purchases.supplierID');
        $result = $this->db->get()->result_array();
        return $result;
    }


    function allpayment_by_id($supplier_id = null) {
        $this->db->select('sum(tbl_pos_supplier_payment.amount) as total_payment');
        $this->db->from('tbl_pos_supplier_payment');
        $this->db->where('tbl_pos_supplier_payment.supplier_id', $supplier_id);
        $result = $this->db->get()->row_array();
        return $result['total_payment'];
    }

    function allDue_by_id($supplier_id = null) {
        $this->db->select('sum(tbl_pos_purchases.netTotal) as total_due');
        $this->db->from('tbl_pos_purchases');
        $this->db->where('tbl_pos_purchases.supplierID', $supplier_id);
        $result = $this->db->get()->row_array();
        return $result['total_due'];
    }

    function store($data) {
        $this->db->insert('tbl_pos_suppliers', array(
            'supplierName' => $data['supplierName'],
            'supplierEmail' => $data['supplierEmail'],
            'supplierPhone' => $data['supplierPhone'],
            'supplierAddress' => $data['supplierAddress'],
        ));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
    }

    function destroy($supplierID) {
        $this->db->where('supplierID', $supplierID);
        $this->db->update('tbl_pos_suppliers', array(
            'softDelete' => 1,
        ));
        return TRUE;
    }

    function update($data) {
        $this->db->where('supplierID', $data['supplierID']);
        $this->db->update('tbl_pos_suppliers', array(
            'supplierName' => $data['supplierName'],
            'supplierEmail' => $data['supplierEmail'],
            'supplierPhone' => $data['supplierPhone'],
            'supplierAddress' => $data['supplierAddress'],
        ));
        return TRUE;
    }

}
