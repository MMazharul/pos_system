<?php
class Pos_model extends CI_Model {

    function store($invoiceNo, $data) {
        $sales_persion = $this->session->userdata('user_id');

        if ($data['r1'] == 1) {
            $this->db->insert('tbl_pos_sales', array(
                'invoiceNo' => $invoiceNo,
                'warehouseID' => $data['warehouseID'][0],
                'invoiceDate' => $data['invoiceDate'],
                'salesDate' => $data['saleDate'],
                'subTotal' => $data['subTotal'],
                'percentage' => $data['vatPertan'],
                'vat' => $data['vat'],
                'grandTotal' => $data['subTotal'] + $data['vat'],
                'discount' => $data['discount'],
                'netTotal' => $data['totalAmount'],
                'posPaying' => $data['totalPaying'],
                'posChange' => $data['balance'],
                'sales_persion' => $sales_persion,
                'note' => $data['note'],
                //note option ti update kora hoise
            ));
        } else {
            $this->db->insert('tbl_pos_sales', array(
                'invoiceNo' => $invoiceNo,
                'warehouseID' => $data['warehouseID'][0],
                'invoiceDate' => $data['invoiceDate'],
                'salesDate' => $data['saleDate'],
                'subTotal' => $data['subTotal'],
                'percentage' => $data['vatPertan'],
                'vat' => $data['vat'],
                'grandTotal' => $data['subTotal'] + $data['vat'],
                'discount' => $data['discount'],
                'netTotal' => $data['ttamountCard'],
                'posPaying' => $data['totalAmount_forCard'],
                'CardNum' => $data['CardNum'],
                'percentage' => $data['extra'],
                'posChange' => $data['balance'],
                'sales_persion' => $sales_persion
            ));
        }
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
    }
    public function cron_store($data)
    {
        $this->db->insert('cron',$data);
    }

    function storePosProduct($data) {
        $this->db->insert('tbl_pos_sale_products', array(
            'invoiceNo' => $data['invoiceNo'],
            'productID' => $data['productID'],
            'quantity' => $data['quantity'],
            'price' => $data['price'],
        ));
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function increaseIncome($invoiceNo, $values, $accountID, $data) {
        if (!empty($values)) {
            $this->db->insert('tbl_pos_transactions', array(
                'refNo' => $invoiceNo,
                'transactionAccountID' => 4,
                'transactionType' => 'SALE',
                'transactionAmount' => $data['totalAmount'],
                'date' => date("Y-m-d"),
            ));
        } else {
            $this->db->insert('tbl_pos_transactions', array(
                'refNo' => $invoiceNo,
                'transactionAccountID' => $accountID,
                'transactionType' => 'SALE',
                'transactionAmount' => $data['totalAmount'],
                'date' => date("Y-m-d"),
            ));
        }



        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    public function increaseIncomeUpdate($invoiceNo,$transactionAmmount)
    {

      $this->db->where('refNo',$invoiceNo);
      $update = $this->db->update('tbl_pos_transactions', array('transactionAmount' => $transactionAmmount));
      return $update;
    }

    function productcatagories($warehouseID) {
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->select('*');
        $this->db->where('tbl_pos_inventory.warehouseID', $warehouseID);
        $this->db->group_by('tbl_pos_product_catagories.catagoryName');
        $this->db->from('tbl_pos_inventory');
        $this->db->join('tbl_pos_products', 'tbl_pos_inventory.productID = tbl_pos_products.productID', 'left');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function productInventoryWarehouseSuggestion($q = '') {
      $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->select('tbl_pos_products.* , tbl_pos_inventory.warehouseID , SUM(tbl_pos_inventory.quantity) as inventory,tbl_pos_purchase_products.purchasePrice');
       // $this->db->where('tbl_pos_inventory.warehouseID', $warehouseID);
        $this->db->group_by('tbl_pos_inventory.productID');
        $this->db->from('tbl_pos_inventory');
        $this->db->join('tbl_pos_products', 'tbl_pos_inventory.productID = tbl_pos_products.productID', 'left');
        $this->db->join('tbl_pos_purchase_products', 'tbl_pos_inventory.productID = tbl_pos_purchase_products.productID');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID', 'left');
        $this->db->like('tbl_pos_products.productName', $q);
        $this->db->or_like('tbl_pos_products.productCode', $q);
        $this->db->order_by("tbl_pos_products.productID","ASC");
        $this->db->limit("30");
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        if (count($result) > 0) {
            $data = array();
            foreach ($result as $key => $value) {
                $data[$key]['id'] = $result[$key]['productID'];
                $data[$key]['warehouseID'] = $result[$key]['warehouseID'];
                $data[$key]['inventory'] = $result[$key]['inventory'];
                $data[$key]['productCode'] = $result[$key]['productCode'];
                $data[$key]['productName'] = $result[$key]['productName'];
                $data[$key]['productPrice'] = $result[$key]['productPrice'];
                $data[$key]['productWholeSalePrice'] = $result[$key]['productWholeSalePrice'];
                $data[$key]['value'] = $result[$key]['productName'] . ' (' . $result[$key]['productCode'] . ')';
                $data[$key]['productModel'] = ($result[$key]['productModel']!='')? $result[$key]['productModel']:'' ;
                $data[$key]['purchasePrice'] = ($result[$key]['purchasePrice']!='')? $result[$key]['purchasePrice']:'' ;
            }
            return $data;
        }
    }
    function productInventoryWarehouseSuggestionScreener($q = '') {
      $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->select('tbl_pos_products.* , tbl_pos_inventory.warehouseID , SUM(tbl_pos_inventory.quantity) as inventory,tbl_pos_purchase_products.purchasePrice');

        $this->db->group_by('tbl_pos_inventory.productID');
        $this->db->from('tbl_pos_inventory');
        $this->db->join('tbl_pos_products', 'tbl_pos_inventory.productID = tbl_pos_products.productID', 'left');
        $this->db->join('tbl_pos_purchase_products', 'tbl_pos_inventory.productID = tbl_pos_purchase_products.productID');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID', 'left');

        $this->db->where('tbl_pos_products.productCode', $q);
        $this->db->order_by("tbl_pos_products.productID","ASC");
        $this->db->limit("1");
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        if (count($result) > 0) {
            $data = array();
            foreach ($result as $key => $value) {
                $data[$key]['id'] = $result[$key]['productID'];
                $data[$key]['warehouseID'] = $result[$key]['warehouseID'];
                $data[$key]['inventory'] = $result[$key]['inventory'];
                $data[$key]['productCode'] = $result[$key]['productCode'];
                $data[$key]['productName'] = $result[$key]['productName'];
                $data[$key]['productPrice'] = $result[$key]['productPrice'];
                $data[$key]['productWholeSalePrice'] = $result[$key]['productWholeSalePrice'];
                $data[$key]['value'] = $result[$key]['productName'] . ' (' . $result[$key]['productCode'] . ')';
                $data[$key]['productModel'] = ($result[$key]['productModel']!='')? $result[$key]['productModel']:'' ;
                 $data[$key]['purchasePrice'] = ($result[$key]['purchasePrice']!='')? $result[$key]['purchasePrice']:'' ;
            }
            return $data;
        }
    }

    function updateSaleByReturningProduct($data) {
        $this->db->select('tbl_pos_sales.dueAmount');
        $this->db->where('tbl_pos_sales.invoiceNo', $data['invoiceNo']);
        $this->db->from('tbl_pos_sales');
        $result = $this->db->get()->result();

        $return_persion = $this->session->userdata('user_id');

        if ($result[0]->dueAmount > 0) {
            $this->db->where('invoiceNo', $data['invoiceNo']);
            $this->db->update('tbl_pos_sales', array(
                'subTotal' => $data['subTotal'],
                'vat' => $data['vat'],
                'grandTotal' => $data['grandTotal'],
                'discount' => $data['discount'],
                'netTotal' => $data['netTotal'],
                'dueAmount' => $result[0]->dueAmount - $data['returnValue'],
                'returnValue' => $data['returnValue'],
                'returnCharge' => $data['returnCharge'],
                'return_persion' => $return_persion,
                'returnDate' =>date("Y-m-d")
            ));
        } else {
            $this->db->where('invoiceNo', $data['invoiceNo']);
            $this->db->update('tbl_pos_sales', array(
                'subTotal' => $data['subTotal'],
                'vat' => $data['vat'],
                'grandTotal' => $data['grandTotal'],
                'discount' => $data['discount'],
                'netTotal' => $data['netTotal'],
                'returnValue' => $data['returnValue'],
                'returnCharge' => $data['returnCharge'],
                'return_persion' => $return_persion,
                'returnDate' =>date("Y-m-d")
            ));
        }
        return TRUE;
    }

    function updateSaleProductByReturningProduct($data) {
        $this->db->select('tbl_pos_sale_products.quantity');
        $this->db->where('tbl_pos_sale_products.invoiceNo', $data['invoiceNo']);
        $this->db->where('tbl_pos_sale_products.productID', $data['productID']);
        $this->db->from('tbl_pos_sale_products');
        $result = $this->db->get()->result();

        $this->db->where('tbl_pos_sale_products.invoiceNo', $data['invoiceNo']);
        $this->db->where('tbl_pos_sale_products.productID', $data['productID']);
        $data = array('quantity'=>$result[0]->quantity - $data['quantity'], "returnDate"=>date("Y-m-d"), "returnQuantity"=>$data['quantity']);
        $this->db->update('tbl_pos_sale_products', $data);
        return TRUE;
    }

    function increaseInventoryByReturningProduct($data) {
        $this->db->insert('tbl_pos_inventory', array(
            'refNo' => $data['invoiceNo'],
            'warehouseID' => $data['warehouseID'],
            'productID' => $data['productID'],
            'quantity' => $data['quantity'],
            'type' => 'RETURN-IN',
        ));
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function deductIncomeByReturningProduct($data) {
        $this->db->select('tbl_pos_transactions.transactionAccountID');
        $this->db->where('tbl_pos_transactions.refNo', $data['invoiceNo']);
        $this->db->where('tbl_pos_transactions.transactionType', 'SALE');
        $this->db->from('tbl_pos_transactions');
        $query_result = $this->db->get();
        $result = $query_result->result();
        if ($result[0]->transactionAccountID) {
            $this->db->insert('tbl_pos_transactions', array(
                'refNo' => $data['invoiceNo'],
                'transactionAccountID' => $result[0]->transactionAccountID,
                'transactionType' => 'SALE-RETURN',
                'transactionAmount' => '-' . $data['returnValue'],
                'date' => date("Y-m-d"),
            ));
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return true;
        }
    }

}
