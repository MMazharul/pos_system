<?php
class Purchases_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function purchases($purchaseNo='',$due=1)
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_purchases');
        $this->db->join('tbl_pos_suppliers', 'tbl_pos_purchases.supplierID = tbl_pos_suppliers.supplierID','left');
        $this->db->join('tbl_pos_warehouses', 'tbl_pos_purchases.warehouseID = tbl_pos_warehouses.warehouseID','left');
        $this->db->where('tbl_pos_purchases.softDelete',0);
        if(!empty($purchaseNo)){
            $this->db->where('tbl_pos_purchases.purchaseNo',$purchaseNo);
        }
        if ($due == 0) {
            $this->db->where('tbl_pos_purchases.dueAmount > ',$due);
        }
        $this->db->order_by("tbl_pos_purchases.purchaseID","DESC");
        $query_result = $this->db->get();
        $result = $query_result->result();

        foreach ($result as $key => $value) {
            $result[$key]->products = $this->purchasedProductList($value->purchaseNo);
        }

        return $result;
    }

    private function purchasedProductList($purchaseNo)
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->where('tbl_pos_purchase_products.purchaseNo',$purchaseNo);
        $this->db->join('tbl_pos_products', 'tbl_pos_purchase_products.productID = tbl_pos_products.productID','left');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID','left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function generateRandomString() {
            $characters = '0123456789';
            $charactersLength = strlen($characters);
            $randomString = '';
            $length='6';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
           $pos = 'PUR-' . $randomString;
        $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_purchases', 'purchaseNo', $pos);
        if($this->db->affected_rows()>0){
            $this->generateRandomString();
        }else{
            return $pos;
        }

    }

    function store($data)
    {
        $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_purchases', 'purchaseNo', $data['purchaseNo']);
        $affRow=$this->db->affected_rows();

        if($this->db->affected_rows() > 0){
            $purchaseNoNew=$this->generateRandomString();
            $check="Find";
        }else{
            $purchaseNoNew=$data['purchaseNo'];
            $check="Not Find";
        }
//        return $affRow."No:-".$purchaseNoNew." checking: ".$check;

        $purchase_persion = $this->session->userdata('user_id');

        $this->db->insert('tbl_pos_purchases', array(
            'purchaseNo'=>$purchaseNoNew,
            'poRef'=>$data['poRef'],
            'payTerms'=>$data['payTerms'],
            'purchaseDate'=>$data['purchaseDate'],
            'dueDate'=>$data['dueDate'],
            'subTotal'=>$data['subTotal'],
            'discount'=>$data['discount'],
            'netTotal'=>$data['netTotal'],
            'dueAmount'=>$data['dueAmount'],
            'note'=>$data['note'],
            'supplierID'=>$data['supplierID'],
            'warehouseID'=>$data['warehouseID'],
            'purchase_persion'=>$purchase_persion
        ));

        if($this->db->affected_rows() > 0){
            return $purchaseNoNew;
        }
    }

    function storeProduct($data)
    {
        $this->db->insert('tbl_pos_purchase_products', array(
            'purchaseNo'=>$data['purchaseNo'],
            'productID'=>$data['productID'],
            'quantity'=>$data['quantity'],
            'purchasePrice'=>$data['purchasePrice'],
        ));

        if($this->db->affected_rows() > 0){
            return true;
        }
    }

    function storeInventory($data, $warehouseID, $date)
    {
        $this->db->insert('tbl_pos_inventory', array(
            'refNo'=>$data['purchaseNo'],
            'warehouseID'=>$warehouseID,
            'productID'=>$data['productID'],
            'quantity'=>$data['quantity'],
            'type'=>'IN',
            'date' => $date,
        ));

        if($this->db->affected_rows() > 0){
            return true;
        }
    }

    function supplierSuggestion($q)
    {
        $this->db->select('*');
        $this->db->like('tbl_pos_suppliers.supplierName', $q);
        $this->db->where('tbl_pos_suppliers.softDelete', 0);

        $this->db->from('tbl_pos_suppliers');
        $query = $this->db->get();
        $result = $query->result_array();
        if(count($result) > 0){
            $data = array();
            foreach ($result as $key => $value) {
                $data[$key]['id'] = $result[$key]['supplierID'] ;
                $data[$key]['value'] = $result[$key]['supplierName'] ;
            }
            return $data;
        }
    }

    function warehouseSuggestion($q)
    {
        $this->db->select('*');
        $this->db->like('tbl_pos_warehouses.warehouseName', $q);
        $this->db->where('tbl_pos_warehouses.softDelete', 0);

        $this->db->from('tbl_pos_warehouses');
        $query = $this->db->get();
        $result = $query->result_array();
        if(count($result) > 0){
            $data = array();
            foreach ($result as $key => $value) {
                $data[$key]['id'] = $result[$key]['warehouseID'] ;
                $data[$key]['value'] = $result[$key]['warehouseName'] ;
            }
            return $data;
        }
    }
}
