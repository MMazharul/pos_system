<?php
class Sales_model extends CI_Model {

    function sales($invoiceNo='',$salesType='')
    {
        $this->db->select('*');

        if(!empty($invoiceNo)){
            $this->db->where('tbl_pos_sales.invoiceNo', $invoiceNo);
        }

        if(!empty($salesType)){
            $this->db->where('tbl_pos_sales.clientID', 0);
        }else{
            $this->db->where('tbl_pos_sales.clientID >', 0);
        }

        $this->db->from('tbl_pos_sales');
        $this->db->join('tbl_pos_clients', 'tbl_pos_sales.clientID = tbl_pos_clients.clientID','left');
        $this->db->join('tbl_pos_warehouses', 'tbl_pos_sales.warehouseID = tbl_pos_warehouses.warehouseID','left');
        $this->db->join('tbl_pos_transactions', 'tbl_pos_sales.invoiceNo = tbl_pos_transactions.refNo','left');
        $this->db->order_by('tbl_pos_sales.salesDate DESC');
        $query = $this->db->get();
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->products = $this->salesProduct($value->invoiceNo);
        }

        return $result;
    }
    function salesNew($invoiceNo='',$salesType='',$per_page,$start_limit)
    {
        $this->db->select('*');

        if(!empty($invoiceNo)){
            $this->db->where('tbl_pos_sales.invoiceNo', $invoiceNo);
        }

        if(!empty($salesType)){
            $this->db->where('tbl_pos_sales.clientID', 0);
        }else{
            $this->db->where('tbl_pos_sales.clientID >', 0);
        }

        $this->db->from('tbl_pos_sales');
        $this->db->join('tbl_pos_clients', 'tbl_pos_sales.clientID = tbl_pos_clients.clientID','left');
        $this->db->join('tbl_pos_warehouses', 'tbl_pos_sales.warehouseID = tbl_pos_warehouses.warehouseID','left');
        $this->db->join('tbl_pos_transactions', 'tbl_pos_sales.invoiceNo = tbl_pos_transactions.refNo','left');
        $this->db->order_by('tbl_pos_sales.salesDate DESC');
        if ($start_limit!='') {
            $this->db->limit( $start_limit,$per_page);
        }
        $query = $this->db->get();
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->products = $this->salesProduct($value->invoiceNo);
        }

        return $result;
    }function salesNewSearch($per_page,$start_limit,$searchingData)
    {
        $this->db->select('*');

        if(!empty($invoiceNo)){
            $this->db->where('tbl_pos_sales.invoiceNo', $invoiceNo);
        }


        if($searchingData!=''){
            $this->db->like('tbl_pos_sales.invoiceNo ', "%".$searchingData."%");

        }

        $this->db->from('tbl_pos_sales');
        $this->db->join('tbl_pos_clients', 'tbl_pos_sales.clientID = tbl_pos_clients.clientID','left');
        $this->db->join('tbl_pos_warehouses', 'tbl_pos_sales.warehouseID = tbl_pos_warehouses.warehouseID','left');
        $this->db->join('tbl_pos_transactions', 'tbl_pos_sales.invoiceNo = tbl_pos_transactions.refNo','left');
        $this->db->order_by('tbl_pos_sales.salesDate DESC');
        if ($start_limit!='') {
            $this->db->limit( $start_limit,$per_page);
        }
        $query = $this->db->get();
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->products = $this->salesProduct($value->invoiceNo);
        }

        return $result;
    }

    function salesDate($invoiceNo='',$salesType='',$first,$last,$per_page,$start_limit)
    {
        $this->db->select('*');

        if(!empty($invoiceNo)){
            $this->db->where('tbl_pos_sales.invoiceNo', $invoiceNo);
        }

        if(!empty($salesType)){
            $this->db->where('tbl_pos_sales.clientID', 0);
        }else{
            $this->db->where('tbl_pos_sales.clientID >', 0);
        }
        $this->db->where("tbl_pos_sales.salesDate >=",$first);
        $this->db->where("tbl_pos_sales.salesDate <=",$last);

        $this->db->from('tbl_pos_sales');
        $this->db->join('tbl_pos_clients', 'tbl_pos_sales.clientID = tbl_pos_clients.clientID','left');
        $this->db->join('tbl_pos_warehouses', 'tbl_pos_sales.warehouseID = tbl_pos_warehouses.warehouseID','left');
        $this->db->join('tbl_pos_transactions', 'tbl_pos_sales.invoiceNo = tbl_pos_transactions.refNo','left');
        if ($start_limit!='') {
            $this->db->limit( $start_limit,$per_page);
        }
        $this->db->order_by('tbl_pos_sales.salesDate DESC');
        $query = $this->db->get();
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->products = $this->salesProduct($value->invoiceNo);
        }

        return $result;
    }

    private function salesProduct($invoiceNo)
    {
        $this->db->select('*');
        $this->db->where('tbl_pos_sale_products.invoiceNo', $invoiceNo);
        $this->db->from('tbl_pos_sale_products');
        $this->db->join('tbl_pos_products', 'tbl_pos_sale_products.productID = tbl_pos_products.productID','left');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID','left');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function store($data)
    {
        $this->db->insert('tbl_pos_sales', array(
            'invoiceNo'=>$data['invoiceNo'],
            'clientID'=>$data['clientID'],
            'warehouseID'=>$data['warehouseID'],
            'invoiceDate'=>$data['invoiceDate'],
            'salesDate'=>$data['salesDate'],
            'poRef'=>$data['poRef'],
            'payTerms'=>$data['payTerms'],
            'deliveryPlace'=>$data['deliveryPlace'],
            'subTotal'=>$data['subTotal'],
            'vat'=>$data['vat'],
            'grandTotal'=>$data['grandTotal'],
            'discount'=>$data['discount'],
            'netTotal'=>$data['netTotal'],
            'dueAmount'=>$data['netTotal'],
        ));

        if($this->db->affected_rows() > 0){
            return true;
        }
    }

    function storeSalesProduct($data, $salesDate)
    {
        $this->db->insert('tbl_pos_sale_products', array(
            'invoiceNo'=>$data['invoiceNo'],
            'productID'=>$data['productID'],
            'quantity'=>$data['quantity'],
            'quantity'=>$data['quantity'],
            'price'=>$data['price'],
            'salesDate' =>$salesDate
        ));

        if($this->db->affected_rows() > 0){
            return true;
        }
    }

    public function purchasePriceStore($data)
    {
      $this->db->insert('tbl_pos_purchase_products', array(

          'productID'=>$data['productID'],
          'purchasePrice'=>$data['purchasePrice'],
      ));

      if($this->db->affected_rows() > 0){
          return true;
      }
    }

    public function purchasePriceUpdate($data,$totalPurchasePrice)
    {
      $totalPrice = $data['quantity'] * $data['purchasePrice'];
      $originialPrice = $totalPurchasePrice - $totalPrice;

      $this->db->where('productID', $data['productID']);
      $this->db->update('tbl_pos_purchase_products', array(
          'purchasePrice'=>$data['purchasePrice'],
          'total_price'=>$originialPrice
      ));
      return TRUE;
    }
    public function purchaseExist($id)
    {
      $this->db->select('*');
      $this->db->where('productID',$id);
      $this->db->from('tbl_pos_purchase_products');
      $query = $this->db->get();
      $result = $query->result_array();
      return $result;
    }

    function deductInventory($data)
      {
          $this->db->insert('tbl_pos_inventory', array(
              'refNo'=>$data['invoiceNo'],
              'warehouseID'=>$data['warehouseID'],
              'productID'=>$data['productID'],
              'quantity'=>'-'.$data['quantity'],
              'purchasePrice'=>$data['purchasePrice'],
              'type'=>'OUT',
              'date' => date("Y-m-d")
          ));

          if($this->db->affected_rows() > 0){
              return true;
          }
      }

    function suggestionClient($q)
    {
        $this->db->select('*');
        $this->db->like('tbl_pos_clients.clientName', $q);
        $this->db->where('tbl_pos_clients.softDelete', 0);

        $this->db->from('tbl_pos_clients');
        $query = $this->db->get();
        $result = $query->result_array();
        if(count($result) > 0){
            $data = array();
            foreach ($result as $key => $value) {
                $data[$key]['id'] = $result[$key]['clientID'] ;
                $data[$key]['value'] = $result[$key]['clientName'] ;
            }
            return $data;
        }
    }

    function suggestionWarehouse($q)
    {
        $this->db->select('*');
        $this->db->like('tbl_pos_warehouses.warehouseName', $q);
        $this->db->where('tbl_pos_warehouses.softDelete', 0);

        $this->db->from('tbl_pos_warehouses');
        $query = $this->db->get();
        $result = $query->result_array();
        if(count($result) > 0){
            $data = array();
            foreach ($result as $key => $value) {
                $data[$key]['id'] = $result[$key]['warehouseID'] ;
                $data[$key]['value'] = $result[$key]['warehouseName'] ;
            }
            return $data;
        }
    }

    function dueSalePaymentHistory($invoiceNo)
    {
        $this->db->select('*');
        $this->db->where('tbl_pos_transactions.refNo', $invoiceNo);
        $this->db->from('tbl_pos_transactions');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function count_all_data($table_name) {
        return $this->db->count_all($table_name);
    }
    function get_pagination(array $paginate) {
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = $paginate['url'];
        $config["total_rows"] = $this->count_all_data($paginate['table']);
        $config["uri_segment"] = 3;
        $config["per_page"] = 50;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li disabled class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["result"] = $paginate['fetchData'];
        $data["links"] = $this->pagination->create_links();
        return $data;
    }


}
