<?php
class Settings_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }    
   
    function storeProductCatagory($data)
    {
        $this->db->insert('tbl_pos_product_catagories', $data);
        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
    }

    function productcatagories($product_catagoriesID = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_product_catagories');
        $this->db->where('tbl_pos_product_catagories.softDelete',0);
        if (!empty($product_catagoriesID)) {
            $this->db->where('tbl_pos_product_catagories.product_catagoriesID',$product_catagoriesID);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    
    function expensehead()
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_expense_head');
        $this->db->where('softDelete',0);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    
    function account()
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_accounts');
        $this->db->where('softDelete',0);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    
    function destroyProductCatagory($product_catagoriesID)
    {
        $this->db->where('product_catagoriesID', $product_catagoriesID);
        $this->db->update('tbl_pos_product_catagories', array(
            'softDelete'=>1,
        ));
        return TRUE;
    }

    function updateProductCatagory($data)
    {
        $this->db->where('product_catagoriesID', $data['product_catagoriesID']);
        $this->db->update('tbl_pos_product_catagories', array(
            'catagoryName'=>$data['catagoryName'],
        ));
        return TRUE;
    }

    function warehouses($warehouseID = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_warehouses');
        $this->db->where('tbl_pos_warehouses.softDelete',0);
        if (!empty($warehouseID)) {
            $this->db->where('tbl_pos_warehouses.warehouseID',$warehouseID);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function storeWarehouse($data)
    {
        $this->db->insert('tbl_pos_warehouses', array(
            'warehouseName'=>$data['warehouseName'],
            'warehouseAddress'=>$data['warehouseAddress'],
            'warehousePhone'=>$data['warehousePhone'],
            ));
        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
    }

    function destroyWarehouse($warehouseID)
    {
        $this->db->where('warehouseID', $warehouseID);
        $this->db->update('tbl_pos_warehouses', array(
            'softDelete'=>1,
        ));
        return TRUE;
    }

    function updateWarehouse($data)
    {
        $this->db->where('warehouseID', $data['warehouseID']);
        $this->db->update('tbl_pos_warehouses', array(
            'warehouseName'=>$data['warehouseName'],
            'warehouseAddress'=>$data['warehouseAddress'],
            'warehousePhone'=>$data['warehousePhone'],
        ));
        return TRUE;
    }

    function config()
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_config');
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    function updateVatRate($data)
    {
        $this->db->where('configID', $data['configID']);
        $this->db->update('tbl_pos_config', array(
            'vatRate'=>$data['vatRate'],
        ));
        return TRUE;
    }

    function posConfig()
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_config_pos');
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    function updatePosConfig($data)
    {
        $this->db->where('configPosID', $data['configPosID']);
        $this->db->update('tbl_pos_config_pos', array(
            'warehouseID'=>$data['warehouseID'],
            'accountID'=>$data['accountID'],
        ));
        return TRUE;
    }
}