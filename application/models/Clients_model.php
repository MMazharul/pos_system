<?php
class Clients_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function clients($clientID='')
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_clients');
        $this->db->where('tbl_pos_clients.softDelete',0);
        if (!empty($supplierID)) {
            $this->db->where('tbl_pos_clients.clientID',$clientID);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function store($data)
    {
        $this->db->insert('tbl_pos_clients', array(
            'clientName'=>$data['clientName'],
            'clientEmail'=>$data['clientEmail'],
            'clientPhone'=>$data['clientPhone'],
            'clientAddress'=>$data['clientAddress'],
            ));
        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
    }

    function destroy($clientID)
    {
        $this->db->where('clientID', $clientID);
        $this->db->update('tbl_pos_clients', array(
            'softDelete'=>1,
        ));
        return TRUE;
    }

    function update($data)
    {
        $this->db->where('clientID', $data['clientID']);
        $this->db->update('tbl_pos_clients', array(
            'clientName'=>$data['clientName'],
            'clientEmail'=>$data['clientEmail'],
            'clientPhone'=>$data['clientPhone'],
            'clientAddress'=>$data['clientAddress'],
        ));
        return TRUE;
    }
}