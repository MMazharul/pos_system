<?php
class Transfers_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }  

    function transfers($transferNo='')
     {
        $this->db->select('*');
        $this->db->from('tbl_pos_transfers');

        if(!empty($transferNo)){
            $this->db->where('transferNo',$transferNo);
        }

        $query_result = $this->db->get();
        $result = $query_result->result();
        foreach ($result as $key => $value) {
            $result[$key]->products = $this->transferProducts($value->transferNo);
            $result[$key]->fromWarehouseInfo = $this->askWarehouseInfo($value->fromWarehouseID);
            $result[$key]->toWarehouseInfo = $this->askWarehouseInfo($value->toWarehouseID);
        }
        return $result;
     } 

    private function transferProducts($transferNo)
    {
        $this->db->select('*');
        $this->db->where('transferNo',$transferNo);
        $this->db->from('tbl_pos_transfer_products');
        $this->db->join('tbl_pos_products', 'tbl_pos_transfer_products.productID = tbl_pos_products.productID','left');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID','left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    private function askWarehouseInfo($warehouseID)
    {
        $this->db->select('*');
        $this->db->where('warehouseID',$warehouseID);
        $this->db->from('tbl_pos_warehouses');
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
	   
    function store($data)
    {
    	$this->db->insert('tbl_pos_transfers', array(
            'transferDate'=>$data['transferDate'],
            'transferNo'=>$data['transferNo'],
            'fromWarehouseID'=>$data['selectedFromWareHouse'],
            'toWarehouseID'=>$data['selectedToWareHouse'],
            'note'=>$data['note'],
        ));

        if($this->db->affected_rows() > 0){
            return true;
        }
    }

    function storeTransferProduct($data)
    {
    	$this->db->insert('tbl_pos_transfer_products', array(
            'transferNo'=>$data['transferNo'],
            'productID'=>$data['productID'],
            'quantity'=>$data['quantity'],
        ));

        if($this->db->affected_rows() > 0){
            return true;
        }
    }

    function storeTransferedInventory($data,$selectedFromWareHouse,$selectedToWareHouse)
    {
    	$this->db->insert('tbl_pos_inventory', array(
            'refNo'=>$data['transferNo'],
            'productID'=>$data['productID'],
            'quantity'=>'-'.$data['quantity'],
            'type'=>'TRANSFER-OUT',
            'warehouseID' => $selectedFromWareHouse
        ));

        if($this->db->affected_rows() > 0){
         	$this->db->insert('tbl_pos_inventory', array(
	            'refNo'=>$data['transferNo'],
	            'productID'=>$data['productID'],
	            'quantity'=>$data['quantity'],
	            'type'=>'TRANSFER-IN',
	            'warehouseID' => $selectedToWareHouse
	        ));

	        if($this->db->affected_rows() > 0){
	            return true;
	        }   
        }
    }
}