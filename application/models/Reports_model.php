<?php

class Reports_model extends CI_Model {

    //monthly summary report

    function monthly_purchases_avg_price($date) {
        $this->db->select('AVG(tbl_pos_purchase_products.purchasePrice) as totalAvgPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate) <=', $date);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_purchases_avg_price_new($date) {
        $data = [];
        $results = $this->db->select('AVG(pp.purchasePrice) avg_price, pp.productID')
            ->from('tbl_pos_purchase_products pp')
            ->join('tbl_pos_purchases p', 'p.purchaseNo = pp.purchaseNo', 'inner')
            ->where('MONTH(p.purchaseDate) <=', $date)
            ->where('YEAR(p.purchaseDate) <=', date('Y'))
            ->where('p.warehouseID', 4)
            ->group_by('pp.productID')
            ->get()->result_array();
        $data['avg_price_purchase'] = [];
        foreach($results as $result)
        {
            $data['avg_price_purchase'][$result['productID']] = $result['avg_price'];
        }

        $results = $this->db->select('AVG(price) avg_price, productID')
            ->from('tbl_pos_inventory')
            ->where('MONTH(date) <=', $date)
            ->where('YEAR(date) <=', date('Y'))
            ->where('refNo', '')
            ->where('type', 'IN')
            ->group_by('productID')
            ->get()->result_array();
        $data['avg_price_inventory'] = [];
        foreach($results as $result)
        {
            $data['avg_price_inventory'][$result['productID']] = $result['avg_price'];
        }

        return $data;
    }

    function monthly_account_oppening_summary($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount,tbl_pos_transactions.transactionAccountID');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('MONTH(tbl_pos_transactions.date) <', $date);
        $this->db->where('YEAR(tbl_pos_transactions.date) <', date('Y'));
        $this->db->group_by('tbl_pos_transactions.transactionAccountID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function monthly_account_closing_summary($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount,tbl_pos_transactions.transactionAccountID');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('MONTH(tbl_pos_transactions.date) <=', $date);
        $this->db->where('YEAR(tbl_pos_transactions.date) <=', date('Y'));
        $this->db->group_by('tbl_pos_transactions.transactionAccountID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function monthly_sales_oppening_summary($date) {
        $this->db->select('SUM(tbl_pos_sale_products.quantity) as total_sales_qty,SUM(tbl_pos_sale_products.price) AS total_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('MONTH(tbl_pos_sale_products.salesDate) <', $date);
        $this->db->where('YEAR(tbl_pos_sale_products.salesDate) <', date('Y'));
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_sales_closing_summary($date) {
        $this->db->select('SUM(tbl_pos_sale_products.quantity) as total_sales_qty,SUM(tbl_pos_sale_products.price) AS total_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('MONTH(tbl_pos_sale_products.salesDate)', $date);
        $this->db->where('YEAR(tbl_pos_sale_products.salesDate)', date('Y'));
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_expense_oppening_summary($date) {
        $this->db->select('tbl_pos_expense.head_id,SUM(tbl_pos_expense.amount) as total_expense');
        $this->db->from('tbl_pos_expense');
        $this->db->where('MONTH(date(tbl_pos_expense.created_at)) <', $date);
        $this->db->where('YEAR(date(tbl_pos_expense.created_at)) <', date('Y'));
        $this->db->group_by('tbl_pos_expense.head_id');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function monthly_expense_closing_summary($date) {
        $this->db->select('tbl_pos_expense.head_id,SUM(tbl_pos_expense.amount) as total_expense');
        $this->db->from('tbl_pos_expense');
        $this->db->where('MONTH(date(tbl_pos_expense.created_at))', $date);
        $this->db->where('YEAR(date(tbl_pos_expense.created_at))', date('Y'));

        $this->db->group_by('tbl_pos_expense.head_id');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function monthly_purchases_oppening_summary($date) {
        $this->db->select('tbl_pos_purchases.supplierID,SUM(tbl_pos_purchases.netTotal) as total_netTotal');
        $this->db->from('tbl_pos_purchases');
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate) <', $date);
        $this->db->where('YEAR(tbl_pos_purchases.purchaseDate) <', date('Y'));
        $this->db->group_by('tbl_pos_purchases.supplierID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function monthly_purchases_closing_summary($date) {
        $this->db->select('tbl_pos_purchases.supplierID,SUM(tbl_pos_purchases.netTotal) as total_netTotal');
        $this->db->from('tbl_pos_purchases');
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate)', $date);
        $this->db->where('YEAR(tbl_pos_purchases.purchaseDate)', date('Y'));
        $this->db->group_by('tbl_pos_purchases.supplierID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function monthly_inventory_oppening_summary($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('MONTH(tbl_pos_inventory.date) <', $date);
        $this->db->where('YEAR(tbl_pos_inventory.date) <', date('Y'));
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_inventory_closing_summary($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('MONTH(tbl_pos_inventory.date) <=', $date);
        $this->db->where('YEAR(tbl_pos_inventory.date) <=', date('Y'));
        $results = $this->db->get()->row_array();
        return $results;
    }

    //Daily summary report

    function daily_purchases_avg_price($date) {
        $this->db->select('AVG(tbl_pos_purchase_products.purchasePrice) as totalAvgPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'inner');
        $this->db->where('tbl_pos_purchases.purchaseDate <=', $date);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_account_oppening_summary($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount,tbl_pos_transactions.transactionAccountID');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date <', $date);
        $this->db->group_by('tbl_pos_transactions.transactionAccountID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function daily_account_closing_summary($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount,tbl_pos_transactions.transactionAccountID');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date <=', $date);
        $this->db->group_by('tbl_pos_transactions.transactionAccountID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function daily_sales_oppening_summary($date) {
        $this->db->select('SUM(tbl_pos_sale_products.quantity) as total_sales_qty,SUM(tbl_pos_sale_products.price) AS total_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('tbl_pos_sale_products.salesDate <', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_sales_closing_summary($date) {
        $this->db->select('SUM(tbl_pos_sale_products.quantity) as total_sales_qty,SUM(tbl_pos_sale_products.price) AS total_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('tbl_pos_sale_products.salesDate', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_expense_oppening_summary($date) {
        $this->db->select('tbl_pos_expense.head_id,SUM(tbl_pos_expense.amount) as total_expense');
        $this->db->from('tbl_pos_expense');
        $this->db->where('date(tbl_pos_expense.created_at) <', $date);
        $this->db->group_by('tbl_pos_expense.head_id');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function daily_expense_closing_summary($date) {
        $this->db->select('tbl_pos_expense.head_id,SUM(tbl_pos_expense.amount) as total_expense');
        $this->db->from('tbl_pos_expense');
        $this->db->where('date(tbl_pos_expense.created_at)', $date);
        $this->db->group_by('tbl_pos_expense.head_id');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function daily_purchases_oppening_summary($date) {
        $this->db->select('tbl_pos_purchases.supplierID,SUM(tbl_pos_purchases.netTotal) as total_netTotal');
        $this->db->from('tbl_pos_purchases');
        $this->db->where('tbl_pos_purchases.purchaseDate <', $date);
        $this->db->group_by('tbl_pos_purchases.supplierID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function daily_purchases_closing_summary($date) {
        $this->db->select('tbl_pos_purchases.supplierID,SUM(tbl_pos_purchases.netTotal) as total_netTotal');
        $this->db->from('tbl_pos_purchases');
        $this->db->where('tbl_pos_purchases.purchaseDate ', $date);
        $this->db->group_by('tbl_pos_purchases.supplierID');
        $results = $this->db->get()->result_array();
        return $results;
    }

    function daily_inventory_oppening_summary($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date <', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_inventory_closing_summary($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date <=', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    /* Monthly statement report start from here */

    /* Purchases operation Start */

    function monthly_purchases_pre_stock($search_month) {
        $this->db->select('SUM(tbl_pos_purchase_products.quantity) as totalurchaQTY,AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        //$this->db->where('tbl_pos_purchases.purchaseDate < ', $date);
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate) <', $search_month);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_purchases_today_stock($search_month) {
        $this->db->select('SUM(tbl_pos_purchase_products.quantity) as totalurchaQTY,AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        // $this->db->where('tbl_pos_purchases.purchaseDate', $date);
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate)', $search_month);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_purchases_close_stock($search_month) {
        $this->db->select('SUM(tbl_pos_purchase_products.quantity) as totalurchaQTY,AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        //$this->db->where('tbl_pos_purchases.purchaseDate <=', $search_month);
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate) <=', $search_month);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    /* Purchases operation close */
    /* Sales operation start */

    function monthly_sales_pre_inventory($search_month) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_sales_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('MONTH(tbl_pos_inventory.date) <', $search_month);
        $this->db->where('tbl_pos_inventory.type', 'OUT');
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_sales_pre_stock_avg($search_month) {
        $this->db->select('AVG(tbl_pos_sale_products.price) as total_avg_sales_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('MONTH(tbl_pos_sale_products.salesDate) <', $search_month);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_sales_today_inventory($search_month) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_sales_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('MONTH(tbl_pos_inventory.date)', $search_month);
        $this->db->where('tbl_pos_inventory.type', 'OUT');
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_sales_today_stock_avg($search_month) {
        $this->db->select('AVG(tbl_pos_sale_products.price) as total_avg_sales_price');
        $this->db->from('tbl_pos_sale_products');
        // $this->db->where('tbl_pos_sale_products.salesDate', $date);
        $this->db->where('MONTH(tbl_pos_sale_products.salesDate)', $search_month);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_sales_close_inventory($search_month) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_sales_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('MONTH(tbl_pos_inventory.date) <=', $search_month);
        $this->db->where('tbl_pos_inventory.type', 'OUT');
        $results = $this->db->get()->row_array();
        return $results;
    }

    function monthly_sales_close_stock_avg($search_month) {
        $this->db->select('AVG(tbl_pos_sale_products.price) as total_avg_sales_price');
        $this->db->from('tbl_pos_sale_products');
        //$this->db->where('tbl_pos_sale_products.salesDate <=', $date);
        $this->db->where('MONTH(tbl_pos_sale_products.salesDate) <= ', $search_month);
        $results = $this->db->get()->row_array();
        return $results;
    }

    /* Sales operation End */

    /* Inventory operation Start */

    function monthly_inventory_avg_price_stock($search_month) {
        $this->db->select('AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchases');
        $this->db->join('tbl_pos_purchase_products', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        //$this->db->where('tbl_pos_purchases.purchaseDate < ', $date);
        $this->db->where('MONTH(tbl_pos_purchases.purchaseDate) <=', $search_month);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['totalPurchasesPrice'];
    }

    function monthly_inventory_pre_stock($search_month) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_pre_qty');
        $this->db->from('tbl_pos_inventory');
        //$this->db->where('tbl_pos_inventory.date <', $date);
        $this->db->where('MONTH(tbl_pos_inventory.date) < ', $search_month);
        $this->db->where('tbl_pos_inventory.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['total_pre_qty'];
    }

    function monthly_inventory_today_stock($search_month) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_queryday_qty');
        $this->db->from('tbl_pos_inventory');
        //$this->db->where('tbl_pos_inventory.date', $date);
        $this->db->where('MONTH(tbl_pos_inventory.date)', $search_month);
        $this->db->where('tbl_pos_inventory.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['total_queryday_qty'];
    }

    function monthly_inventory_close_stock($search_month) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_closing_qty');
        $this->db->from('tbl_pos_inventory');
        //$this->db->where('tbl_pos_inventory.date <=', $date);
        $this->db->where('MONTH(tbl_pos_inventory.date) <=', $search_month);
        $this->db->where('tbl_pos_inventory.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['total_closing_qty'];
    }

    /* Inventory operation close */


    /* Account Expense Report Start */

    function monthly_expense_pre_stock($search_month) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount');
        $this->db->from('tbl_pos_transactions');
        //$this->db->where('tbl_pos_transactions.date <', $date);
        $this->db->where('MONTH(tbl_pos_transactions.date) <', $search_month);
        $this->db->where('tbl_pos_transactions.transactionType', 'EXPENSE');
        $results = $this->db->get()->row_array();
        return $results['total_pre_Amount'];
    }

    function monthly_expense_today_stock($search_month) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_today_Amount');
        $this->db->from('tbl_pos_transactions');
        //$this->db->where('tbl_pos_transactions.date', $date);
        $this->db->where('MONTH(tbl_pos_transactions.date)', $search_month);
        $this->db->where('tbl_pos_transactions.transactionType', 'EXPENSE');
        $results = $this->db->get()->row_array();
        return $results['total_today_Amount'];
    }

    function monthly_expense_close_stock($search_month) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_closing_Amount');
        $this->db->from('tbl_pos_transactions');
        //$this->db->where('tbl_pos_transactions.date <=', $date);
        $this->db->where('MONTH(tbl_pos_transactions.date) <= ', $search_month);
        $this->db->where('tbl_pos_transactions.transactionType', 'EXPENSE');
        $results = $this->db->get()->row_array();
        return $results['total_closing_Amount'];
    }

    /* Account Expense Report End */


    /* Account operation start */

    function monthly_account_pre_stock($search_month) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount');
        $this->db->from('tbl_pos_transactions');
        // $this->db->where('tbl_pos_transactions.date <', $date);
        $this->db->where('MONTH(tbl_pos_transactions.date) < ', $search_month);
        $results = $this->db->get()->row_array();
        return $results['total_pre_Amount'];
    }

    function monthly_account_today_stock($search_month) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_today_Amount');
        $this->db->from('tbl_pos_transactions');
        // $this->db->where('tbl_pos_transactions.date', $date);
        $this->db->where('MONTH(tbl_pos_transactions.date)', $search_month);
        $results = $this->db->get()->row_array();
        return $results['total_today_Amount'];
    }

    function monthly_account_close_stock($search_month) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_closing_Amount');
        $this->db->from('tbl_pos_transactions');
        // $this->db->where('tbl_pos_transactions.date <=', $date);
        $this->db->where('MONTH(tbl_pos_transactions.date) <=', $search_month);
        $results = $this->db->get()->row_array();
        return $results['total_closing_Amount'];
    }

    /* Account operation close */









    /* ------------ Monthly statement report End from here ------------------------ */













    /* ---------------- Daily statement report start from here --------------------------- */

    /* Purchases operation Start */

    function daily_purchases_pre_stock($date) {
        $this->db->select('SUM(tbl_pos_purchase_products.quantity) as totalurchaQTY,AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        $this->db->where('tbl_pos_purchases.purchaseDate < ', $date);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_purchases_today_stock($date) {
        $this->db->select('SUM(tbl_pos_purchase_products.quantity) as totalurchaQTY,AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        $this->db->where('tbl_pos_purchases.purchaseDate', $date);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_purchases_close_stock($date) {
        $this->db->select('SUM(tbl_pos_purchase_products.quantity) as totalurchaQTY,AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchase_products');
        $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        $this->db->where('tbl_pos_purchases.purchaseDate <=', $date);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results;
    }

    /* Purchases operation close */
    /* Sales operation start */

    function daily_sales_pre_inventory($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_sales_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date <', $date);
        $this->db->where('tbl_pos_inventory.type', 'OUT');
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_sales_pre_stock($date) {
        $this->db->select('AVG(tbl_pos_sale_products.price) as total_avg_sales_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('tbl_pos_sale_products.salesDate < ', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_sales_today_inventory($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_sales_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date', $date);
        $this->db->where('tbl_pos_inventory.type', 'OUT');
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_sales_today_stock($date) {
        $this->db->select('AVG(tbl_pos_sale_products.price) as total_avg_sales_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('tbl_pos_sale_products.salesDate', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_sales_close_inventory($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_sales_quantity');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date <=', $date);
        $this->db->where('tbl_pos_inventory.type', 'OUT');
        $results = $this->db->get()->row_array();
        return $results;
    }

    function daily_sales_close_stock($date) {
        $this->db->select('AVG(tbl_pos_sale_products.price) as total_avg_sales_price');
        $this->db->from('tbl_pos_sale_products');
        $this->db->where('tbl_pos_sale_products.salesDate <=', $date);
        $results = $this->db->get()->row_array();
        return $results;
    }

    /* Sales operation End */

    /* Inventory operation Start */

    function daily_inventory_avg_price_stock($date) {
        $this->db->select('AVG(tbl_pos_purchase_products.purchasePrice) as totalPurchasesPrice');
        $this->db->from('tbl_pos_purchases');
        $this->db->join('tbl_pos_purchase_products', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        $this->db->where('tbl_pos_purchases.purchaseDate <=', $date);
        $this->db->where('tbl_pos_purchases.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['totalPurchasesPrice'];
    }

    function daily_inventory_pre_stock($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_pre_qty');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date <', $date);
        $this->db->where('tbl_pos_inventory.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['total_pre_qty'];
    }

    function daily_inventory_today_stock($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_queryday_qty');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date', $date);
        $this->db->where('tbl_pos_inventory.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['total_queryday_qty'];
    }

    function daily_inventory_close_stock($date) {
        $this->db->select('SUM(tbl_pos_inventory.quantity) as total_closing_qty');
        $this->db->from('tbl_pos_inventory');
        $this->db->where('tbl_pos_inventory.date <=', $date);
        $this->db->where('tbl_pos_inventory.warehouseID', 4);
        $results = $this->db->get()->row_array();
        return $results['total_closing_qty'];
    }

    /* Inventory operation close */


    /* Account Expense Report Start */

    function daily_expense_pre_stock($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date <', $date);
        $this->db->where('tbl_pos_transactions.transactionType', 'EXPENSE');
        $results = $this->db->get()->row_array();
        return $results['total_pre_Amount'];
    }

    function daily_expense_today_stock($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_today_Amount');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date', $date);
        $this->db->where('tbl_pos_transactions.transactionType', 'EXPENSE');
        $results = $this->db->get()->row_array();
        return $results['total_today_Amount'];
    }

    function daily_expense_close_stock($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_closing_Amount');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date <=', $date);
        $this->db->where('tbl_pos_transactions.transactionType', 'EXPENSE');
        $results = $this->db->get()->row_array();
        return $results['total_closing_Amount'];
    }

    /* Account Expense Report End */


    /* Account operation start */

    function daily_account_pre_stock($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_pre_Amount');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date <', $date);
        $results = $this->db->get()->row_array();
        return $results['total_pre_Amount'];
    }

    function daily_account_today_stock($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_today_Amount');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date', $date);
        $results = $this->db->get()->row_array();
        return $results['total_today_Amount'];
    }

    function daily_account_close_stock($date) {
        $this->db->select('SUM(tbl_pos_transactions.transactionAmount) as total_closing_Amount');
        $this->db->from('tbl_pos_transactions');
        $this->db->where('tbl_pos_transactions.date <=', $date);
        $results = $this->db->get()->row_array();
        return $results['total_closing_Amount'];
    }

    /* Account operation close */

    /* ----------------------- Daily statement report End from here ------------------------ */

    function total_damage_report($first_date, $last_date) {
        $this->db->select('tbl_pos_inventory.timestamp as damage_date,tbl_pos_products.productName,tbl_pos_inventory.productID,SUM(tbl_pos_inventory.quantity) AS total_qty,tbl_pos_products.productPrice as product_price,tbl_pos_products.productCode as productCode');
        $this->db->from('tbl_pos_inventory');
        $this->db->join('tbl_pos_products', 'tbl_pos_products.productID = tbl_pos_inventory.productID', 'left');
        $this->db->where('date(tbl_pos_inventory.timestamp) >=', $first_date);
        $this->db->where('date(tbl_pos_inventory.timestamp) <=', $last_date);
        $this->db->where('tbl_pos_inventory.type', 'DAMAGE-OUT');
        $results = $this->db->get()->result_array();
        return $results;
    }

//    function salesReport($firstDate, $lastDate) {
//        $this->db->select('salesDate,SUM(discount) AS total_dis,SUM(netTotal) AS total_net_amount');
//        $this->db->where('salesDate >= ', $firstDate);
//        $this->db->where('salesDate <= ', $lastDate);
//        $this->db->order_by("salesDate", "DESC");
//        $this->db->group_by('salesDate');
//        $query = $this->db->get('tbl_pos_sales');
//        $result = $query->result();
//        return $result;
//    }
    function salesReport($firstDate, $lastDate) {
        $this->db->select('invoiceNo,salesDate,sales_persion,subTotal,percentage,vat,discount,netTotal,grandTotal');
        $this->db->where('salesDate >= ', $firstDate);
        $this->db->where('salesDate <= ', $lastDate);
        $this->db->order_by("salesDate", "DESC");
//        $this->db->group_by('salesDate');
        $query = $this->db->get('tbl_pos_sales');
        $result = $query->result();
        return $result;
    }

    function detailSalesRep($firstDate, $lastDate, $category_id) {
        $this->db->select('sp.productID, p.productName, p.productCode');
        $this->db->from('tbl_pos_sale_products as sp');

        if($category_id === false)
        {
            $this->db->join('tbl_pos_products as p', "sp.productID = p.productID", "left");
        }
        else
        {
            $this->db->join('tbl_pos_products as p', "sp.productID = p.productID", "inner");
            $this->db->join('tbl_pos_product_catagories catagories', "catagories.product_catagoriesID = p.catagoryID", "inner");
            $this->db->where('catagories.product_catagoriesID', $category_id);
        }

        $this->db->group_by("sp.productID");
        $this->db->order_by("sp.saleProductID", "DESC");
        $this->db->where('sp.salesDate >= ', $firstDate);
        $this->db->where('sp.salesDate <= ', $lastDate);
        $result = $this->db->get()->result_array();
        foreach ($result as $k => $val) {
            $where = array("salesDate >= " => $firstDate, "salesDate <= " => $lastDate, "productID" => $val["productID"]);
            $invoices = $this->common_model->get_result("invoiceNo", "tbl_pos_sale_products", '', $where);
            $qtyPrice = $this->common_model->get_row("sum(quantity)as quantity, price as price", "tbl_pos_sale_products", $where);
            array_push($result[$k], $invoices, $qtyPrice);
        }

        return $result;
    }

    function detailSalesRepDiscount($firstDate, $lastDate)
    {
        $this->db->select('sum(tbl_pos_sales.discount) as total_discount');
        $this->db->from('tbl_pos_sales');
        $this->db->where('tbl_pos_sales.salesDate >= ', $firstDate);
        $this->db->where('tbl_pos_sales.salesDate <= ', $lastDate);
        $result = $this->db->get()->row_array();
        return $result['total_discount'];
    }

    function detailsExpenseReport($firstDate, $lastDate) {
        $this->db->select('*');
        $this->db->from('tbl_pos_expense');
        $this->db->order_by("created_at", "asc");
        $this->db->where('created_at >= ', $firstDate);
        $this->db->where('created_at <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function detailsHWExpenseReportH($firstDate, $lastDate, $head) {
        $this->db->select('*');
        $this->db->from('tbl_pos_expense');
        $this->db->where("head_id", $head);
        $this->db->where('created_at >= ', $firstDate);
        $this->db->where('created_at <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function detailsHWExpenseReportSH($firstDate, $lastDate, $subhead) {
        $this->db->select('*');
        $this->db->from('tbl_pos_expense');
        $this->db->where("sub_head_id", $subhead);
        $this->db->where('created_at >= ', $firstDate);
        $this->db->where('created_at <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function detailsprocuctReport($firstDate, $lastDate, $productid) {
        $this->db->select('SUM(tbl_pos_sales.netTotal) as netttl,tbl_pos_sales.invoiceNo,tbl_pos_sale_products.productID,tbl_pos_sales.salesDate');
        $this->db->from('tbl_pos_sale_products');
        $this->db->join('tbl_pos_sales', 'tbl_pos_sales.invoiceNo = tbl_pos_sale_products.invoiceNo', 'left');
        //$this->db->order_by("tbl_pos_sales.salesDate", "asc");
        $this->db->group_by('tbl_pos_sales.salesDate');
        $this->db->where('tbl_pos_sale_products.productID', $productid);
        $this->db->where('tbl_pos_sales.salesDate >= ', $firstDate);
        $this->db->where('tbl_pos_sales.salesDate <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function detailsSupplierReport($firstDate, $lastDate, $supplierID) {
        $this->db->select('*');
        $this->db->from('tbl_pos_purchases');
        $this->db->order_by("purchaseDate", "DESC");
        //$this->db->group_by('purchaseDate');
        $this->db->where('supplierID', $supplierID);
        $this->db->where('purchaseDate >= ', $firstDate);
        $this->db->where('purchaseDate <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }
    function supplierPaymentCollection($firstDate, $lastDate, $supplierID) {
        $this->db->select('*');
        $this->db->from('tbl_pos_supplier_payment');
        $this->db->where('supplier_id', $supplierID);
        $this->db->where('date >= ', $firstDate);
        $this->db->where('date <= ', $lastDate);
        $this->db->order_by("payment_id", "DESC");
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    function detailsClientReport($firstDate, $lastDate, $clientid) {
        $this->db->select('SUM(tbl_pos_sales.netTotal) as netttl,SUM(tbl_pos_sales.posPaying) as paayttl,SUM(tbl_pos_sales.dueAmount) as duettl,salesDate');
        $this->db->from('tbl_pos_sales');
        $this->db->order_by("tbl_pos_sales.salesDate", "asc");
        $this->db->group_by('tbl_pos_sales.salesDate');
        $this->db->where('tbl_pos_sales.clientID', $clientid);
        $this->db->where('tbl_pos_sales.salesDate >= ', $firstDate);
        $this->db->where('tbl_pos_sales.salesDate <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function detailsPurchseReport($firstDate, $lastDate, $category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_pos_purchases');

        if($category_id === false)
        {
            $this->db->join('tbl_pos_purchase_products pp', 'pp.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
        }
        else
        {
            $this->db->join('tbl_pos_purchase_products pp', 'pp.purchaseNo = tbl_pos_purchases.purchaseNo', 'inner');
            $this->db->join('tbl_pos_products as p', "pp.productID = p.productID", "inner");
            $this->db->join('tbl_pos_product_catagories catagories', "catagories.product_catagoriesID = p.catagoryID", "inner");
            $this->db->where('catagories.product_catagoriesID', $category_id);
        }

        $this->db->order_by("tbl_pos_purchases.purchaseDate", "asc");
        $this->db->where('tbl_pos_purchases.purchaseDate >= ', $firstDate);
        $this->db->where('tbl_pos_purchases.purchaseDate <= ', $lastDate);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function getposreport($date) {
        $this->db->select('SUM(netTotal) as netttl,SUM(discount) as ttldiscount');
        $this->db->from('tbl_pos_sales');
        $this->db->where('salesDate', $date);
        $this->db->where('clientID', 0);
        $query = $this->db->get('');
        $result = $query->row_array();
        return $result;
    }

    function getsalesonly($date) {
        $this->db->select('SUM(netTotal) as netttl,SUM(discount) as ttldiscount');
        $this->db->from('tbl_pos_sales');
        $this->db->where('salesDate', $date);
        $this->db->where('clientID', 2);
        $query = $this->db->get('');
        $result = $query->row_array();
        return $result;
    }

    //profit / loss

    public function querycurrenttotalsale($first_date, $last_date) {
        $this->db->select('SUM(netTotal) as grand_total');
        $this->db->from('tbl_pos_sales');
        $this->db->where('salesDate >=', $first_date);
        $this->db->where('salesDate <=', $last_date);
        return $this->db->get()->row();
    }

    public function totalSales($first_date, $last_date) {
        $this->db->select('SUM(netTotal) AS totalSaless');
        $this->db->from('tbl_pos_sales');
        $this->db->where('salesDate >=', $first_date);
        $this->db->where('salesDate <=', $last_date);
        return $this->db->get()->row_array();
    }

  public function duePurchase($first_date, $last_date)
  {
    $this->db->select('SUM(totalDueAmount) AS total_purchase,SUM(amount) AS payAmount');
    $this->db->from('tbl_pos_supplier_payment');
    $this->db->where('date >=', $first_date);
    $this->db->where('date <=', $last_date);
    return $this->db->get()->row_array();
  }

  function accountBalance($accountID = '') {
      $this->db->select('accountID,accountName');
      $this->db->from('tbl_pos_accounts');
      $query_result = $this->db->get();
      $result = $query_result->result();

      foreach ($result as $key => $value) {
          $result[$key]->balance = $this->askAccountBalanceRemain($value->accountID);
      }

      return $result;
  }

  private function askAccountBalanceRemain($accountID) {
      $this->db->select('SUM(transactionAmount) as balance');
      $this->db->from('tbl_pos_transactions');
      $this->db->where('tbl_pos_transactions.transactionAccountID', $accountID);
      $this->db->group_by('tbl_pos_transactions.transactionAccountID');
      $query_result = $this->db->get();
      $result = $query_result->result();
      if (empty($result)) {

      } else {
          return $result[0]->balance;
      }
  }

    public function salesGoods($first_date, $last_date) {
        $this->db->select('sum(tbl_pos_sale_products.quantity)as qty, tbl_pos_sale_products.productID, tbl_pos_sale_products.quantity, tbl_pos_sales.invoiceNo, tbl_pos_sales.salesDate');
        $this->db->from('tbl_pos_sale_products');
        $this->db->join('tbl_pos_sales', 'tbl_pos_sale_products.invoiceNo = tbl_pos_sales.invoiceNo', 'left');
        $this->db->where('tbl_pos_sales.salesDate >=', $first_date);
        $this->db->where('tbl_pos_sales.salesDate <=', $last_date);
        $this->db->group_by('tbl_pos_sale_products.productID');
        return $this->db->get()->result_array();
    }

    function avgCostGoods($data, $first_date, $last_date) {
            $totalCost = 0;
            foreach ($data as $r) {
                $this->db->select('avg(tbl_pos_inventory.purchasePrice)as avg_price');
                $this->db->from('tbl_pos_purchase_products');
                $this->db->join('tbl_pos_purchases', 'tbl_pos_purchase_products.purchaseNo = tbl_pos_purchases.purchaseNo', 'left');
                $this->db->join('tbl_pos_inventory', 'tbl_pos_purchase_products.productID = tbl_pos_inventory.productID');
                $this->db->where('tbl_pos_purchase_products.productID', $r['productID']);
                $this->db->where('tbl_pos_inventory.type', 'OUT');
                $this->db->group_by('tbl_pos_purchase_products.productID');
                $price = $this->db->get()->row_array();
                if($price['avg_price']==0){
                   $this->db->select('AVG(price)AS avg_price');
                   $this->db->from('tbl_pos_inventory');
                   $this->db->where('productID', $r['productID']);
                   $this->db->where('type', 'IN');
                   $price = $this->db->get()->row_array();
               }


               $totalCost += $price['avg_price'] * $r['qty'];
           }
           return $totalCost;
       }
   public function totalPurchases($first_date, $last_date) {
    $this->db->select('SUM(netTotal) AS totalPurchases');
    $this->db->from('tbl_pos_purchases');
        //$this->db->join('invoice as i', 'i.invoice_id = p.invoice_id', 'right');
    $this->db->where('purchaseDate >=', $first_date);
    $this->db->where('purchaseDate <=', $last_date);
    return $this->db->get()->row_array();
}

public function totalExpenses($first_date, $last_date) {
    $this->db->select('SUM(amount) AS totalExpenses');
    $this->db->from('tbl_pos_expense');
    $this->db->where('created_at >=', $first_date);
    $this->db->where('created_at <=', $last_date);
    return $this->db->get()->row_array();
}

public function profitLosss($first_date, $last_date) {
    $this->db->select('i.date, i.type, i.inventoryID, i.productID, p.productName, p.productCode, p.productPrice, AVG(sp.price)AS avg_selling');
    $this->db->from('tbl_pos_inventory as i');
    $this->db->join('tbl_pos_products as p', 'i.productID = p.productID', 'left');
    $this->db->join('tbl_pos_sales as s', 'i.refNo = s.invoiceNo', 'left');
    $this->db->join('tbl_pos_sale_products as sp', 's.invoiceNo = sp.invoiceNo', 'left');
    $this->db->where('i.date >=', $first_date);
    $this->db->where('i.date <=', $last_date);
    $this->db->where('i.type', 'OUT');
    $this->db->group_by("i.productID");
    $query = $this->db->get();
    $result = $query->result_array();
    foreach ($result as $k => $r) {
        $avgPrice = $this->AvgSellingPrice($first_date, $last_date, $r['productID']);
        $avgPurchasePrice = $this->AvgPurchasingPrice($first_date, $last_date, $r['productID']);
        array_push($result[$k], $avgPrice, $avgPurchasePrice);
    }
    return $result;
}

public function profitLosss2($first_date, $last_date) {
    $this->db->select('tbl_pos_inventory.date, tbl_pos_inventory.type, tbl_pos_inventory.inventoryID, tbl_pos_inventory.productID, tbl_pos_products.productName, tbl_pos_products.productCode, tbl_pos_products.productPrice, AVG(tbl_pos_sale_products.price)AS avg_selling');
    $this->db->from('tbl_pos_inventory');
    $this->db->join('tbl_pos_products', 'tbl_pos_inventory.productID = tbl_pos_products.productID', 'left');
    $this->db->join('tbl_pos_sales', 'tbl_pos_inventory.refNo = tbl_pos_sales.invoiceNo', 'left');
    $this->db->join('tbl_pos_sale_products', 'tbl_pos_sales.invoiceNo = tbl_pos_sale_products.invoiceNo', 'left');
    $this->db->where('tbl_pos_inventory.date >=', $first_date);
    $this->db->where('tbl_pos_inventory.date <=', $last_date);
    $this->db->where('tbl_pos_inventory.type', 'OUT');
    $this->db->group_by("tbl_pos_inventory.productID");
    $query = $this->db->get();
    $result = $query->result_array();
    die("");
    foreach ($result as $k => $r) {
        $avgPrice = $this->AvgSellingPrice($first_date, $last_date, $r['productID']);
        $avgPurchasePrice = $this->AvgPurchasingPrice($first_date, $last_date, $r['productID']);
        array_push($result[$k], $avgPrice, $avgPurchasePrice);
    }
    return $result;
}

public function AvgSellingPrice($first_date, $last_date, $item_id) {
    $this->db->select('AVG(i.price)AS avg_selling, sum(i.quantity)AS quantity');
    $this->db->from('tbl_pos_sale_products as i');
    $this->db->join('tbl_pos_sales as s', 'i.invoiceNo = s.invoiceNo', 'left');
    $this->db->where('i.productID', $item_id);
    $this->db->where('s.salesDate >=', $first_date);
    $this->db->where('s.salesDate <=', $last_date);
    return $this->db->get()->row_array();
}

public function p_cost_id($id) {
    $this->db->select('AVG(tbl_pos_inventory.purchasePrice) AS avgPrice , tbl_pos_inventory.productID');
    $this->db->from('tbl_pos_purchase_products');
    $this->db->join('tbl_pos_inventory', 'tbl_pos_inventory.productID = tbl_pos_purchase_products.productID');
    $this->db->where('tbl_pos_purchase_products.productID', $id);
    $this->db->where('tbl_pos_inventory.type', 'OUT');
    $this->db->group_by('tbl_pos_inventory.productID');
    return $this->db->get()->row_array();
}
public function overallcost($month, $year) {
    $this->db->select('SUM(quantity) AS quantity , productID');
    $this->db->from('tbl_pos_sale_products');

    if (empty($month)) {
        $this->db->where('MONTH(salesDate)', $month);
    } else {
        $this->db->where('MONTH(salesDate)', $month);
        $this->db->where('YEAR(salesDate)', $year);
    }
    $this->db->group_by('productID');
    $data = $this->db->get()->result_array();
    $sum = 0;
    foreach ($data as $key => $value) {
        $a = $this->p_cost_id($value['productID']);
        $sum+= $a['avgPrice']*$value['quantity'];
    }

    return (int)$sum;
}

public function AvgPurchasingPrice($first_date, $last_date, $item_id) {
    $this->db->select('AVG(i.purchasePrice)AS avg_purchasing');
    $this->db->from('tbl_pos_purchase_products as i');
    $this->db->join('tbl_pos_purchases as s', 'i.purchaseNo = s.purchaseNo', 'left');
    $this->db->where('i.productID', $item_id);
    return $this->db->get()->row_array();
}

public function totalpurchase($month, $year) {
    $this->db->select('sum(netTotal) as ttlpurchase');
    $this->db->from('tbl_pos_purchases');
    if (empty($month)) {
        $this->db->where('MONTH(purchaseDate)', $month);
    } else {
        $this->db->where('MONTH(purchaseDate)', $month);
        $this->db->where('YEAR(purchaseDate)', $year);
    }
    return $this->db->get()->row();
}

public function totalpurchasedue($month, $year) {
    $this->db->select('sum(dueAmount) as ttlpurchasedue');
    $this->db->from('tbl_pos_purchases');

    if (empty($month)) {
        $this->db->where('MONTH(purchaseDate)', $month);
    } else {
        $this->db->where('MONTH(purchaseDate)', $month);
        $this->db->where('YEAR(purchaseDate)', $year);
    }
    return $this->db->get()->row();
}

public function totalsale($month, $year) {
    $this->db->select('sum(netTotal) as ttlsales');
    $this->db->from('tbl_pos_sales');

    if (empty($month)) {
        $this->db->where('MONTH(salesDate)', $month);
    } else {
        $this->db->where('MONTH(salesDate)', $month);
        $this->db->where('YEAR(salesDate)', $year);
    }
    return $this->db->get()->row();
}

public function totalexpense($month, $year) {
    $this->db->select('sum(amount) as ttltransaction');
    $this->db->from('tbl_pos_expense');
        //$this->db->where('transactionType','EXPENSE');
    if (empty($month)) {
        $this->db->where('MONTH(created_at)', $month);
    } else {
        $this->db->where('MONTH(created_at)', $month);
        $this->db->where('YEAR(created_at)', $year);
    }
    return $this->db->get()->row();
}

function detailsAccountReport($firstDate, $accID) {
    $this->db->select('SUM(transactionAmount) as ttltransaction');
    $this->db->from('tbl_pos_transactions');
    $this->db->order_by("created_at", "asc");
    $this->db->where('transactionAccountID', $accID);
    $this->db->where('transactionType', 'SALE');
    $this->db->where('date = ', $firstDate);
        //$this->db->where('date <= ', $lastDate);
    $query_result = $this->db->get();
    $result = $query_result->row_array();
    return $result;
}

function detailsAccountReport2($firstDate, $accID) {
    $this->db->select('SUM(transactionAmount) as ttltransactionout,date');
    $this->db->from('tbl_pos_transactions');
    $this->db->order_by("created_at", "asc");
    $this->db->where('transactionAccountID', $accID);
    $this->db->where('transactionType != ', 'SALE');
    $this->db->where('date = ', $firstDate);
        // $this->db->where('date <= ', $lastDate);
    $query_result = $this->db->get();
    $result = $query_result->row_array();
    return $result;
}

function returnRep($firstDate, $lastDate) {
    $this->db->select('sp.productID, p.productName, p.productCode');
    $this->db->from('tbl_pos_sale_products as sp');
    $this->db->join('tbl_pos_products as p', "sp.productID = p.productID", "left");
    $this->db->group_by("sp.productID");
    $this->db->order_by("sp.saleProductID", "DESC");
    $this->db->where('sp.returnDate >= ', $firstDate);
    $this->db->where('sp.returnDate <= ', $lastDate);
    $result = $this->db->get()->result_array();
    foreach ($result as $k => $val) {
        $where = array("returnDate >= " => $firstDate, "returnDate <= " => $lastDate, "productID" => $val["productID"]);
        $invoices = $this->common_model->get_result("invoiceNo", "tbl_pos_sale_products", '', $where);
        $qtyPrice = $this->common_model->get_row("sum(returnQuantity)as returnQuantity", "tbl_pos_sale_products", $where);
        $productPrice = $this->common_model->get_row("productPrice", "tbl_pos_products", array("productID" => $val["productID"]));
        array_push($result[$k], $invoices, $qtyPrice, $productPrice["productPrice"] * $qtyPrice["returnQuantity"]);
    }
//        echo '<pre>';print_r($result);die;
    return $result;
}


function viewReturnChage($invoiceId) {
    $this->db->select('returnCharge');
//        $this->db->where('returnDate >= ', $firstDate);
//        $this->db->where('returnDate <= ', $lastDate);
    $this->db->where('invoiceNo ', $invoiceId);
    $query = $this->db->get('tbl_pos_sales');
    $result = $query->row();
    return $result->returnCharge;
}

function vatReport($firstDate, $lastDate) {
    $this->db->select('invoiceNo,salesDate,subTotal,percentage,vat,discount,netTotal');
    $this->db->where('salesDate >= ', $firstDate);
    $this->db->where('salesDate <= ', $lastDate);
    $this->db->order_by("salesDate", "DESC");
    $this->db->group_by('salesDate');
    $query = $this->db->get('tbl_pos_sales');
    $result = $query->result();
    return $result;
}
function viewSearchingMonth($month){
    if($month=='01'){
        return "January";
    }else if($month=='02'){
        return "Febuary";
    }else if($month=='03'){
        return "March";
    }else if($month=='04'){
        return "April";
    }else if($month=='05'){
        return "May";
    }else if($month=='06'){
        return "June";
    }else if($month=='07'){
        return "July";
    }else if($month=='08'){
        return "August";
    }else if($month=='09'){
        return "September";
    }else if($month=='10'){
        return "Octabar";
    }else if($month=='11'){
        return "November";
    }else if($month=='12'){
        return "December";
    }
}


}
