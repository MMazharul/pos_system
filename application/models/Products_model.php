<?php

class Products_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function products($productID = '') {
        $this->db->select('*');
        $this->db->from('tbl_pos_products');
        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID', 'left');
        $this->db->where('tbl_pos_products.softDelete', 0);
        if (!empty($productID)) {
            $this->db->where('tbl_pos_products.productID', $productID);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    function storeProduct($data) {
        $this->db->insert('tbl_pos_products', array(
            'productName' => $data['productName'],
            'productCode' => $data['productCode'],
            'productPrice' => $data['productPrice'],
            'productModel' => $data['productModel'],
            'catagoryID' => $data['catagoryID'],
        ));

        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function destroyProduct($productID) {
        $this->db->where('productID', $productID);
        $this->db->update('tbl_pos_products', array(
            'softDelete' => 1,
        ));
        return TRUE;
    }

    function get_single_data_by_single_column($table_name, $column_name, $column_value) {
        $this->db->where($column_name, $column_value);
        return $this->db->get($table_name)->row_array();
    }

    function updateProduct($data) {
        $this->db->where('productID', $data['productID']);
        $this->db->update('tbl_pos_products', array(
            'productName' => $data['productName'],
            'productCode' => $data['productCode'],
            'productPrice' => $data['productPrice'],
            'productModel' => $data['productModel'],
            'catagoryID' => $data['catagoryID'],
        ));

        return TRUE;
    }

    function productSuggestion($q) {
        $this->db->select('*');
        $this->db->like('tbl_pos_products.productName', $q);
        $this->db->or_like('tbl_pos_products.productCode', $q);
        $this->db->where('tbl_pos_products.softDelete', 0);

        $this->db->join('tbl_pos_product_catagories', 'tbl_pos_products.catagoryID = tbl_pos_product_catagories.product_catagoriesID', 'left');
        $this->db->from('tbl_pos_products');
        $query = $this->db->get();
        $result = $query->result_array();
        if (count($result) > 0) {
            $data = array();
            foreach ($result as $key => $value) {
                if($result[$key]['productModel']!=''){
                    $model="[".$result[$key]['productModel']."]";
                }else{
                    $model='';
                }
                $data[$key]['id'] = $result[$key]['productID'];
                $data[$key]['productCode'] = $result[$key]['productCode'];
                $data[$key]['productName'] = $result[$key]['productName'];
                $data[$key]['productPrice'] = $result[$key]['productPrice'];
                $data[$key]['catName'] = $result[$key]['catagoryName'];
                $data[$key]['value'] = $result[$key]['productName'] . ' [' . $result[$key]['productCode'] . '] '.$model;
            }
            return $data;
        }
    }

}