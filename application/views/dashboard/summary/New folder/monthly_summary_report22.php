<style>
    table tr td{
        //text-align: right;
        // font-weight: bold;
        border: 1px solid black !important;
    }
    table tr th{
        //text-align: right;
        font-weight: bold;
        border: 1px solid black !important;
    }

</style>

<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Monthly Summary( <?php echo $search_month; ?>)
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-8">
                    
                </div>
                <div class="col-xs-4">
                    <button type="button" onclick="window.print();" class="no-print btn  btn-success pull-right"><i class="fa fa-print"></i> Print</button>
                    <button type="button" onclick="window.history.back();" class="no-print btn  btn-danger pull-right" style="margin-right:5px;"><i class="fa fa-backward"></i> Back</button>
<!--                    <a href="<?php echo site_url('reports/datewiseExpensedetails_exportbyxls'); ?>" class="btn btn-app pull-right">
               <i class="fa fa-save"></i> EXCEL
           </a>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-offset-3 table-responsive">
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Account(Openning Balance)</th>
                        </tr>
                        <?php
                        foreach ($monthly_account_oppening_summary as $each_info):
                            $account_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_accounts', 'accountID', $each_info['transactionAccountID']);
                            ?>
                            <tr>
                                <td width="50%"><?php echo $account_name['accountName']; ?></td>
                                <td style="text-align:right;"><?php echo number_format($each_info['total_pre_Amount'], 2); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </thead>
                </table>
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Account(Closing Balance)</th>
                        </tr>
                        <?php
                        foreach ($monthly_account_closing_summary as $each_info):
                            $account_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_accounts', 'accountID', $each_info['transactionAccountID']);
                            ?>
                            <tr>
                                <td width="50%"><?php echo $account_name['accountName']; ?></td>
                                <td style="text-align:right;"><?php echo number_format($each_info['total_pre_Amount'],2); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </thead>
                </table>
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Sales</th>
                        </tr>

                        <tr>
                            <td width="50%">Qty</td>
                            <td style="text-align:right;"><?php echo $monthly_sales_closing_summary['total_sales_qty']; ?></td>
                        </tr>
                         <?php $total_mnth_sale = $this->COMMON_MODEL->viewMonthTotalSale('tbl_pos_sales', 'netTotal', 'created_at'); ?>
                        <tr>
                            <td width="50%">Value</td>
                            <td style="text-align:right;"><?php echo number_format($total_mnth_sale,2); ?></td>
                        </tr>

                    </thead>
                </table>
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Expense</th>
                        </tr>
                        <?php
                        $total_expense = 0;
                        foreach ($monthly_expense_closing_summary as $each_info):
                            $expense_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_expense_head', 'expheadID', $each_info['head_id']);
                            $total_expense += $each_info['total_expense'];
                            ?>
                            <tr>
                                <td  width="50%"><?php echo $expense_name['title']; ?></td>
                                <td style="text-align:right;"><?php echo number_format($each_info['total_expense'],2); ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td style="text-align:right;">Total:</td>
                            <td style="text-align:right;"><?php echo number_format($total_expense, 2); ?></td>
                        </tr>
                    </thead>
                </table>
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Purchases</th>
                        </tr>
                        <?php
                        $total_purchases = 0;
                        foreach ($monthly_purchases_closing_summary as $each_info):
                            $supplier_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_suppliers', 'supplierID', $each_info['supplierID']);
                            $total_purchases += $each_info['total_netTotal'];
                            ?>
                            <tr>
                                <td width="50%"><?php echo $supplier_name['supplierName']; ?></td>
                                <td style="text-align:right;"><?php echo number_format($each_info['total_netTotal'],2); ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td style="text-align:right;">Total:</td>
                            <td style="text-align:right;"><?php echo number_format($total_purchases,2); ?></td>
                        </tr>

                    </thead>
                </table>
                
                <?php
                    $total_quantity = 0;
                    $total_value = 0;
                    foreach($total_quantities_opening as $item)
                    {
                        $total_quantity += $item['total_quantity'];
                        if(isset($avg_price_purchase[$item['productID']]))
                        {
                            $value = $avg_price_purchase[$item['productID']];
                        }
                        else
                        {
                            $value = $avg_price_inventory[$item['productID']];
                        }
                        $total_value += $value * $item['total_quantity'];
                    }
                ?>
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Inventory(Opening )</th>
                        </tr>

                        <tr>
                            <td width="50%">Qty</td>
                            <td style="text-align:right;"><?php echo number_format($total_quantity, 2); ?></td>
                        </tr>
                        <tr>
                            <td>Value</td>
                            <td style="text-align:right;"><?php echo number_format($total_value, 2); ?></td>
                        </tr>
                    </thead>
                </table>

                <?php
                    $total_quantity = 0;
                    $total_value = 0;
                    foreach($total_quantities_closing as $item)
                    {
                        $total_quantity += $item['total_quantity'];
                        if(isset($avg_price_purchase[$item['productID']]))
                        {
                            $value = $avg_price_purchase[$item['productID']];
                        }
                        else
                        {
                            $value = $avg_price_inventory[$item['productID']];
                        }
                        $total_value += $value * $item['total_quantity'];
                    }
                ?>
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Inventory(Closing)</th>
                        </tr>

                        <tr>
                            <td width="50%">Qty</td>
                            <td style="text-align:right;"><?php echo number_format($total_quantity, 2); ?></td>
                        </tr>

                        <tr>
                            <td width="50%">Value</td>
                            <td style="text-align:right;"><?php echo number_format($total_value, 2); ?></td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2">
            </div>
        </div>
    </section>
</section>
