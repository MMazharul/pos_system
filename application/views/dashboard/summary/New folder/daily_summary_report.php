<style>
    table tr td{
        //text-align: right;
        // font-weight: bold;
        border: 1px solid black !important;
    }
    table tr th{
        //text-align: right;
        font-weight: bold;
        border: 1px solid black !important;
    }

</style>

<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Daily Summary( <?php echo $date; ?>)
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-8">
                    
                </div>
              
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Expense</th>
                        </tr>
                        <?php
                        $total_expense = 0;
                        foreach ($daily_expense_closing_summary as $each_info):
                            $expense_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_expense_head', 'expheadID', $each_info['head_id']);
                            $total_expense += $each_info['total_expense'];
                            ?>
                            <tr>
                                <td  width="50%"><?php echo $expense_name['title']; ?></td>
                                <td style="text-align:right;"><?php echo number_format($each_info['total_expense'],2); ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td style="text-align:right;">Total:</td>
                            <td style="text-align:right;"><?php echo number_format($total_expense, 2); ?></td>
                        </tr>
                    </thead>
                </table>
               

        <div class="row">
            <div class="col-xs-2">
            </div>
        </div>
    </section>
</section>
