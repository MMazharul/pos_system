
<section class="content">
    <?php
    $success = $this->session->flashdata('success');
    if ($success) {
        ?> 
        <div class="box box-info">
            <div class="box-body">
                <div class="callout callout-info">
                    <?php
                    echo $success;
                    ?>
                </div>
            </div><!-- /.box-body -->
        </div>
        <?php
    }
    $failed = $this->session->flashdata('failed');
    if ($failed) {
        ?>

        <div class="box box-info">
            <div class="box-body">

                <b style="color: red;">
                    <?php
                    echo $failed;
                    ?>
                </b>

            </div><!-- /.box-body -->
        </div>

        <?php
    }
    ?>
    <div class="col-md-6 col-md-offset-3">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Monthly Summary</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo site_url('reports/monthly_summary_report'); ?>" method="post">
                <div class="box-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-8">
                                <div class="form-group has-feedback">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <select name="date"   class="form-control pull-right">
                                            <?php
                                            foreach($monthnames as $key=>$month){
                                            ?>
                                            <option value="<?php  echo $key;?>"><?php echo $month; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label>
                                <div class="input-group">
                                    <button type="submit" class="btn btn-primary" ><i class="fa fa-search"></i> &nbsp;Search</button>   
                                </div><!-- /.input group -->
                            </div>
                        </div>

                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">


                </div>
            </form>
        </div><!-- /.box -->
</section>
<script type="text/javascript">
    $("#item_name").autocomplete({
        source: 'new_report/report_controller/getproductname',
        select: function (event, ui) {
            $("#item_name").val(ui.item.value);
            $("#item_id").val(ui.item.id);
            return false;
        }
    });
</script>