<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo isset($title) ? $title: 'DOKANI'; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">


        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/fonts/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/fonts/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/fonts/css/jquery-ui.css">


        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- date-range-picker -->
<!--        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/bootstrap/fonts/css/moment.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!--        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-new-ui.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

        <style type="text/css">
            .main-sidebar {
                border-right: 1px solid #ddd;
            }
            .skin-black-light .content-wrapper, .skin-black-light .main-footer {
                border-left: 0px solid #d2d6de;
            }
            .center {
                text-align: center;
            }
            .right {
                text-align: right;
            }
        </style>
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#tbl1').DataTable({
                    orderable: false,
                    "ordering": false
                });
                //Initialize Select2 Elements
                $(".select2").select2();
                //bootstrap WYSIHTML5 - text editor
                $(".textarea").wysihtml5();
                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true,
                    endDate: '+0d',
                    format: 'yyyy-mm-dd'
                });
                $('#datepicker2').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                //Date range picker
                $('#reservation').daterangepicker({
                    locale: {
                        format: 'YYYY/MM/DD'
                    }
                });
            });
        </script>
    </head>
    <body class="hold-transition skin-black-light sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url(); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b></b>DOKAN</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b></b>DOKAN</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

					<div  align="" style="font-size:25px;font-weight:bold;" class="pull-left hidden-xs" >
                    <b>" يا رسول الله "</b>
					</div>
					<div  align="center" style="font-size:25px;font-weight:bold;" >
                    <b> " بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم" </b>

					 <div  <a href="" style="font-size:25px;font-weight:bold;"  class="pull-right hidden-xs">
                    <b> " يا الله " </b>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="<?php echo base_url('pos'); ?>"  title="SALES POINT( NEW SALE)" style="font-size:17px;font-weight:bold;"><span class="glyphicon glyphicon-shopping-cart"></span> SALES POINT</a>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class=""><?php echo $this->session->userdata('user_name'); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo base_url('settings/profile')?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url('login/logOut'); ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </nav>
            </header>

            <?php require_once('navigation.php'); ?>

            <div class="content-wrapper">
                <?php echo $content; ?>
            </div>

            <footer class="main-footer  no-print">
                <div class="pull-right hidden-xs">
                    <b>Version 1.3</b>
                </div>
                <strong>Copyright &copy;  <a href=""></a></strong> All rights
                reserved.
            </footer>
        </div>
        <script>
//         $(function(){
//   var delay = 2000;
//       $.ajax({
//           type: 'POST',
//           url: 'https://www.codexworld.com',
//           data: 'data_to_send',
//           success: function(response){
//               setTimeout(function() {
//                   $('#target').append(response);
//               }, delay);
//           }
//       });
//
// });
setInterval(function () { Request2(); }, 60000);

function Request2(){
  $.ajax({
           type: 'GET',
           url: '<?=base_url('/email')?>',
           data: 'data_to_send',
           success: function(response){

           }
       });
//add request 2 code here
}
        </script>
    </body>
</html>
