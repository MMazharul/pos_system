<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Pos Config <sup></sup></h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <div class="box-body">
                    <p style="text-align: center;font-size: 20px;font-weight: bold;">Pos 1</p>
                    <form action="<?php echo base_url('settings/PosConfigUpdate'); ?>" method="post">
                        <input value="<?php echo $config->configPosID; ?>" name="configPosID" type="hidden">
                        <div class="form-group has-feedback">
                            <label>Warehouse</label>
                            <select name="warehouseID" class="form-control select2" style="width: 100%;">
                                <?php foreach ($warehouses as $warehouse) { ?>
                                    <?php if ($config->warehouseID == $warehouse->warehouseID) { ?>
                                        <option value="<?php echo $warehouse->warehouseID; ?>"><?php echo $warehouse->warehouseName; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                <option value=""> Select Warehouse </option>
                                <?php foreach ($warehouses as $warehouse) { ?>
                                    <?php if ($config->warehouseID != $warehouse->warehouseID) { ?>
                                        <option value="<?php echo $warehouse->warehouseID; ?>"><?php echo $warehouse->warehouseName; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Account</label>
                            <select name="accountID" class="form-control select2" style="width: 100%;">
                                <?php foreach ($accounts as $account) { ?>
                                    <?php if ($config->accountID == $account->accountID) { ?>
                                        <option value="<?php echo $account->accountID; ?>"><?php echo $account->accountName; ?></option>
                                    <?php } ?>
                                <?php } ?>
                                <option value=""> Select Account </option>
                                <?php foreach ($accounts as $account) { ?>
                                    <?php if ($config->accountID != $account->accountID) { ?>
                                        <option value="<?php echo $account->accountID; ?>"><?php echo $account->accountName; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>


                        <div class="row">
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                            </div>

                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">

        </div>
        <div class="col-md-3"></div>
    </div>
</section>
<!-- /.content -->