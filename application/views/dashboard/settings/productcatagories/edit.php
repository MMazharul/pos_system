<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="box">
		        <div class="box-header with-border">
		          <h3 class="box-title">Edit Product Catagory</h3>
		          	<?php if($this->session->flashdata('msg')){?>
    
				        <?php echo $this->session->flashdata('msg'); ?>
				    
				    <?php }?>
		        </div>
		        <div class="box-body">
		          <form action="<?php echo base_url('settings/ProductCatagoryUpdate'); ?>" method="post">
				      <div class="form-group has-feedback">
				      <label>Product Catagory Name</label>
				        <input type="hidden" value="<?php echo $productcatagories[0]->product_catagoriesID; ?>" name="product_catagoriesID" >
				        <input type="hidden" value="<?php echo $productcatagories[0]->catagoryName; ?>" name="original-catagoryName" >
				        <input name="catagoryName" class="form-control" placeholder="Product Catagory Name" value="<?php echo $productcatagories[0]->catagoryName; ?>">
				      </div>
				      
				      <div class="row">
				        <div class="col-xs-4">
				          
				        </div>
				        <div class="col-xs-4">
				          
				        </div>
				        <!-- /.col -->
				        <div class="col-xs-4">
				          <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
				        </div>

				        <!-- /.col -->
				      </div>
				    </form>
		        </div>
		        <!-- /.box-body -->
		    </div>
		</div>
		<div class="col-md-4"></div>
	</div>
</section>
<!-- /.content -->