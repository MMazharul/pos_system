<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="box">
		        <div class="box-header with-border">
		          <h3 class="box-title">New Product Catagory</h3>
		          	<?php if($this->session->flashdata('msg')){?>
    
				        <?php echo $this->session->flashdata('msg'); ?>
				    
				    <?php }?>
		        </div>
		        <div class="box-body">
		          <form action="<?php echo base_url('settings/ProductCatagoryStore'); ?>" method="post">
				      <div class="form-group has-feedback">
				        <label>Product Catagory Name</label>
				        <input name="catagoryName" class="form-control" placeholder="Product Catagory Name">
				      </div>
				      
				      <div class="row">
				        <div class="col-xs-4">
				          
				        </div>
				        <div class="col-xs-4">
				          
				        </div>
				        <!-- /.col -->
				        <div class="col-xs-4">
				          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
				        </div>

				        <!-- /.col -->
				      </div>
				    </form>
		        </div>
		        <!-- /.box-body -->
		    </div>
		</div>
		<div class="col-md-4"></div>
	</div>
</section>
<!-- /.content -->