<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Product Catagories</h3>
				<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg');  }?>
				<?php if($this->session->flashdata('usingTransaction')){ echo $this->session->flashdata('usingTransaction');  }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>SL.</th>
                  <th>Product Catagory</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $sl =1;?>
                <?php foreach($productcatagories as $productcatagory){?>
                <tr>
                  <td><?php echo $sl; ?></td>
                  <td><?php echo $productcatagory->catagoryName; ?></td>
                  <td><a href="<?php echo base_url('settings/ProductCatagoryEdit'); ?>/<?php echo $productcatagory->product_catagoriesID; ?>" class="btn btn-primary btn-sm">Edit</a> <a href="<?php echo base_url('settings/ProductCatagoryDestroy'); ?>/<?php echo $productcatagory->product_catagoriesID; ?>" onclick="return confirm('Are you want to delete this product category');"  class="btn btn-danger btn-sm">Delete</a></td>
                </tr>
                <?php $sl++;?>
                <?php } ?>
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</div>
	<div class="col-md-3"></div>
</div>
</section>
<!-- /.content -->