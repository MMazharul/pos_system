<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Warehouses</h3>
      				<?php if($this->session->flashdata('msg')){?>

      				<?php echo $this->session->flashdata('msg'); ?>

      				<?php }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th style="width:5%;">SL.</th>
                  <th style="width:20%">Name</th>
                  <th style="width:24%;">Phone Number</th>
                  <th style="width:37%">Address</th>
                  <th style="width:14%;"></th>
                </tr>
                </thead>
                <tbody>
                <?php $sl =1;?>
                <?php foreach($warehouses as $warehouse){?>
                <tr>
                  <td><?php echo $sl; ?></td>
                  <td><?php echo $warehouse->warehouseName; ?></td>
                  <td><?php echo $warehouse->warehousePhone; ?></td>
                  <td><?php echo $warehouse->warehouseAddress; ?></td>
                  <td><a href="<?php echo base_url('settings/WarehouseEdit'); ?>/<?php echo $warehouse->warehouseID; ?>" class="btn btn-primary btn-sm">Edit</a> <a href="<?php echo base_url('settings/WarehouseDestroy'); ?>/<?php echo $warehouse->warehouseID; ?>" class="btn btn-danger btn-sm">Delete</a></td>
                </tr>
                <?php $sl++;?>
                <?php } ?>
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</div>
	<div class="col-md-1"></div>
</div>
</section>
<!-- /.content -->