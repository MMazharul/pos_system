<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="box">
		        <div class="box-header with-border">
		          <h3 class="box-title">New Warehouse</h3>
		          	<?php if($this->session->flashdata('msg')){?>
    
				        <?php echo $this->session->flashdata('msg'); ?>
				    
				    <?php }?>
		        </div>
		        <div class="box-body">
		          <form action="<?php echo base_url('settings/WarehouseStore'); ?>" method="post">
				      <div class="form-group has-feedback">
				      	<label>Warehouse Name</label>
				        <input name="warehouseName" class="form-control" placeholder="Warehouse Name">
				      </div>
				      <div class="form-group has-feedback">
				        <label>Warehouse Phone Number</label>
				        <input name="warehousePhone" class="form-control" placeholder="Warehouse Phone Number">
				      </div>
				      <div class="form-group has-feedback">
				      	<label>Warehouse Address</label>
				        <textarea class="textarea" name="warehouseAddress"  placeholder="Warehouse Address" style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
				      </div>
				      
				      <div class="row">
				        <div class="col-xs-4">
				          
				        </div>
				        <div class="col-xs-4">
				          
				        </div>
				        <!-- /.col -->
				        <div class="col-xs-4">
				          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
				        </div>

				        <!-- /.col -->
				      </div>
				    </form>
		        </div>
		        <!-- /.box-body -->
		    </div>
		</div>
		<div class="col-md-3"></div>
	</div>
</section>
<!-- /.content -->