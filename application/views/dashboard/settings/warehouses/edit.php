<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="box">
		        <div class="box-header with-border">
		          <h3 class="box-title">Edit Warehouse</h3>
		          	<?php if($this->session->flashdata('msg')){?>
    
				        <?php echo $this->session->flashdata('msg'); ?>
				    
				    <?php }?>
		        </div>
		        <div class="box-body">
		          <form action="<?php echo base_url('settings/WarehouseUpdate'); ?>" method="post">
		          <input type="hidden" name="warehouseID" value="<?php echo $warehouses[0]->warehouseID; ?>">
				      <div class="form-group has-feedback">
				      	<label>Warehouse Name</label>
				        <input type="hidden" name="original-warehouseName" value="<?php echo $warehouses[0]->warehouseName; ?>">
				        <input name="warehouseName" class="form-control" placeholder="Warehouse Name" value="<?php echo $warehouses[0]->warehouseName; ?>">
				      </div>
				      <div class="form-group has-feedback">
				        <label>Warehouse Phone Number</label>
				        <input name="warehousePhone" class="form-control" placeholder="Warehouse Phone Number" value="<?php echo $warehouses[0]->warehousePhone; ?>">
				      </div>
				      <div class="form-group has-feedback">
				      	<label>Warehouse Address</label>
				        <textarea class="textarea" name="warehouseAddress"  placeholder="Warehouse Address" style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $warehouses[0]->warehouseAddress; ?></textarea>
				      </div>
				      
				      <div class="row">
				        <div class="col-xs-4">
				          
				        </div>
				        <div class="col-xs-4">
				          
				        </div>
				        <!-- /.col -->
				        <div class="col-xs-4">
				          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
				        </div>

				        <!-- /.col -->
				      </div>
				    </form>
		        </div>
		        <!-- /.box-body -->
		    </div>
		</div>
		<div class="col-md-3"></div>
	</div>
</section>
<!-- /.content -->