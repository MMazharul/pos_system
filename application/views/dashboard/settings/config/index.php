<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
<div class="row">
		<div class="col-md-3">
			<div class="box">
		        <div class="box-header with-border">
		          <h3 class="box-title">Vat Rate <sup><?php echo $config->vatRate; ?>% </sup></h3>
		          	<?php if($this->session->flashdata('msg')){?>
    
				        <?php echo $this->session->flashdata('msg'); ?>
				    
				    <?php }?>
		        </div>
		        <div class="box-body">
		          <form action="<?php echo base_url('settings/updateStoreVatRate'); ?>" method="post">
				      <div class="form-group has-feedback">
				      	<label>Vat Rate (%)</label>
				        <input value="<?php echo $config->configID; ?>" name="configID" type="hidden">
				        <input value="<?php echo $config->vatRate; ?>" name="vatRate" class="form-control" placeholder="Vat Rate">
				      </div>
				      
				      
				      <div class="row">
				        <div class="col-xs-4">
				          
				        </div>
				        <div class="col-xs-4">
				          
				        </div>
				        <!-- /.col -->
				        <div class="col-xs-4">
				          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
				        </div>

				        <!-- /.col -->
				      </div>
				    </form>
		        </div>
		        <!-- /.box-body -->
		    </div>
		</div>
		<div class="col-md-6">
			
		</div>
		<div class="col-md-3"></div>
	</div>
</section>
<!-- /.content -->