<style>
    .divstyles{margin-top: 4px;}
</style>
<section class="content-header">
    <div class="box-body">
        <form action="<?php echo base_url('inventory/serarchInventroy'); ?>" method="post">
            <div class="row">
                <div class="col-md-3">
                    <label>Warehouse</label>
                    <div class="form-group">
                        <?php
                        $this->db->select('*');
                        $this->db->from('tbl_pos_warehouses');
                        $this->db->order_by("warehouseID", "desc");
                        $query = $this->db->get();
                        $result = $query->result_array();
//                        dumpVar($result);
                        ?>
                        <select name="warehouseID" class="form-control select2" style="width: 100%;">
                            <?php foreach ($result as $warehouse) { ?>
                                <option value="<?php echo $warehouse['warehouseID']; ?>"><?php echo $warehouse['warehouseName']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <label>Product Categories</label>
                    <div class="form-group">
                        <?php
                        $this->db->select('*');
                        $this->db->from('tbl_pos_product_catagories');
                        $query = $this->db->get();
                        $result = $query->result_array();
//                        dumpVar($result);
                        ?>
                        <select name="product_catagoriesID" class="form-control select2" style="width: 100%;">
                            <option value="">Product Category</option>
                            <?php foreach ($result as $value) { ?>
                                <option value="<?php echo $value['product_catagoriesID']; ?>"><?php echo $value['catagoryName']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <label>Product</label>
                    <div class="form-group ">
                        <?php
                        $this->db->select('*');
                        $this->db->from('tbl_pos_products');
                        $query = $this->db->get();
                        $results = $query->result_array();
//                        dumpVar($results);
                        ?>
                        <select name="productID" class="form-control select2" style="width: 100%;">
                            <option value="">Products</option>
                            <?php foreach ($results as $e_product) { ?>
                                <option value="<?php echo $e_product['productID']; ?>"><?php echo $e_product['productName'].'('.$e_product['productCode'].')'; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label></label>
                    <div class="form-group divstyles">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</section>
<?php

?>

<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Inventory

                    <small class="pull-right">Date: <?php echo $today = date("d-m-Y");?></small>
                </h2>

            </div>
        </div>
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
							<th style="width: 15%;">BarCode</th>
                                <th style="width: 30%;">Product Category</th>
                                <th style="width: 25%;">Product Name</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 20%;">Qty Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($inventory as $value) { ?>
                                <tr>
								<td><?php echo $value->productCode; ?></td>
                                    <td><?php echo $value->catagoryName; ?></td>
                                    <td><?php echo $value->productName; ?></td>
                                    <td><b><?php echo $value->ttlqty; ?></b></td>
                  
                                    <?php $avg_price = $this->INVENTORY->getProductPrice($value->productID); ?>
                                    <td><b><?php echo $avg_price + $value->ttlpurchasePrice ?></b></td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
		 <div class="row no-print">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> Print
            </button>
        </div>
    </div>
    </section>
</section>
