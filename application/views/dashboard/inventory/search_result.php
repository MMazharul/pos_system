<style>
    table tr th{text-align: center;
                border:1px solid grey!important;
                margin: 0px!important;
                padding: 0px!important;

    }
    table tr td{
        text-align: center;
        border:1px solid grey!important;
        margin: 0px!important;
        padding: 0px!important;
    }

</style>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <a href="<?php echo base_url('inventory/index')?>" class="btn btn-default btn-xs pull-right">
                        <span class="glyphicon glyphicon-step-backward"></span>Back
                    </a>
                </div>
            </div>
            <div class="col-xs-12">
                <h2 class="page-header">
                    Inventory
                    <small class="pull-right">Date: 2/10/2014</small>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 15%;">Store</th>
                            <th style="width: 20%;">Product Category</th>
                            <th style="width: 40%;">Product Name ( Barcode )</th>
                            <th style="width: 10%;">Qty</th>
                            <th style="width: 15%;">Qty Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $total_quantity = 0;
                            $total_value = 0;

                            foreach($inventroyseracrdatabyhouse as $value)
                            {
                                $avg_price = $this->INVENTORY->getProductPrice($value['productID']);
                                $total_quantity += $value['ttlbalance'];
                                $quantity_value = $value['ttlbalance'] * $avg_price;
                                $total_value += $quantity_value;
                                ?>
                            <tr>
                                <td>
                                    <?php
                                    $whouse_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_warehouses', 'warehouseID', $value['warehouseID']);
                                    echo $whouse_name['warehouseName'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $prodt_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $value['productID']);
                                    $id = $prodt_name['catagoryID'];
                                    $prodt_names = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_product_catagories', 'product_catagoriesID', $id);
                                    echo $prodt_names['catagoryName'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $prodt_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $value['productID']);
                                    echo $prodt_name['productName'].'('. $prodt_name['productCode'].')';
                                    ?>
                                </td>
                                <td class="text-right"><b><?php echo $value['ttlbalance']; ?></b></td>
                                <td class="text-right"><b><?php echo $quantity_value; ?></b></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th colspan="3" class="text-right">Total</th>
                            <th class="text-right">
                                <?php echo $total_quantity; ?>
                            </th>
                            <th class="text-right">
                                <?php echo $total_value; ?>
                            </th>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
		 <div class="row no-print">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> Print
            </button>
        </div>
    </div>
    </section>
</section>