<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DOKAN</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/hover.css">

<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->
<!--        <link rel="stylesheet" href="-->
<!--    --><?php //echo base_url(); ?><!--assets/bootstrap/fonts/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/fonts/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/fonts/css/jquery-ui.css">


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-new-ui.js"></script>

    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
    <style type="text/css">
        .myStyleCss {
            background: white
            height: 200px;
        }

        .displayingS {
            display: none
        }

        .inputStyle {
            height: 30px !important;
            text-align: center;

        }

        .inputStyleNested {
            height: 30px !important;
            text-align: center;
            width: 100%;
        }

        .tdStyle {
            text-align: center;
            padding-bottom: 5px !important;
            padding-top: 5px !important;
            width: 25%;
        }

        .tdStyleNested {
            text-align: center;
            padding-bottom: 5px !important;
            padding-top: 5px !important;

        }

        .thStyleNew {
            width: 30%;
            text-align: left;
            padding-bottom: 5px !important;
            padding-top: 5px !important;
            padding-left: 10px;

        }

        .tdStyleNew {
            width: 70%;
            text-align: center;
            padding-bottom: 5px !important;
            padding-top: 5px !important;
        }

        .appenTd {
            border-right: 2px solid #fff;
            border-bottom: 2px solid #fff;
        }

        td, th, div {
            text-transform: uppercase;
        }

        .search input {
            text-indent: 32px;
        }

        .search .glyphicon-search {
            position: absolute;
            top: 10px;
            left: 30px;
        }
    </style>
</head>
<body class="hold-transition skin-black-light layout-top-nav">
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header pull-left">
                    <a href="<?php echo base_url('pos'); ?>" class="navbar-brand"><b>DOKAN</b></a>
                </div>
                <div class="navbar-custom-menu pull-right">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="<?php echo base_url(); ?>"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class=""><?php echo $this->session->userdata('user_name'); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo base_url('settings/profile') ?>"
                                           class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo base_url('login/logOut'); ?>"
                                           class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="content-wrapper">
        <div class="container-fluid">


            <form action="<?php echo base_url('pos/store'); ?>" method="post">

                <section class="content">
                    <div class="row">
                        <div class="col-sm-12" style="background: #fff">
                            <div class="col-sm-12" style="margin-top:15px;">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="row">
                                            <div class="col-md-6" style="">
                                                <div class="form-group">
                                                    <select disabled="disabled" class="form-control select2"
                                                            style="width: 100%;">
                                                        <option>Walk In Customer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="">
                                                <div class="form-group">

                                                    <select readonly name="warehouseID"
                                                            class="form-control select2"
                                                            style="width: 100%;">
                                                        <?php foreach ($warehouses as $warehouse) { ?>
                                                            <?php if ($posConfig->warehouseID == $warehouse->warehouseID) { ?>
                                                                <option value="<?php echo $warehouse->warehouseID; ?>"><?php echo $warehouse->warehouseName; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>


                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="row">
                                            <div class="col-md-2"
                                                 style="padding-top:5px;font-weight:bold;text-align:right;">
                                                Invoice
                                                Date
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input readonly="" value="<?php echo date('Y-m-d'); ?>"
                                                           id="datepicker"
                                                           name="invoiceDate" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-2"
                                                 style="padding-top:5px;font-weight:bold;text-align:right;">
                                                Sales
                                                Date
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input readonly="" value="<?php echo date('Y-m-d'); ?>"
                                                           id="datepicker1"
                                                           name="saleDate" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-2 " style="text-align:right;">
                                                <div class="row">
                                                    <a href="<?php echo site_url('pos'); ?>"
                                                       class="btn btn-danger btn-sm"><i
                                                                class="glyphicon glyphicon-refresh"></i> Clear Sale</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding-right:5px;">
                                        <div class="form-group">
                                            <div class="search">
                                                <span class="glyphicon glyphicon-search"></span>
                                                <input id="productName" class="form-control"
                                                       placeholder="Scan/Search Product by Name/Code">
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-8 myStyleCss">
                                    <div class="row" style="background: #fff;min-height:420px;border:1px solid #fff;">
                                        <table class="table" style="background: #eee; border:2px solid #fff !important;"
                                               rules="all"
                                        >
                                            <thead>
                                            <tr>
                                                <th style="width: 35%;text-align:center;background: #737373;color:white;padding:10px;">
                                                    Product Information
                                                </th>
                                                <th style="10%;text-align:center;background: #737373;color:white;padding:10px;">
                                                    Price
                                                </th>
                                                <th style="10%;text-align:center;background: #737373;color:white;padding:10px;">
                                                    Purchase Price
                                                </th>


                                                <th style="15%;text-align:center;background: #737373;color:white;padding:10px;">
                                                    Qty
                                                </th>
                                                <th style="15%;text-align:center;background: #737373;color:white;padding:10px;">
                                                    Subtotal
                                                </th>
                                                <th style="15%;text-align:center;background: #737373;color:white;padding:10px;">
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="tableDynamic">
                                            <tr id="emptyProduct">
                                                <td colspan="6"
                                                    style="text-align:center;font-weight:bold;padding:10px;">Please
                                                    Scan/Search Product Name/Code
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>


                                <div class="col-md-4"
                                     style="background-color:#f2f2f2;min-height:420px;border:1px solid #fff;">
                                    <table style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <th class="thStyleNew">
                                                Sub Total
                                            </th>
                                            <td class="tdStyleNew">
                                                <input id="subTotal" readonly type="text" value="0" name="subTotal"
                                                       class="form-control inputStyle">
                                            </td>
                                        </tr>
                                          <tr>

                                            <th class="thStyleNew">
                                                Vat <?php echo $config->vatRate; ?>(%)
                                            </th>
                                            <td class="tdStyleNew">
                                                <input autocomplete="off" id="vatAmount"
                                                       class="form-control inputStyle" value="0" name="vat">

                                                <input type="hidden" id="vatRate" name="vatPertan"
                                                       value="<?php echo $config->vatRate; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="thStyleNew"style="color: red;" style="vertical-align:top;">D.00000</th>
                                            <td style="border-top: medium none;">
                                                <label for="taka">&#2547;</label>
                                                <input checked="" type="radio" id="taka" name="discountType"
                                                       value="0" readonly>
                                               <!-- <label for="percent" style="padding-left: 20px;">&#37;</label>
                                                <input type="radio" id="percent" name="discountType" value="1">-->

                                                 <p id="percentP" style="display: none;">
                                                    <label for="discountPercent">Percent(%)</label>
                                                    <input placeholder="10" name="discountPercent" id="discountPercent"
                                                           class="form-control">
                                                </p>
                                                <p>
                                                    <input name="discount" id="discount" class="form-control inputStyle"
                                                           value="0" readonly>
                                                </p>
                                            </td>
                                        </tr>


                                        <tr>

                                            <th class="thStyleNew" style="color: green;">যত টাকা দরদার হল</th>

                                            <th class="">
                                                <input id="cus_amount" type="text" class="form-control"
												name="cus_amount"value=".00"
												style="text-align: center;">
                                            </th>
											</td>
                                        </tr>
										<tr>

                                            <th class="thStyleNew">Grand Total</th>

                                            <th class="tdStyleNew">
                                                <input id="totalAmount" type="text"
                                                       class="form-control inputStyle"
                                                       readonly=""
                                                       name="totalAmount"
                                                >
                                            </th>
											</td>
                                        </tr>
                                        <tr>

                                            <th class="thStyleNew">Payment By</th>
                                            <th class="tdStyleNew">
                                                <input type="radio" id="id_radio1" value="1"
                                                       name="r1" class="minimal" checked> Cash
                                               <!-- <input type="radio" id="id_radio2" value="2"
                                                       name="r1" class="minimal"> Card  -->

                                            </th>
                                        </tr>


                                        <tr>
                                            <td id="div2" class="displayingS" colspan="2">
                                                <table style="width:100%">
                                                    <tr>
                                                        <th class="thStyleNew">Extra (%)</th>
                                                        <th class="tdStyleNew"><input type="text"
                                                                                      id="extraamount_card"
                                                                                      class="form-control commontVariable inputStyle"
                                                                                      name="extra" value="0"
                                                                                      placeholder="%"
                                                            >
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th class="thStyleNew">Total Amount</th>
                                                        <th class="tdStyleNew"><input type="text" id="ttamountCard"
                                                                                      class="form-control inputStyle"
                                                                                      readonly=""
                                                                                      name="ttamountCard"
                                                            >
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>


                                        </tr>
                                        <tr id="div3" class="displayingS ">
                                            <th class="thStyleNew">Card Number</th>
                                            <th class="tdStyleNew"><input id="CardNum" type="text"
                                                                          class="form-control inputStyle"
                                                                          name="CardNum"
                                                >
                                            </th>
                                        </tr>

                                        <tr>

                                            <td colspan="2" id="div4" class="displayingS">

                                                <table style="width:100%;">
                                                    <tr>
                                                        <th class="thStyleNew">Total Paying</th>
                                                        <th class="tdStyleNew"
                                                        >
                                                            <input type="text"
                                                                   class="form-control commontVariable"
                                                                   id="Amount_forCard"
                                                                   name="totalAmount_forCard"
                                                                   style="text-align:center;">
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" style="padding-top:5px !important;">
                                                            <button name="submitBtn" type="submit" id="paymentCard"
                                                                    class="btn btn-block btn-success subBtn "
                                                                    onclick="return checkadd();"><i
                                                                        class="glyphicon glyphicon-saved"></i> PAYMENT
                                                                NOW
                                                            </button>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td id="div1" colspan="2">
                                                <table style="width:100%">
                                                    <tr>
                                                        <th class="thStyleNew"style="color: green;" >যত টাকা দিল</th>
                                                        <th class="tdStyleNew"
                                                        ><input id="totalPaying" type="text"
                                                                class="form-control inputStyle"
                                                                name="totalPaying"
                                                                style="padding:5px;text-align: center;"
                                                                autocomplete="off" readonly></th>
                                                    </tr>
                                                    <tr>
                                                        <th class="thStyleNew">Return/Change</th>

                                                        <th class="tdStyleNew"
                                                        ><input id="balance" type="text"
                                                                class="form-control inputStyle"
                                                                readonly="" name="balance"
                                                                style="padding:5px;text-align: center;">
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th class="thStyleNew">Note</th>

                                                        <th class="tdStyleNew"
                                                        >
                                                        <textarea id="note"
                                                                class="form-control"
                                                                 name="note"></textarea>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>


                                        </tr>

                                        <tr id="div5">
                                            <th colspan="4">
                                                <button name="submitBtn" type="submit" disabled="" id="payment"
                                                        class="btn btn-block btn-success subBtn"
                                                        onclick="return checkadd();"><i
                                                            class="glyphicon glyphicon-saved "></i> PAYMENT NOW
                                                </button>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </section>
            </form>
        </div>
    </div>
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="pull-right hidden-xs">
                <b>Version 1.3</b>
            </div>
            <strong>Copyright &copy; <a href=""></a></strong> All rights reserved.
        </div>
    </footer>
</div>


<script>
    $(function () {
        $('#datepicker').datepicker({
            autoclose: true,
            endDate: '+0d',
            format: 'yyyy-mm-dd'
        });
        $('#datepicker1').datepicker({
            autoclose: true,
            endDate: '+0d',
            format: 'yyyy-mm-dd'
        });

    });
</script>
<script>
    $('input[name="discountType"]').on('change', function () {

        var val = $('input[name="discountType"]:checked').val();
        if (val == 1) {
            $('#percentP').show();
            $('#discount').attr('readonly', true);
        } else {
            $('#percentP').hide();
            $('#discount').attr('readonly', false);
        }
    });

    $(document).on("keyup", "#cus_amount", function (event) {

        var subTotal = parseFloat($("#subTotal").val());
        var discount = parseFloat($("#discount").val());
        var cus_amount = parseFloat($("#cus_amount").val());
        var totalpay = $("#totalPaying").val(cus_amount);
        var vatAmount = parseFloat($("#vatAmount").val());
        var total = subTotal + vatAmount;

        var newDis = parseFloat(subTotal-cus_amount);

        $("#discount").val(newDis);
        var newTotal = total.toFixed(2);
        var gandTotal = (newTotal - newDis).toFixed(2);
        $("#totalAmount").append().val(gandTotal);
});


    $(document).on("keyup", "#discountPercent", function (event) {

        var subTotal = parseFloat($("#subTotal").val());
        var vatAmount = parseFloat($("#vatAmount").val());
        var total = subTotal + vatAmount;
        var discountPerchanage = parseFloat($(this).val());
        if (isNaN(total)) {
            alert('Please Entry Item Information');
            $("#discount").append().val(0);
            $("#discountPercent").append().val(0);
            $("#totalAmount").append().val(0);
        } else if (isNaN(discountPerchanage)) {
            $("#discount").append().val(0);
            $("#discountPercent").append().val('');
        } else {
            var discountPercent = parseFloat($(this).val());

            var discountAmount = discountPercent / 100 * total;
            var newDis = discountAmount.toFixed(2);
            $("#discount").val(newDis);
            var newTotal = total.toFixed(2);
            var gandTotal = (newTotal - newDis).toFixed(2);
            $("#totalAmount").append().val(gandTotal);


        }

    });
</script>

<script type="text/javascript">
    $(function () {
        $(document).on("keyup", ".commontVariable", function (event) {



            var totalAmount = parseFloat($("#totalAmount").val());
            if (isNaN(totalAmount)) {
                alert('Please Entry Item Information');
                $("#extraamount_card").append().val(0);
            } else {
                var cardAmount = parseFloat($("#Amount_forCard").val());
                var extraAmount = parseFloat($("#extraamount_card").val());
                if (!isNaN(extraAmount)) {
                    var parcentage = totalAmount * extraAmount / 100;
                    var ttamount = totalAmount + parcentage;
                    $("#ttamountCard").val(Math.round(ttamount));
                    $("#Amount_forCard").val(Math.round(ttamount));

                } else {
                    $("#ttamountCard").val('');
                    $("#Amount_forCard").val('');
                }
            }
        });


        var selectedClass = "";
        $(".fil-cat").click(function () {
            selectedClass = $(this).attr("data-catagory");
            //console.log(selectedClass);
            $(".productsRow .col-md-2").not("." + selectedClass).fadeOut();
            $(".productsRow").fadeTo(100, 0.1);
            $(".productsRow .col-md-2").not("." + selectedClass).fadeOut();
            setTimeout(function () {
                $("." + selectedClass).fadeIn();
                $(".productsRow").fadeTo(300, 1);
            }, 300);

        });


        $("input, textarea, select").keypress(function (event) {
            if (event.which == '10' || event.which == '13') {
                event.preventDefault();
            }
        });


        $("#productName").autocomplete({



            source: function (request, response) {
                $.getJSON("<?php echo base_url('sales'); ?>/get_product_list_by_branch", {term: request.term},
                    response);
            },
            response: function (event, ui) {
                if (ui.content) {

                    if (ui.content.length == 1) {
                        addRowProduct(ui.content[0].id, ui.content[0].inventory, ui.content[0].productPrice,ui.content[0].purchasePrice, ui.content[0].productName, ui.content[0].warehouseID, ui.content[0].productCode, ui.content[0].productModel);
                        var dataIns = $(this).val();
                        $(this).val('');
                        $(this).focus();
                        $(this).autocomplete('close');
                        return false;
                    } else if (ui.content.length == 0) {


                        $(this).val('');
                        $(this).focus();
                        $(this).autocomplete('close');
                        return false;
                    } else {
                        //alert("This Character and code have no item!!");
                    }
                }
            },
            select: function (event, ui) {

                addRowProduct(ui.item.id, ui.item.inventory, ui.item.productPrice,ui.item.purchasePrice,ui.item.productName, ui.item.warehouseID, ui.item.productCode, ui.item.model);
                $(this).val('');
                return false;
            }

        });

        var findTotal = function () {
            if ($("#tableDynamic tr").size() == 0) {
                return $("#totalAmount").val(0);
            }
            $("#tableDynamic tr").each(function () {
                row_total = 0;
                $(".totalprice").each(function () {
                    row_total += Number(parseFloat($(this).text()));
                });

                if (!isNaN(row_total)) {
                    vatRate = parseFloat($("#vatRate").val());

                    vatAmount = row_total / 100 * vatRate;
                    vatAmount = Math.round(vatAmount * 100) / 100;
                    var discount = $("#discount").val();
                    $("#subTotal").val(row_total);
                    $("#vatAmount").val(vatAmount);

                    let paid = $("#totalPaying").val((row_total + vatAmount) - discount);
                    if(paid)
                    {
                      totalPaying();
                    }
                    return $("#totalAmount").val((row_total + vatAmount) - discount);

                }
            });
        };

        var addRowProduct = function (id, inventory, price,purchasePrice,value, warehouseID, productCode, model) {
            $("#emptyProduct").hide();
            if (inventory <= 0) {
                alert(value + ' Current stock is: ' + inventory);

            } else {
                var productID = parseFloat($('#productID_' + id).val());

                var productQuantity = parseInt($('#qty_' + id).val());

                var productInventory = parseFloat($("#inventory_" + id).val());
                var productPrice = parseFloat($("#price_" + id).val());

                //console.log(productInventory);
                if (productID == id) {

                    if (productInventory >= productQuantity + 1) {
                        $('#qty_' + id).val(productQuantity + 1);

                        if (!isNaN(productPrice)) {
                            var total = (productQuantity + 1) * productPrice;
                            $('#total_' + id).text(total);
                        }
                        return findTotal();

                    } else {
                        return findTotal();

                    }

                }

                if (model === undefined) {
                    txt = "";
                } else {
                    txt = '[' + model + ']';
                }


                $('<tr>\n\
                        <td style="width:35%;" class="appenTd">' + value + ' [ ' + productCode + ']   ' + txt + '</td>\n\
                        <td style="text-align:center;width:10%;" class="appenTd"><input id="price_' + id + '" name="price[]" class="si_price form-control" value="' + price + '" type="text"></td>\n\
                        <td style="text-align:center;width:10%;" class="appenTd"><input id="purchase_price_' + id + '" name="purchasePrice[]" class="si_purchase_price form-control" value="' + purchasePrice + '" type="text"></td>\n\
                        <td class="appenTd" style="text-align:center;width:15%;height:30px;padding-top:2px;padding-bottom:0px;"><input id="qty_' + id + '" type="text" class="quantity form-control" name="qty[]" value="1" style="text-align: center;height:30px;" required autocomplete="off">\n\
                          <input id="productID_' + id + '" name="productID[]" value="' + id + '" type="hidden">\n\
                          <input id="warehouseID" name="warehouseID[]" value="' + warehouseID + '" type="hidden">\n\
                          <input id="inventory_' + id + '" value="' + inventory + '" type="hidden">\n\
                        </td>\n\
                        <td style="width:15%;text-align:center;"  class="totalprice appenTd" style="width:20%" id="total_' + id + '">' + price * 1 + '</td>\n\
                        <td class="appenTd" style="width:15%;text-align:center;padding-top:5px;padding-bottom:0px;"><button  class="btn btn-danger btn-xs" id="removeRow"><i class="glyphicon glyphicon-trash" style="cursor:pointer;"></i></button></td>\n\
                        </tr>').appendTo('#tableDynamic');

                return findTotal();
            }
        };

        $(document).on("click", "#removeRow", function (e) {

            var target = e.target;
            $(target).closest('tr').remove();
            findTotal();
            $('#discount').val('');
            $('#totalPaying').val('');
            $('#balance').val('');


            return $('#payment').prop('disabled', true);


        });

        $(document).on("keyup", ".quantity", function (event) {

            var quantity = parseFloat($(this).val());
            var id_arr = $(this).attr('id');
            var id = id_arr.split("_");
            var element_id = id[id.length - 1];
            var inventory = parseFloat($("#inventory_" + element_id).val());
            var price = parseFloat($("#price_" + element_id).val());
            $("#totalPaying").append().val('');
            $("#balance").append().val('');
            if (!isNaN(quantity)) {
                var payType = $("input[type='radio'].minimal:checked").val();
                if (payType == '1') {
                    $("#totalPaying").attr("required", true);
                    $("#CardNum").attr("required", false);
                    $("#Amount_forCard").attr("required", false);
                }else {
                    $("#CardNum").attr("required", true);
                    $("#Amount_forCard").attr("required", true);
                    $("#totalPaying").attr("required", false);
                }
                if (quantity > inventory) {
                    alert("Sorry! Not have enough products.");
                    $(this).css("border", "2px solid red");
                    if (payType == '1') {
                        document.getElementById('payment').type = 'button';
                    } else {
                        document.getElementById('paymentCard').type = 'button';
                    }
                    findTotal();
                    } else {

                    $("#total_" + element_id).text(price * quantity);
                    $(this).css("border", "1px solid #ddd");

                    if (payType == '1') {
                        document.getElementById('payment').type = 'submit';
                    }
                    else {
                        document.getElementById('paymentCard').type = 'submit';
                    }
                    findTotal();
                }
            }
        })
        ;

        $(document).on("keyup", ".si_price", function (event) {

            var id_arr = $(this).attr('id');
            var id = id_arr.split("_");
            var element_id = id[id.length - 1];
           //alert(element_id);
            var inventory = parseFloat($("#inventory_" + element_id).val());

            var quantity = parseFloat($("#qty_" + element_id).val());

            var price = parseFloat($(this).val());

            $("#total_" + element_id).text(price * quantity);
            findTotal();
        })
        ;


        $('#discount').keyup(function () {
            findTotal();
            var discount = parseFloat(this.value);
            var subTotal = parseFloat($("#subTotal").val());
            var vatAmount = parseFloat($("#vatAmount").val());
            var total = subTotal + vatAmount;

            if (!isNaN(discount)) {
                return $("#totalAmount").val(total - discount);
            } else {
                findTotal();
            }

        });


        $('#totalPaying').keyup(function () {

            totalPaying();
        });

        var totalPaying = function () {
            var totalPaying = parseFloat($("#totalPaying").val());
            var total = parseFloat($("#totalAmount").val());

            if (!isNaN(total)) {
                if (!isNaN(totalPaying)) {
                    var balance = totalPaying - total;
                    $("#balance").val(Math.round(balance * 100) / 100);
                } else {
                    $("#balance").val('');
                    return $('#payment').prop('disabled', true);
                }

                if ($("#balance").val() >= 0) {
                    return $('#payment').prop('disabled', false);
                } else {
                    return $('#payment').prop('disabled', true);
                }
            } else {
                $("#totalPaying").val('');
            }

        };

        $(document).on("keyup", "#vatAmount", function (event) {

            var vatAmount = parseFloat(this.value);
            var subTotal = parseFloat($('#subTotal').val());
            var discount = parseFloat($('#discount').val());
            var totalPaying = parseFloat($('#totalPaying').val());
            var balance = parseFloat($('#balance').val());

            if (isNaN(vatAmount)) {
                vatAmount = 0;
            }
            if (isNaN(discount)) {
                discount = 0;
            }
            if (isNaN(totalPaying)) {
                totalPaying = 0;
            }

            $("#totalAmount").val(subTotal + vatAmount - discount);
            if (totalPaying > 0) {
                var remain = subTotal + vatAmount - discount;
                var updatedBalance = totalPaying - remain;
                $('#balance').val(Math.round(updatedBalance * 100) / 100);
            }
        });

    })
    ;

    var checkadd = function () {
        var chk = confirm("Are you sure to add this record ?");
        if (chk) {
            return true;
        } else {
            return false;
        }

    };


</script>
<script>
    $(document).ready(function () {

        $('#id_radio1').click(function () {
            $('#div2').hide('fast');
            $('#div3').hide('fast');
            $('#div4').hide('fast');
            $('#div1').show('fast');
            $('#div5').show('fast');
            $("#totalPaying").attr("required", true);
            $("#CardNum").attr("required", false);


        });
        $('#id_radio2').click(function () {
            $('#div1').hide('fast');
            $('#div5').hide('fast');
            $('#div3').show('fast');
            $('#div2').show('fast');
            $('#div4').show('fast');
            $("#CardNum").attr("required", true);
            $("#Amount_forCard").attr("required", true);
            $("#totalPaying").attr("required", false);


            var totalAmount = parseFloat($("#totalAmount").val());
            if (isNaN(totalAmount)) {
                alert('Please Entry Item Information');
                $("#extraamount_card").append().val(0);
            } else {
                var cardAmount = parseFloat($("#Amount_forCard").val());
                var extraAmount = parseFloat($("#extraamount_card").val());
                var parcentage = totalAmount * extraAmount / 100;
                var ttamount = totalAmount + parcentage;
                $("#ttamountCard").val(Math.round(ttamount));
                $("#Amount_forCard").val(Math.round(ttamount));
            }


        });
    });
    $( document ).ready(function() {
        $("#subTotal").val('0');
        $("#totalAmount").val('0');
        $("#balance").val('0');
    });
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

</body>
</html>
