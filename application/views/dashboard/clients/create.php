<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="box">
		        <div class="box-header with-border">
		          <h3 class="box-title">New Client</h3>
		          	<?php if($this->session->flashdata('msg')){?>
    
				        <?php echo $this->session->flashdata('msg'); ?>
				    
				    <?php }?>
		        </div>
		        <div class="box-body">
		          <form action="<?php echo base_url('clients/store'); ?>" method="post">
				      <div class="form-group has-feedback">
				      	<label>Client Name</label>
				        <input name="clientName" class="form-control" placeholder="Client Name">
				      </div>
				      <div class="form-group has-feedback">
				      	<label>Client Email</label>
				        <input type="email" name="clientEmail" class="form-control" placeholder="Client Email">
				      </div>
				      <div class="form-group has-feedback">
				        <label>Client Phone Number</label>
				        <input name="clientPhone" class="form-control" placeholder="Client Phone Number">
				      </div>
				      <div class="form-group has-feedback">
				      	<label>Client Address</label>
				        <textarea class="textarea" name="clientAddress"  placeholder="Client Address" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
				      </div>
				      
				      <div class="row">
				        <div class="col-xs-4">
				          
				        </div>
				        <div class="col-xs-4">
				          
				        </div>
				        <!-- /.col -->
				        <div class="col-xs-4">
				          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
				        </div>

				        <!-- /.col -->
				      </div>
				    </form>
		        </div>
		        <!-- /.box-body -->
		    </div>
		</div>
		<div class="col-md-3"></div>
	</div>
</section>
<!-- /.content -->