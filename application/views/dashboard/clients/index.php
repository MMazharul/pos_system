<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Clients</h3>
      				<?php if($this->session->flashdata('msg')){?>

      				<?php echo $this->session->flashdata('msg'); ?>

      				<?php }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th style="width:5%;">SL.</th>
                  <th style="width:20%">Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Address</th>
                  <th style="width:15%;"></th>
                </tr>
                </thead>
                <tbody>
                <?php $sl =1;?>
                <?php foreach($clients as $client){?>
                <tr>
                  <td><?php echo $sl; ?></td>
                  <td><?php echo $client->clientName; ?></td>
                  <td><?php echo $client->clientEmail; ?></td>
                  <td><?php echo $client->clientPhone; ?></td>
                  <td><?php echo $client->clientAddress; ?></td>
                  <td><a href="<?php echo base_url('clients/edit'); ?>/<?php echo $client->clientID; ?>" class="btn btn-primary btn-sm">Edit</a> <a href="<?php echo base_url('clients/destroy'); ?>/<?php echo $client->clientID; ?>" class="btn btn-danger btn-sm">Delete</a></td>
                </tr>
                <?php $sl++;?>
                <?php } ?>
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</div>
	<div class="col-md-1"></div>
</div>
</section>
<!-- /.content -->