<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Monthly Statement Report
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-8">
                    <h5><b>Report Month  : <?php echo date('M-Y', strtotime($report_date)); ?></b></h5>
                </div>
                <div class="col-xs-4">
                    <button type="button" onclick="window.print();" class="no-print btn  btn-success pull-right"><i class="fa fa-print"></i> Print</button>

<!--                    <a href="<?php echo site_url('reports/datewiseExpensedetails_exportbyxls'); ?>" class="btn btn-app pull-right">
               <i class="fa fa-save"></i> EXCEL
           </a>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center;font-weight: bold;">Statement</th>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Opening Summary</th>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Present Summary</th>
                            <th style="text-align: center;font-weight: bold;" colspan="2">Closing Summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center;font-weight: bold;">#</td>
                            <td style="text-align: center;font-weight: bold;">Qty</td>
                            <td style="text-align: center;font-weight: bold;">Value</td>
                            <td style="text-align: center;font-weight: bold;">Qty</td>
                            <td style="text-align: center;font-weight: bold;">Value</td>
                            <td style="text-align: center;font-weight: bold;">Qty</td>
                            <td style="text-align: center;font-weight: bold;">Value</td>
                        </tr>
                        <tr style="text-align: center;" class="success">
                            <td>Purchases</td>
                            <td><?php echo number_format($purchases_pre_stock['totalurchaQTY'], 2); ?></td>
                            <td><?php echo number_format($purchases_pre_stock['totalurchaQTY'] * $purchases_pre_stock['totalPurchasesPrice'], 2); ?></td>
                            <td><?php echo number_format($purchases_today_stock['totalurchaQTY'], 2); ?></td>
                            <td><?php echo number_format($purchases_today_stock['totalurchaQTY'] * $purchases_today_stock['totalPurchasesPrice'], 2); ?></td>
                            <td><?php echo number_format($purchases_closing_stock['totalurchaQTY'], 2); ?></td>
                            <td><?php echo number_format($purchases_closing_stock['totalurchaQTY'] * $purchases_closing_stock['totalPurchasesPrice'], 2); ?></td>
                        </tr>
                        <tr style="text-align: center;" class="danger">
                            <td>Sales</td>
                            <td><?php echo number_format($sales_pre_stock_inventory['total_sales_quantity'], 2); ?></td>
                            <td><?php echo number_format(abs($sales_pre_stock_inventory['total_sales_quantity']) * $sales_pre_stock['total_avg_sales_price'], 2); ?></td>
                            <td><?php echo number_format($sales_today_inventory['total_sales_quantity'], 2); ?></td>
                            <td><?php echo number_format(abs($sales_today_inventory['total_sales_quantity']) * $sales_today_stock['total_avg_sales_price'], 2); ?></td>
                            <td><?php echo number_format($sales_closing_inventory['total_sales_quantity'], 2); ?></td>
                            <td><?php echo number_format(abs($sales_closing_inventory['total_sales_quantity']) * $sales_closing_stock['total_avg_sales_price'], 2); ?></td>
                        </tr>
                        <tr style="text-align: center;" class="info">
                            <td>Inventory</td>
                            <td><?php echo number_format($inventory_pre_stock, 2); ?></td>
                            <td><?php echo number_format($inventory_pre_stock * $inventory_avg_price_stock, 2); ?></td>
                            <td><?php echo number_format($inventory_today_stock, 2); ?></td>
                            <td><?php echo number_format($inventory_today_stock * $inventory_avg_price_stock, 2); ?></td>
                            <td><?php echo number_format($inventory_closing_stock, 2); ?></td>
                            <td><?php echo number_format($inventory_closing_stock * $inventory_avg_price_stock, 2); ?></td>
                        </tr>
                        <tr style="text-align: center;" class="warning">
                            <td>Expense</td>
                            <td colspan="2"><?php echo number_format(abs($total_expense_pre_Amount), 2); ?></td>
                            <td colspan="2"><?php echo number_format(abs($total_expense_today_Amount), 2); ?></td>
                            <td colspan="2"><?php echo number_format(abs($total_expense_closing_Amount), 2); ?></td>
                        </tr>
                        <tr style="text-align: center;" class="success">
                            <td>Account</td>
                            <td colspan="2"><?php echo number_format($total_pre_Amount, 2); ?></td>
                            <td colspan="2"><?php echo number_format($total_today_Amount, 2); ?></td>
                            <td colspan="2"><?php echo number_format($total_closing_Amount, 2); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2">
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>
