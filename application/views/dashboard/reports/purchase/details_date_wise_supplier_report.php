<style>
    @media print
    {    
        .no-print
        {
            display: none !important;
        }
        a[href]:after {
             content: none !important;
         }
    }

    #tableInfo tr th{text-align: center;
                border:1px solid grey!important;
                margin: 0px!important;
                padding: 0px!important;

    }
    #tableInfo tr td{
        text-align: center;
        border:1px solid grey!important;
        margin: 0px!important;
        padding: 0px!important;
    }
    address p {
        margin: 0px;
    }

</style>

<section class="content-header">

</section>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Supplier Ledger
                    <small class="pull-right" id="today"><b>Date:</b> <?php echo date('Y-m-d'); ?></small>
                </h2>
                <div class="col-xs-9">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:40%;float:left;">
                                Supplier
                                    <address>
                                    <strong><?php echo $supplierInfo['supplierName']; ?></strong><br>
                                    Phone: <?php echo $supplierInfo['supplierPhone']; ?><br>
                                    Email: <?php echo $supplierInfo['supplierEmail']; ?><br>
                                    <?php echo $supplierInfo['supplierAddress']; ?><br>
                                    </address>
                            </td>


                        </tr>
                    </table>
                    <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                    <h5><b>Present Balance: <?php if($supplierPresentVal < 0){ $epxData=explode("-",$supplierPresentVal); echo $epxData[1];  }else{ echo $supplierPresentVal; }
                    echo " [";
                            if ($supplierPresentVal < 0) {
                            echo '<span style="color:green;">Advanced</span>';
                            } else if ($supplierPresentVal == 0) {

                            echo '<span style="color:blue;"> Balance</span>';
                            } else {
                            echo '<span style="color:red;">Due </span>';
                            }
                            ?>]
                        </b></h5>
                </div>
                <div class="col-xs-3 no-print">
                    <div class="col-xs-6">
                        <button type="button" onclick="window.print();" class="no-print btn btn-block btn-primary btn-flat"><i class="fa fa-print"></i> Print</button>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo site_url('reports/datewiseSupplierdetails_exportbyxls'); ?>" class="btn btn-success pull-right">
                            <i class="fa fa-save"></i> EXCEL
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped" id="tableInfo" style="width:49%;float:left;margin-right:1%;">
                    <thead>
                        <tr>
                            <td colspan="5" style="font-weight:bold;font-size: 16px;">
                                Purchase History
                            </td>
                        </tr>
                        <tr>
                            <th>Purchase ID</th>
                            <th>Purchase Date</th>

                            <th>Grand Total</th>
                            <th>Discount</th>
                            <th>Net Total</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $gttl = 0;
                        $dscttl = 0;
                        $ttlpurchase = 0;
                        $ttldue = 0;
                        $ttlpay = 0;
                        $due = 0;
                        foreach ($supplierreport as $report) {
                            ?>
                            <tr>
                                <td><a href="<?php echo base_url('purchases/show') ?>/<?php echo $report['purchaseNo']; ?>"><?php echo $report['purchaseNo']; ?></a></td>
                                <td><?php echo $report['purchaseDate']; ?></td>

                                <td><?php
                                    echo $report['subTotal'];
                                    $gttl += $report['subTotal'];
                                    ?></td>
                                <td ><?php
                                    echo $report['discount'];
                                    $dscttl += $report['discount'];
                                    ?></td>
                                <td><?php
                                    echo $report['netTotal'];
                                    $ttlpurchase += $report['netTotal'];
                                    ?></td>

                            </tr>
                        <?php } ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align: right;">Total : </th>
                            <th><?php echo $gttl; ?></th>
                            <th><?php echo $dscttl; ?></th>
                            <th><?php echo $ttlpurchase; ?></th>

                        </tr>
                    </tfoot>
                </table>
                <table class="table table-bordered table-hover" id="tableInfo" style="width:49%;margin-left:1%;">
                    <thead>
                        <tr>
                            <td colspan="6"  style="font-weight:bold;font-size: 16px;">
                                 Payment History
                            </td>
                        </tr>

                    </thead>
                    <tbody>
                    <tr>

                        <th style="width:20%">Date</th>

                        <th style="width:20%"> Account</th>
<!--                        <th>Previous</th>-->
                        <th>Payment</th>
<!--                        <th>Present</th>-->
                        <th>Note</th>

                    </tr>


                    <?php
                    if (!empty($suppliers_payment)):

                    $total_amount = '';
                    foreach ($suppliers_payment as $key => $supplier) {

                        $account_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_accounts', 'accountID', $supplier['transactionAccountID']);
                        $total_amount += $supplier['amount'];
                        ?>
                        <tr>

                            <td><a href="<?php echo site_url('Suppliers/supplierPaymentDetailsInfo/'.$supplier['payment_id']); ?>" ><?php echo $supplier['date']; ?></a></td>

                            <td><?php echo $account_name['accountName']; ?></td>
<!--                            <td>--><?php //echo $supplier['totalDueAmount']; ?><!--</td>-->
                            <td style="text-align:right;"><?php echo $supplier['amount']; ?></td>
<!--                            <td>--><?php //echo $supplier['presentDue']; ?><!--</td>-->
                            <td><?php echo $supplier['note']; ?></td>

                        </tr>

                    <?php } ?>
                    <tfoot>
                    <tr>
                        <td style="text-align: right;color:green;font-weight: bold;" colspan="2"> Total Payment : </td>
                        <td colspan="1" style="color:green;font-weight: bold;text-align:right"><?php echo number_format($total_amount, 2); ?>/=</td>
                        <td></td>



                    </tr>
                    </tfoot>
                    <?php else: ?>
                        <tr>
                            <td style="text-align: center;color:red;" colspan="4">
                                No transaction found
                            </td>
                        </tr>
                    <?php endif; ?>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</section>
