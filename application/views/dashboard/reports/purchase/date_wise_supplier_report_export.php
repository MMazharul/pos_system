 
<style>

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }

</style>

<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<table style="width:100%;">
    <tr>
        <td style="width:40%;float:left;">
            Supplier
            <address>
                <strong><?php echo $supplierInfo['supplierName']; ?></strong><br>
                Phone: <?php echo $supplierInfo['supplierPhone']; ?><br>
                Email: <?php echo $supplierInfo['supplierEmail']; ?><br>
                <?php echo $supplierInfo['supplierAddress']; ?><br>
            </address>
        </td>


    </tr>
</table>


<table class="table table-striped" id="tableInfo" style="width:49%;float:left;margin-right:1%;">
    <thead>
    <tr>
        <td colspan="2">
            <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
        </td>
        <td colspan="3">
            <h3><b>Supplier Summary  </b></h3>
        </td>

    </tr>
    <tr>
        <td colspan="5" style="font-weight:bold;font-size: 16px;">
            Purchase History
        </td>
    </tr>
    <tr>
        <th>Purchase ID</th>
        <th>Purchase Date</th>

        <th>Grand Total</th>
        <th>Discount</th>
        <th>Net Total</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $gttl = 0;
    $dscttl = 0;
    $ttlpurchase = 0;
    $ttldue = 0;
    $ttlpay = 0;
    $due = 0;
    foreach ($supplierreport as $report) {
        ?>
        <tr>
            <td><?php echo $report['purchaseNo']; ?></td>
            <td><?php echo $report['purchaseDate']; ?></td>

            <td><?php
                echo $report['subTotal'];
                $gttl += $report['subTotal'];
                ?></td>
            <td ><?php
                echo $report['discount'];
                $dscttl += $report['discount'];
                ?></td>
            <td><?php
                echo $report['netTotal'];
                $ttlpurchase += $report['netTotal'];
                ?></td>

        </tr>
    <?php } ?>
    </tbody>

    <tfoot>
    <tr>
        <th colspan="2" style="text-align: right;">Total : </th>
        <th><?php echo $gttl; ?></th>
        <th><?php echo $dscttl; ?></th>
        <th><?php echo $ttlpurchase; ?></th>

    </tr>
    </tfoot>
</table>
<table class="table table-bordered table-hover" id="tableInfo" style="width:49%;margin-left:1%;">
    <thead>
    <tr>
        <td colspan="6"  style="font-weight:bold;font-size: 16px;">
            Payment History
        </td>
    </tr>

    </thead>
    <tbody>
    <tr>

        <th style="width:20%">Date</th>

        <th style="width:20%"> Account</th>
        <th>Previous</th>
        <th>Payment</th>
        <th>Present</th>
        <th>Note</th>

    </tr>


    <?php
    if (!empty($suppliers_payment)):

    $total_amount = '';
    foreach ($suppliers_payment as $key => $supplier) {

        $account_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_accounts', 'accountID', $supplier['transactionAccountID']);
        $total_amount += $supplier['amount'];
        ?>
        <tr>

            <td><?php echo $supplier['date']; ?></td>

            <td><?php echo $account_name['accountName']; ?></td>
            <td><?php echo $supplier['totalDueAmount']; ?></td>
            <td><?php echo $supplier['amount']; ?></td>
            <td><?php echo $supplier['presentDue']; ?></td>
            <td><?php echo $supplier['note']; ?></td>

        </tr>

    <?php } ?>
    <tfoot>
    <tr>
        <td style="text-align: right;color:green;font-weight: bold;" colspan="3"> Total Payment : </td>
        <td colspan="1" style="color:green;font-weight: bold;"><?php echo number_format($total_amount, 2); ?>/=</td>
        <td colspan="2"></td>


    </tr>
    </tfoot>
    <?php else: ?>
        <tr>
            <td style="text-align: center;color:red;" colspan="4">
                No transaction found
            </td>
        </tr>
    <?php endif; ?>

    </tbody>
    <tfoot>
    </tfoot>
</table>