<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }
</style>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<table class="table table-striped" width="100%">
    <thead>
        <tr>
            <td colspan="2">
                <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
            </td>
            <td colspan="3">
                <h3><b>Details Purchase Report  </b></h3>
            </td>

        </tr>
        <tr>
            <th>Purchase Date</th>
            <th>Invoice No</th>
            <th>Product</th>
            <th>Supplier Name</th>
            <th style="text-align: right" >Quantity</th>
            <th style="text-align: right" >Net Total</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $ttlnet = 0;
        $ttldue = 0;
        $ttlqty = 0;
        $ttlprice = 0;
        $ttlcost = 0;
        foreach ($purchasereport as $report) {
            ?>
            <tr>
                <td><?php echo $report->purchaseDate; ?></td>
                <td><?php echo $report->purchaseNo; ?></td>
                <td>
                    <?php
                    $pro_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $report->productID);
                    echo $pro_name['productName'];
                    ?>
                </td>
                <td>
                    <?php
                    $supl_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_suppliers', 'supplierID', $report->supplierID);
                    echo $supl_name['supplierName'];
                    ?>
                </td>
                <td style="text-align: right"><?php
                    echo $report->quantity;
                    $ttlqty +=$report->quantity;
                    ?></td>
                <td style="text-align: right"><?php
                    echo $report->purchasePrice;
                    $ttlnet+=$report->purchasePrice;
                    ?>
                </td>
                <td style="text-align: right"><?php
                    echo $ttlcost = $report->quantity * $report->purchasePrice;
                    $ttlprice+= $ttlcost;
                    ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>

    <tfoot>
        <tr>
            <th colspan="4">Total</th>
            <th style="text-align: right"><?php echo $ttlqty; ?></th>
            <th style="text-align: right"><?php echo $ttlnet; ?></th>
            <th style="text-align: right"><?php echo $ttlprice; ?></th>
        </tr>
    </tfoot>
</table>