<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Supplier</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <?php echo $this->session->flashdata('msg'); ?>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('reports/askdetailsDateWiseSupplierReport'); ?>" method="post">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="form-group has-feedback">
                                    <label>Supplier</label>
                                    <div class="input-group">
                                        <?php
                                        $this->db->select('*');
                                        $this->db->from('tbl_pos_suppliers');
                                        $query = $this->db->get();
                                        $result = $query->result_array();
//                                        dumpVar($result);
                                        ?> 
                                      
                                        <select name="supplierID" class="form-control select2">
                                            <option value="">---( Select Supplier )---</option>
                                            <?php foreach ($result as $eachclient) { ?>
                                                <option value="<?php echo $eachclient['supplierID']; ?>"><?php echo $eachclient['supplierName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="form-group has-feedback">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="date" class="form-control pull-right" id="reservation">
                                    </div>
                                </div>
                            </div>

                            <!-- /.col -->
                            <div class="col-xs-2">
                                <div class="form-group has-feedback">
                                    <label style="color: white">.</label>

                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>

                                </div>
                            </div>

                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</section>
<!-- /.content -->