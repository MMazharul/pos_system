<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<style>
    @media print
    {    
        .no-print
        {
            display: none !important;
        }
    }

    table tr th{text-align: center;
                border:1px solid grey!important;
                margin: 0px!important;
                padding: 0px!important;

    }
    table tr td{
        text-align: center;
        border:1px solid grey!important;
        margin: 0px!important;
        padding: 0px!important;
    }

</style>

<section class="content-header">

</section>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Details Purchase Report
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-9">
                    <h5><b>Report Between : <?php echo $first_date . '-' . $last_date; ?></b></h5>
                </div>
                <div class="col-xs-3">
                    <div class="col-xs-6">
                        <button type="button" onclick="window.print();" class="no-print btn  btn-primary btn-flat"><i class="fa fa-print"></i> Print</button>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo site_url('reports/datewisepurchse_exportbyxls'); ?>" class="btn btn-success">
                            <i class="fa fa-save"></i> EXCEL
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table id="example1" class="table table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>Purchase Date</th>
                            <th>Invoice No</th>
                            <th>Product</th>
                            <th>Supplier Name</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $ttlnet = 0;
                        $ttldue = 0;
                        $ttlqty = 0;
                        $ttlprice = 0;
                        $ttlcost = 0;
                        foreach ($purchasereport as $report) {
                            ?>
                            <tr>
                                <td><?php echo $report->purchaseDate; ?></td>
                                <td><?php echo $report->purchaseNo; ?></td>
                                <td>
                                    <?php
                                    $pro_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $report->productID);
                                    echo $pro_name['productName']; if($pro_name['productCode']!=''): echo " [".$pro_name['productCode']."]"; endif;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $supl_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_suppliers', 'supplierID', $report->supplierID);
                                    echo $supl_name['supplierName'];
                                    ?>
                                </td>
                                <td><?php
                                    echo $report->quantity;
                                    $ttlqty += $report->quantity;
                                    ?></td>
                                <td><?php
                                    echo $report->purchasePrice;
                                    $ttlnet += $report->purchasePrice;
                                    ?>
                                </td>
                                <td><?php
                                    echo $ttlcost = $report->quantity * $report->purchasePrice;
                                    $ttlprice += $ttlcost;
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="4" style="text-align:right;"> Total: </th>
                            <th><?php echo $ttlqty; ?></th>
                            <th><?php echo $ttlnet; ?></th>
                            <th><?php echo $ttlprice; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>
