<section class="content-header">

</section>
<section class="content">
    <?php
    $success = $this->session->flashdata('success');
    if ($success) {
        ?> 

        <div class="box box-info">
            <div class="box-body">
                <div class="callout callout-info">
                    <?php
                    echo $success;
                    ?>
                </div>
            </div><!-- /.box-body -->
        </div>

        <?php
    }

    $failed = $this->session->flashdata('failed');
    if ($failed) {
        ?>

        <div class="box box-info">
            <div class="box-body">

                <b style="color: red;">
                    <?php
                    echo $failed;
                    ?>
                </b>

            </div><!-- /.box-body -->
        </div>

        <?php
    }
    ?>
    <div class="col-md-8 col-md-offset-2">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Item Report</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo site_url('reports/itemReportByDate'); ?>" method="post">
                <div class="box-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5">
                                <div class="form-group has-feedback">
                                    <label>Product</label>
                                    <div class="input-group">
                                        <?php
                                        $this->db->select('*');
                                        $this->db->from('tbl_pos_products');
                                        $query = $this->db->get();
                                        $result = $query->result_array();
//                                        dumpVar($result);
                                        ?> 

                                        <select name="productID" class="form-control select2">
                                            <option value="">---( Select Product )---</option>
                                            <?php foreach ($result as $eachitem) { ?>
                                                <option value="<?php echo $eachitem['productID']; ?>"><?php echo $eachitem['productName']." (".$eachitem['productCode'].")"; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label>Date</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="date_range" id="reservation" placeholder="Date Range" class="form-control pull-right">
                                </div><!-- /.input group -->
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <div class="input-group">
                                    <button type="submit" class="btn btn-sm btn-primary pull-left" ><i class="fa fa-search"></i>&nbsp;Search</button>
                                </div><!-- /.input group -->
                            </div>
                        </div>

                    </div>
                </div><!-- /.box-body -->

            </form>
        </div><!-- /.box -->
</section>
<script type="text/javascript">
    $("#item_name").autocomplete({
        source: 'new_report/report_controller/getproductname',
        select: function (event, ui) {
            $("#item_name").val(ui.item.value);
            $("#item_id").val(ui.item.id);
            return false;
        }
    });
</script>