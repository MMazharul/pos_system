<!-- Main content -->
<style>
    @media print {
        .no-print {
            display: none !important;
        }
    }

    table tr th {
        text-align: center;
        border: 1px solid grey !important;
        margin: 0px !important;
        padding: 0px !important;

    }

    table tr td {
        text-align: center;
        border: 1px solid grey !important;
        margin: 0px !important;
        padding: 0px !important;
    }

</style>
<section class="content invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h6 class="page-header">
                <small> Category : <?php echo $catSubItem['catagoryName']; ?></small>
                <small>Product Name : <?php echo $catSubItem['productName'];
                    echo " [" . $catSubItem['productCode'] . "]"; ?></small>
                <small>Available Stock : <?php echo $avail_stock['avail_stock']; ?></small>
                <small class="pull-right">Date: <?php echo $current_date; ?></small>
            </h6>
        </div>
    </div>

    <br/>
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-6 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th colspan="6">Stock In</th>
                </tr>
                <tr>
                    <th>PUR No.</th>
                    <th> Date</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>T.Price</th>
                    <th>Supplier</th>
                </tr>
                </thead>
                <?php
                if (!empty($itemReportByDate)) {
                    $sl = 1;
                    $stock = 0;
                    $price = 0;
                    foreach ($itemReportByDate as $ir) {
                        $price +=  $ir['quantity'] * $ir['purchasePrice'];
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo site_url('purchases/show/'.$ir['refNo']); ?>" target="_blank">
                                <?php
                                if (!empty($ir['refNo'])) {
                                    echo $ir['refNo'];
                                } else {
                                    echo "Positive Adjusted";
                                }
                                $stock += $ir['quantity'];
                                ?>
                                </a>
                            </td>
                            <td><?php echo $ir['date']; ?></td>
                            <td><?php echo $ir['quantity']; ?></td>
                            <td><?php echo $ir['purchasePrice']; ?></td>
                            <td><?php echo $ir['quantity'] * $ir['purchasePrice']; ?></td>

                            <td><?php echo $ir['supplierName']; ?></td>
                        </tr>
                    <?php }
                    ?>
                    <tr>
                        <td colspan="2" style="text-align:right"><b><i>Total Quantity: </i></b></td>
                        <td><b><i><?php echo $stock; ?></td>
                        <td></td>
                        <td><?php echo $price; ?></td>
                        <td></td>
                    </tr>
                <?php } else {
                    ?>
                    <tr>
                        <td style="color:red;" colspan="6">Stock In not found!!</td>
                    </tr>
                <?php }
                ?>
            </table>
        </div><!-- /.col -->
        <div class="col-xs-6 table-responsive">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th colspan="5">Stock Out</th>
                </tr>
                <tr>
                    <th>Invoice</th>
                    <th>Date</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>T.Price</th>
<!--                    <th>Customer</th>-->
                </tr>
                </thead>
                <?php
                if (!empty($itemReportByDate2)) {
                    $sl = 1;
                    $stock = 0;
                    $price = 0;
                    foreach ($itemReportByDate2 as $ir) {
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo site_url('pos/show/'.$ir['refNo']); ?>" target="_blank">
                                <?php
                                if (!empty($ir['refNo'])) {
                                    echo $ir['refNo'];
                                } else {
                                    echo "Negative Adjusted";
                                }
                                $stock += $ir['quantity'];
                                ?>
                                </a>
                            </td>
                            <td><?php echo $ir['date']; ?></td>
                            <td><?php echo $ir['quantity']; ?></td>
                            <td><?php
                                if (!empty($ir['price'])) {
                                    echo $ir['price'];
                                    $price += $ir['price'];
                                }
                                ?>
                            </td>
                            <td><?php echo $ir['quantity']*$ir['price']; ?></td>
<!--                            <td>--><?php //echo $ir['clientName']; ?><!--</td>-->
                        </tr>
                    <?php }
                    ?>
                    <tr>
                        <td colspan="2" style="text-align:right"><b><i>Total Quantity: </i></b></td>
                        <td><b><i><?php echo $stock; ?></td>
                        <td></td>
                        <td>
                            <?php echo $price; ?>
                        </td>
                    </tr>
                    <?php
                } else {
                    ?>
                    <tr>
                        <td style="color:red;" colspan="5">Stock out not found!!</td>
                    </tr>
                <?php }
                ?>
            </table>
        </div><!-- /.col -->
    </div><!-- /.row -->

    </br>
    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
            <p>Copyright © <?php echo date("Y"); ?>dokan</p>
        </div><!-- /.col -->
        <div class="col-xs-6">

        </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> Print
            </button>
        </div>
    </div>
</section><!-- /.content -->