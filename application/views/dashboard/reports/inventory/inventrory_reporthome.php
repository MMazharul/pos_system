<style>
    .divstyles{margin-top: 4px;}
</style>
<section class="content-header">
    <div class="box-body">
        <form action="<?php echo base_url('inventory/serarchInventroy'); ?>" method="post">
            <div class="row">
                <div class="col-md-4">
                    <label>Warehouse</label>
                    <div class="form-group">
                        <?php
                        $this->db->select('*');
                        $this->db->from('tbl_pos_warehouses');
                        $this->db->order_by("warehouseID", "desc");
                        $this->db->where("warehouseID", 4);
                        $query = $this->db->get();
                        $result = $query->result_array();
//                        dumpVar($result);
                        ?>
                        <select name="warehouseID" class="form-control select2" style="width: 100%;">
                            <?php foreach ($result as $warehouse) { ?>
                                <option value="<?php echo $warehouse['warehouseID']; ?>"><?php echo $warehouse['warehouseName']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Product</label>
                    <div class="form-group ">
                        <?php
                        $this->db->select('*');
                        $this->db->from('tbl_pos_products');
                        $query = $this->db->get();
                        $results = $query->result_array();
//                        dumpVar($result);
                        ?>
                        <select name="productID" class="form-control select2" style="width: 100%;">
                            <option value="">Products</option>
                            <?php foreach ($results as $e_product) { ?>
                                <option value="<?php echo $e_product['productID']; ?>"><?php echo $e_product['productName']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label></label>
                    <div class="form-group divstyles">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</section>
<?php
//extract($_POST);
//print_r($_POST);
//if (($warehouseID == '') && ($productID == '')) {
//    $dataSearch = $inventory;
//} elseif (($warehouseID != '') && ($productID == '')) {
//    $dataSearch = $inventroyseracrdatabyhouse;
//} elseif (($warehouseID == '') && ($productID != '')) {
//    $dataSearch = $inventroyseracrdatabyhouse;
//}elseif(($warehouseID != '') && ($productID != '')){
//    $dataSearch = $inventroyseracrdatabyhouse;
//}
?>

<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Inventory
                    <small class="pull-right">Date: <?php echo $today = date("d-m-Y");?></small>
                </h2>
            </div>
        </div>
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 20%;">Product Category</th>
                                <th style="width: 70%;">Product Name ( Barcode )</th>
                                <th style="width: 10%;">Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($inventory as $value) { ?>
                                <tr>
                                    <td><?php echo $value->catagoryName; ?></td>
                                    <td><?php echo $value->productName.'('.$value->productCode.')'; ?></td>
                                    <td><b><?php echo $value->ttlqty; ?></b></td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
    </section>
</section>