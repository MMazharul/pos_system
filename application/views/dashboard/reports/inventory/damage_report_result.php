<style>
    @media print
    {    
        .no-print
        {
            display: none !important;
        }
    }
    table tr th{text-align: center;
                border:1px solid grey!important;
                margin: 0px!important;
                padding: 0px!important;

    }
    table tr td{
        text-align: center;
        border:1px solid grey!important;
        margin: 0px!important;
        padding: 0px!important;
    }

</style>

<section class="content-header">

</section>

<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Damage Product Report
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-9">
                    <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                </div>

                <div class="col-xs-3 no-print">
                    <div class="col-xs-5">
                       
                    </div>
                    <div class="col-xs-5">
                        <button type="button" onclick="window.print();" class="no-print btn btn-primary pull-right"><i class="fa fa-print"></i> Print</button>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:15%">Date</th>
                            <th style="width:40%">Product</th>

                            <th style="width:15%">Qty</th>
                            <th style="width:15%">Price</th>
                            <th style="width:15%">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total_qty = 0;
                        $total_price = 0;
                        foreach ($damage_report as $each_damage) {
                            $total_qty += abs($each_damage['total_qty']);
                            $total_price += abs($each_damage['total_qty']) * $each_damage['product_price'];
                            ?>
                            <tr>
                                <td><?php echo date('Y-m-d', strtotime($each_damage['damage_date'])); ?></td>
                                <td><?php echo $each_damage['productName']." [".$each_damage['productCode']."]"; ?></td>

                                <td><?php echo abs($each_damage['total_qty']); ?></td>
                                <td><?php echo $each_damage['product_price']; ?></td>
                                <td><?php echo number_format(abs($each_damage['total_qty']) * $each_damage['product_price'], 2); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align: right;">Total : </th>
                            <th><?php echo $total_qty; ?></th>
                            <td></td>
                            <th><?php echo number_format($total_price, 2); ?></th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>
