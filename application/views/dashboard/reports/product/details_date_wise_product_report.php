<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->


<section class="content-header">

</section>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Details Product Report
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-8">
                    <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                </div>
                <div class="col-xs-4">
                    
                    <a href="<?php echo site_url('reports/datewiseproduct_exportbyxls'); ?>" class="btn btn-app pull-right">
                        <i class="fa fa-save"></i> EXCEL
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>Sales Date</th>
                            <th>Invoice</th>
                            <th style="text-align: right">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $gttl = 0;
                        foreach ($productreport as $report) {
                            ?>
                            <tr>
                                <td><?php echo $report->salesDate; ?></td>
                                <td><?php echo $report->invoiceNo; ?></td>
                                <td align="right"><?php
                        echo $report->netttl;
                        $gttl += $report->netttl;
                            ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="2">Total</th>
                            <th style="text-align: right"><?php echo $gttl; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2">
                <button type="button" onclick="window.print();" class="no-print btn btn-block btn-success btn-flat"><i class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = yyyy+'/'+mm+'/'+dd;
    //document.write(today);
    $('#today').text(today);
</script>
