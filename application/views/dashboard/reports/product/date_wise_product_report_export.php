<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }
</style>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>

<table class="table table-striped" width="100%">
    <thead>
        <tr>
            <td colspan="2">
                <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
            </td>
            <td colspan="3">
                <h3><b>Product Report  </b></h3>
            </td>
        </tr>
        <tr>
            <th>Sales Date</th>
            <th>Invoice</th>
            <th style="text-align: right">Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $gttl = 0;
        foreach ($productreport as $report) {
            ?>
            <tr>
                <td><?php echo $report->salesDate; ?></td>
                <td><?php echo $report->invoiceNo; ?></td>
                <td align="right"><?php
        echo $report->netttl;
        $gttl += $report->netttl;
            ?></td>
            </tr>
        <?php } ?>
    </tbody>

    <tfoot>
        <tr>
            <th colspan="2">Total</th>
            <th style="text-align: right"><?php echo $gttl; ?></th>
        </tr>
    </tfoot>
</table>