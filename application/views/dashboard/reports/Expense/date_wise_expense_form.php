<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Expense</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('reports/askdetailsDateWiseExpenseHWReport'); ?>" method="post">


                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group has-feedback">
                                    <label>Expense Head</label>
                                    <div class="input-group">
                                        <?php
                                        $this->db->select('*');
                                        $this->db->from('tbl_pos_expense_head');
                                        $query = $this->db->get();
                                        $result = $query->result_array();
                                        ?> 

                                        <select name="head_id" class="form-control head_id select2">
                                            <option value="">--(Select Expense Head)--</option>
                                            <?php foreach ($result as $eachhead) { ?>
                                                <option value="<?php echo $eachhead['expheadID']; ?>"><?php echo $eachhead['title']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group has-feedback">
                                    <label>Expense Sub Head</label>
                                    <div class="input-group">
                                        <select name="sub_head_id" class="form-control subitem_1 select2">
                                            <option value="">--(Select Expense Sub Head)--</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group has-feedback">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="date" class="form-control pull-right" id="reservation">
                                    </div>
                                </div>
                            </div>

                            <!-- /.col -->
                            <div class="col-xs-2">
                                <div class="form-group has-feedback">
                                    <label style="color: white">.</label>

                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>

                                </div>
                            </div>

                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $('.head_id').change(function () {
            var head_id = $(this).val();

            $.ajax({
                url: "ajaxSubheadload",
                method: "POST",
                data: {head_id: head_id},
                dataType: "text",
                success: function (data)
                {
                    $('.subitem_1').html(data);
                }
            })
        });
    });

</script>