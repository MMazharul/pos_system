<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<style>
    @media print
    {    
        .no-print
        {
            display: none !important;
        }
    }

    table tr th{text-align: center;
                border:1px solid grey!important;
                margin: 0px!important;
                padding: 0px!important;

    }
    table tr td{
        text-align: center;
        border:1px solid grey!important;
        margin: 0px!important;
        padding: 0px!important;
    }

</style>

<section class="content-header">

</section>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Details Expense Report
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-9">
                    <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                </div>
                <div class="col-xs-3 no-print ">
                    <div class="col-xs-6">
                        <button type="button" onclick="window.print();" class="no-print btn btn-block btn-primary btn-flat"><i class="fa fa-print"></i> Print</button>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo site_url('reports/datewiseExpenseHW_exportbyxls'); ?>" class="btn  btn-success pull-right">
                            <i class="fa fa-save"></i> EXCEL
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Create Person</th>
                            <th>Account Head</th>
                            <th>Account Sub Head</th>
                            <th>Account</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $ttlpay = 0;
                        foreach ($expenseHW as $report) {
                            ?>
                            <tr>
                                <td><?php echo date("Y-m-d", strtotime($report->created_at)); ?></td>
                                <td><?php echo strtoupper($this->users[$report->expense_persion]); ?></td>
                                <td>
                                    <?php
                                    echo $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_expense_head', 'expheadID', $report->head_id)['title'];
                                    ?>
                                </td>
                                <td><?php echo $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_exp_sub_head', 'subheadId', $report->sub_head_id)['title']; ?></td>
                                <td><?php echo $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_accounts', 'accountID', $report->account_id)['accountName']; ?></td>
                                <td><?php
                                    echo $report->amount;
                                    $ttlpay += $report->amount
                                    ?></td>

                            </tr>
                        <?php } ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="5"  style="text-align: right">Total : </th>
                            <th><?php echo $ttlpay; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>
