
<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }
</style>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>


<table class="table table-striped" width="100%">
    <thead>
        <tr>
            <td colspan="2">
                <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
            </td>
            <td colspan="3">
                <h3><b>Expense Report  </b></h3>
            </td>
        </tr>
        <tr>
            <th>Date</th>
            <th>Account Head</th>
            <th>Account Sub Head</th>
            <th>Account</th>
            <th style="text-align: right">Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $ttlpay = 0;
        foreach ($expenseHW as $report) {
            ?>
            <tr>
                <td><?php echo date("Y-m-d", strtotime($report->created_at)); ?></td>
                <td>
                    <?php
                    echo $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_expense_head', 'expheadID', $report->head_id)['title'];
                    ?>
                </td>
                <td><?php echo $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_exp_sub_head', 'subheadid', $report->head_id)['title']; ?></td>
                <td><?php echo $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_accounts', 'accountID', $report->account_id)['accountName']; ?></td>
                <td style="text-align: right"><?php
                    echo $report->amount;
                    $ttlpay +=$report->amount
                    ?></td>

            </tr>
        <?php } ?>
    </tbody>

    <tfoot>
        <tr>
            <th colspan="4">Total</th>
            <th style="text-align: right"><?php echo $ttlpay; ?></th>
        </tr>
    </tfoot>
</table>