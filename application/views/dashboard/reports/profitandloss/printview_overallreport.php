<style>
    .myStyleCss{background: white}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="col-md-12">
                <div class="col-md-11">
                    <h4>Overall Reports</h4>
                </div>
                <div class="col-md-1"><br>
                    <a onclick="window.print();" style="cursor:pointer;" class="pull-right btn btn-circle btn-xs btn-default hidden-print" ><i class="fa fa-print"></i> Print</a> 
                </div>
            </div>
            
            
            <table class="table table-bordered myStyleCss">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($totalpurchase->ttlpurchase)) { ?>
                        <tr>
                            <td>Total Purchase</td>
                            <td><?php echo $totalpurchase->ttlpurchase ?></td>
                        </tr>
                    <?php } ?>
                    <?php if (!empty($totalpurchasedue->ttlpurchasedue)) { ?>
                        <tr>
                            <td>Total Purchase Due</td>
                            <td><?php echo $totalpurchasedue->ttlpurchasedue ?></td>
                        </tr>
                    <?php } ?>
                    <?php if (!empty($totalsale->ttlsales)) { ?>
                        <tr>
                            <td>Total Sale</td>
                            <td><?php echo $totalsale->ttlsales ?></td>
                        </tr>
                    <?php } ?>

                    <?php if (!empty($overallcost)) { ?>
                        <tr>
                            <td>Total Sale cost</td>
                            <td><?php echo $overallcost ?></td>
                        </tr>
                    <?php } ?>

                    <?php if (!empty($totalexpense->ttltransaction)) { ?>
                        <tr>
                            <td>Total Expense</td>
                            <td><?php echo $totalexpense->ttltransaction ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>
        <div class="col-md-2"></div>
    </div>
</div>