<div class="col-md-6 col-md-offset-3">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Profit/Loss Report</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form action="<?php echo site_url('reports/profitLoss'); ?>" method="post">
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <div class="input-group">
                                <select name="type" class="form-control">
                                    <option value="1">Gross Profit/Loss</option>
                                    <option value="2">Net Profit/Loss</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input required="" name="date_range" id="reservation" placeholder="Date Range" class="form-control pull-right">
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div style="padding-bottom: 10px;">
                    </div>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div><!-- /.box -->
</div>