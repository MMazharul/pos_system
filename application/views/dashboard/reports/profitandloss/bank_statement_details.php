<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->

<!--<style>
    th{text-align: center}
    td{text-align: right}

</style>-->

<section class="content-header">

</section>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
$accountID =  $_SESSION['accountID'];
?>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Bank Statement
                    <small class="pull-right" id="today"></small>
                </h2>
                <div class="col-xs-8">
                    <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                </div>
                <div class="col-xs-4">
                    <!--<a href="<?php // echo site_url('reports/datewiseSupplierdetails_exportbyxls');                ?>" class="btn btn-app pull-right">-->
                        <!--<i class="fa fa-save"></i> EXCEL-->
                    <!--</a>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th style="text-align: right">In Amount</th>
                            <th style="text-align: right">Out Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $ttlin = 0;
                        $ttlout = 0;
                        $inAmount = 0;
                        $outAmount = 0;
                      


                        $date = $first_date;
                        $end_date = $last_date;

                        while (strtotime($date) <= strtotime($end_date)) {
                            ?>
                            <tr> 
                                <td>
                                    <?php
                                    $fdate = $date;
                                    echo "$date\n";
                                    $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                                    ?>
                                </td>
                                <td style="text-align: right"><?php  $inAmount = $this->REPORTS->detailsAccountReport($fdate, $accountID); echo $inAmount['ttltransaction']; $ttlin += $inAmount['ttltransaction'] ?></td>
                                <td style="text-align: right"><?php $outAmount = $this->REPORTS->detailsAccountReport2($fdate, $accountID); echo $outAmount['ttltransactionout']; $ttlout += $outAmount['ttltransactionout'] ?></td>
                            </tr>

                        <?php } ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th colspan="1">Total</th>
                            <th style="text-align: right"><?php echo $ttlin; ?></th>
                            <th style="text-align: right"><?php echo $ttlout; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2">
                <button type="button" onclick="window.print();" class="no-print btn btn-block btn-success btn-flat"><i class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>
