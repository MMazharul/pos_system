
<?php
$success = $this->session->flashdata('success');
if ($success) {
    ?> 

    <div class="box box-info">
        <div class="box-body">
            <div class="callout callout-info">
                <?php
                echo $success;
                ?>
            </div>
        </div><!-- /.box-body -->
    </div>

    <?php
}
?>
<?php
$failed = $this->session->flashdata('failed');
if ($failed) {
    ?>

    <div class="box box-info">
        <div class="box-body">
            <div class="callout callout-warning">
                <?php
                echo $failed;
                ?>
            </div>
        </div>
    </div>

    <?php
}
?>


<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header">
             <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                     <h3 style="text-align:center">Monthly / Yearly Overall Report</h3>
                </div>
                <div class="col-md-3"></div>
            </div>
           
        </div>

        <form class="form-inline" action="<?php echo base_url(); ?>reports/printmonthly_sum_rep" method="post">
            <?php
            $user_role = $this->session->userdata('abhinvoiser_1_1_role');
            $user_name = $this->session->userdata('abhinvoiser_1_1_user_name');
            ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 col-md-offset-4">
                        <div id="collapse1">
                            <div class="form-group"> 
                                <select name="month" id="month" class="form-control"  style="width: 125px;">
                                    <option value=""> Month</option>
                                    <?php
                                    $months = array('01'=>"January", '02'=>"February", '03'=>"March", '04'=>"April", '05'=>"May", '06'=>"June", '07'=>"July", '08'=>"August", '09'=>"September", '10'=>"October", '11'=>"November", '12'=>"December");
                                    foreach ($months as $k=>$month) {
                                        echo "<option value=\"" . $k . "\">" . $month . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group" >
                                <?php $year = date("Y"); ?>   
                                <select class="form-control"  name="year" id="year" style="width: 125px;">
                                    <option value=" ">Select</option>
                                    <?php
                                    for ($i = date('Y') + 10; $i > 1899; $i--) {
                                        ?>
                                        <option value="<?php echo $i; ?>" <?php
                                    if ($year == $i) {
                                        echo "Selected";
                                    }
                                        ?>>
                                            <?php echo $i; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" style="width: 75px; padding: 7px 0" class="btn btn-primary btn-sm btn-flat" >Search</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.box -->
</div><!-- /.box -->
<script>
    function myFunction() {
        var x = document.getElementById('myDIV');
        if (x.style.display === 'none') {
            x.style.display = 'block';
        } else {
            x.style.display = 'none';
        }
    }
</script>
