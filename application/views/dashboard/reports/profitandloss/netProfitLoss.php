﻿
<style>

    .myStyle{
        text-decoration: underline double;
-webkit-text-decoration: underline double;
    }
</style>
<section class="content">
    <?php
    $sales = $totalSales['totalSaless'];
    $cost = $costOfGoodsSold;
    $expenses = $totalExpenses['totalExpenses'];
    $gross = $sales - $cost;
    $net = $gross - $expenses;
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="success">
                        <th colspan="3">
                <h5 align="center">
                   MEGAMART Income Statement  <?php // echo $storeInfo->company_info;          ?>
                </h5>
                </th>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <i>
                            <b>
                                from <?php
                                $date = date_create($first_date);
                                echo date_format($date, "M d, Y");
                                echo " To ";
                                $date = date_create($last_date);
                                echo date_format($date, "M d, Y");
                                ?>
                            </b>
                        </i>
                    </td>
                </tr>
                </thead>
                <tbody>

                    <tr>
                        <th>Total Sales</th>
                        <td></td>
                        <td align='right'>BDT <?php echo $totalSales['totalSaless']; ?></td>
                    </tr>

                    <tr>
                        <td colspan="3"></td>
                    </tr>

                    <tr>
                        <th>Cost of Goods Sold</th>
                        <td></td>
                        <td align='right'><u><b>(BDT <?php echo $cost; ?>)</b></u></td>
                </tr>

                <tr>
                    <th>
                        Gross <?php
                        if ($gross > 0) {
                            echo 'Profit';
                        } else {
                            echo 'Loss';
                        }
                        ?>
                    </th>
                    <td></td>
                    <td align='right'><b>BDT <?php echo $gross; ?></b></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>

<!--                <tr>
                    <th colspan="3"> Expenses</th>
                </tr>

                <tr>
                    <td>General</td>
                    <td align='right'>BDT <?php // echo $expenses;          ?></td>
                    <td></td>
                </tr>-->

                <tr>
                    <th> Total Expenses</th>
                    <td></td>
                    <td align='right'><b>(BDT <?php echo $expenses; ?>)</b></td>
                </tr>

                <tr
                <?php
                if ($net < 0) {
                    $pl = "Loss";
                    echo 'style="color: red;"';
                } else {
                    $pl = "Profit";
                    echo 'style="color: green;"';
                }
                ?>>

                    <th> NET PROFIT <?php // echo $pl;          ?></th>
                    <td></td>
                    <td align='right'><u class="myStyle"><b>BDT <?php echo $net; ?></b></u></td>
                </tr>
                <tr>
                  <th>Total Puchase</th>
                  <td></td>
                  <td align='right'><u class="myStyle"><b>BDT
							  <?php if($totalPurchases['totalPurchases'])
								  echo $totalPurchases['totalPurchases'];
							  else if($duePurchase['total_purchase'])
									echo $duePurchase['total_purchase'];

							  	?>
						  </b></u></td>
                <tr>
                  <tr>
                    <th>Total Pay Amount</th>
                    <td></td>
                    <td align='right'><u class="myStyle"><b>BDT <?php  if($duePurchase['total_purchase']) echo $duePurchase['payAmount']; else echo 0 ?></b></u></td>
                  </tr>
                  <tr>
                    <th>Total Due Amount</th>
                    <td></td>
                    <td align='right'><u class="myStyle"><b>BDT <?php  if($duePurchase['payAmount']) echo ($duePurchase['total_purchase']-$duePurchase['payAmount']); else echo 0 ?></b></u></td>
                  </tr>

                      <tr>
                        <th>Account Name</th>
                        <td></td>

                        <td align='right'><u class="myStyle"><b>Balance</b></u></td>
                      </tr>
                      <?php if(count($accountBalanceHistory)>0){
                        foreach ($accountBalanceHistory as $key => $value) {

                        ?>
                        <tr>
                          <th><?=$value->accountName?></th>
                          <td></td>

                          <td align='right'><u class="myStyle"><b><?=$value->balance?></b></u></td>
                        </tr>
                      <?php } } ?>

                </tbody>
            </table>

            <?php
                $inventory_quantities = $this->INVENTORYMODEL->searchdataHistory1(4);
                $total_quantity = 0;
                $total_price = 0;
                $inventory = $this->INVENTORYMODEL->inventory();
                foreach($inventory_quantities as $key => $quantity)
                {
                    $total_quantity += $quantity['ttlbalance'];
                    $price = $this->INVENTORYMODEL->getProductPrice($quantity['productID']);
                    $inventory_price = $inventory[$key]->ttlpurchasePrice;
                    $total_price += $price + $inventory_price;
                }
            ?>

            <table class="table table-bordered">
                <tr>
                    <th>
                        INVENTORY / TOTAL STOCK
                    </th>
                    <th class="text-right">
                        Total Qty - <?php echo number_format($total_quantity, 0); ?>
                    </th>
                    <th class="text-right">
                        Total Price - <?php echo number_format($total_price, 2); ?>
                    </th>
                </tr>
            </table>
        </div>
            </div>
            <div class="col-md-1"></div>
        </div>


    </div>
    </br>
    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-5"></div>
        <div class="col-xs-5">
            <p al>Copyright © <?php echo date("Y"); ?>dokan</p>
        </div><!-- /.col -->
        <div class="col-xs-6">

        </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> Print
            </button>
        </div>
    </div>
</section>
