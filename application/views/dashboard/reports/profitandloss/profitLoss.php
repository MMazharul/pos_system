<!-- Main content -->
<section class="content">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <div style="font-size: 18px;border-bottom: 0px;text-align: center;" class="page-header">

                <p> Profit/Loss Report</br>
                    <small>
                        <?php
                            $date = date_create($first_date);
                            echo date_format($date, "M d, Y");
                            echo " To ";
                            $date = date_create($last_date);
                            echo date_format($date, "M d, Y");
                        ?>
                    </small>

                </p><small class="pull-right">Date: <?php //echo $current_date; ?></small>
            </div>
        </div><!-- /.col -->
    </div>

    <!-- Table row -->
    <div class="row">

        <div class="box-body table-responsive">
            <table id="example" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Item Code</th>
                        <th>Item Name</th>
                        <th>Avg Purchase Price</th>
                        <th>Avg Selling Price</th>
                        <th>Sold Qty</th>
                        <th>
                            <span style="color: green;">Profit</span> /
                            <span style="color: red;">Loss</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($profitLoss)){
                        $total_qty = 0;
                        $total_purchase = 0;
                        $total_sale = 0;
                        $profit_loss = 0;
                        foreach($profitLoss as $all_item){
                            if($all_item[0]['quantity'] >0){ ?>
                    <tr>
                        <td><?php echo $all_item['productCode']; ?></td>
                        <td><?php echo $all_item['productName']; ?></td>
                        <td><?php echo round($all_item['avgpurchase'], 2); ?></td>
                        <td>
                            <?php echo round($all_item[0]['avg_selling'], 2); ?>
                        </td>
                        <td><?php echo abs($all_item[0]['quantity']); ?></td>
                        <!--<td><?php echo round($selling_price['qty'] * $selling_price['avg_selling'], 2); ?></td>-->
                        <td>
                            <?php
                                $purchase_all = $all_item['avgpurchase'] * $all_item[0]['quantity'];
                                $sale_all = $all_item[0]['avg_selling'] * $all_item[0]['quantity'];
                                $profit_loss = round($sale_all - $purchase_all, 2);
                                if($profit_loss < 0){
                                    echo "<b style='color: red;'>".$profit_loss."</b>";
                                }else{
                                    echo "<b style='color: green;'>".$profit_loss."</b>";
                                }
                                $total_qty += abs($all_item[0]['quantity']);
                                $total_purchase += $purchase_all;
                                $total_sale += $sale_all;

                            ?>
                        </td>
                    </tr>
                    <?php }
                        } }else{
                        echo "No data available";
                    }
                ?>
                </tbody>
            </table>
            <?php
                if(!empty($profitLoss)){
                    ?>
                    <h5 align="center">
                        Total Sold Quantity: <?php echo $total_qty; ?><br>
                       <!--Total Sold Amount: <?php //echo round($totalsale->grand_total, 2); ?><br><br>-->
                        Total Sold Amount (Average): <?php echo round($total_sale, 2); ?><br>
                        Total Discount: <?php echo round($total_discount, 2); ?><br>
                        Total Purchased Amount (Average): <?php echo round($total_purchase, 2); ?><br>
                        <?php
                            $profitOrLoss = round($total_sale - $total_discount - $total_purchase, 2);
//                            $profitOrLoss = round($totalsale->grand_total - $total_purchase, 2);
                            if($profitOrLoss < 0){
                               echo "<b style='color: red;'>Loss: ".$profitOrLoss."</b>";
                            }else{
                                echo "<b style='color: green;'>Profit: ".$profitOrLoss."</b>";
                            }
                        ?>
                    </h5>
            <?php }
             ?>
        </div><!-- /.box-body -->
    </div><!-- /.row -->

    </br>
    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
            <p>Copyright © <?php echo date("Y"); ?>dokan</p>
        </div><!-- /.col -->
        <div class="col-xs-6">

        </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> Print Invoice
            </button>
        </div>
    </div>
</section><!-- /.content -->
