<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Bank Statements</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <?php echo $this->session->flashdata('msg'); ?>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('reports/askdetailsDateWiseAccountReport'); ?>" method="post">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="form-group has-feedback">
                                    <label>Account</label>
                                    <div class="input-group">
                                        <?php
                                        $this->db->select('*');
                                        $this->db->from('tbl_pos_accounts');
                                        $query = $this->db->get();
                                        $result = $query->result_array();
//                                        dumpVar($result);
                                        ?> 
                                      
                                        <select name="accountID" class="form-control select2">
                                            <option value="">---( Select Account )---</option>
                                            <?php foreach ($result as $eachclient) { ?>
                                                <option value="<?php echo $eachclient['accountID']; ?>"><?php echo $eachclient['accountName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="form-group has-feedback">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="date" class="form-control pull-right" id="reservation">
                                    </div>
                                </div>
                            </div>

                            <!-- /.col -->
                            <div class="col-xs-2">
                                <div class="form-group has-feedback">
                                    <label style="color: white">.</label>

                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>

                                </div>
                            </div>

                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</section>
<!-- /.content -->