<style>

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }

</style>

<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<table class="table table-striped" width="100%">
    <thead>
        <tr>
            <td colspan="2">
                <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
            </td>
            <td colspan="3">
                <h3><b>Details Sales Report  </b></h3>
            </td>

        </tr>
        <tr>
            <th>Sales Date</th>
            <th>Invoice No</th>
            <th>Client Name</th>
            <th>Net Total</th>
            <th>Due Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $ttlnet = 0;
        $ttldue = 0;
        foreach ($reports as $report) {
            ?>
            <tr>
                <td><?php echo $report->salesDate; ?></td>
                <td><?php echo $report->invoiceNo; ?></td>
                <?php if ($report->clientName == '') { ?>
                    <td>Walk In Customer</td>
                <?php } else { ?>
                    <td><?php echo $report->clientName; ?></td>
                <?php } ?>
                <td style="text-align: right">
                    <?php
                    echo $report->netTotal;
                    $ttlnet += $report->netTotal;
                    ?>
                </td>
                <td style="text-align: right">
                    <?php
                    echo $report->dueAmount;
                    $ttldue +=$report->dueAmount;
                    ?></td>
            </tr>
        <?php } ?>
    </tbody>

    <tfoot>
        <tr>
            <th colspan="3">Total</th>
            <th style="text-align: right"><?php echo $ttlnet; ?></th>
            <th style="text-align: right"><?php echo $ttldue; ?></th>
        </tr>
    </tfoot>
</table>