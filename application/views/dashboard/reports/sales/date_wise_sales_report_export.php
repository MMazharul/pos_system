<style>

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }

</style>


<table class="table table-bordered dataShowTable">
    <thead>
    <tr>
        <td colspan="2">
            <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
        </td>
        <td colspan="6">
            <h3><b>Details Sales Report  </b></h3>
        </td>

    </tr>
    <tr>
        <th>Invoice</th>
        <th>Date</th>
        <th>Sale Amount</th>
        <th>Vat(%)</th>
        <th>Vat Amount</th>
        <th>Grand Total</th>
        <th>Discount</th>
        <th>Net Amount</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $netttl = 0;
    $dsctt = 0;
    $dsvat = 0;
    $grandTotalTotal = 0;
    foreach ($reports as $report) {
        $dsvat += $report->vat;
        $dsctt += $report->discount;
        $grandTotalTotal += $report->grandTotal;
        $netttl += $report->netTotal;
        ?>
        <tr>

            <td  id="printShow"><?php echo $report->invoiceNo; ?></td>
            <td><?php echo $report->salesDate; ?></td>
            <td><?php echo $report->subTotal; ?></td>
            <td><?php echo $report->percentage; ?></td>
            <td><?php echo $report->vat; ?></td>
            <td><?php echo $report->grandTotal; ?></td>
            <td><?php echo $report->discount; ?></td>
            <td><?php echo $report->netTotal; ?></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="4">Total</th>
        <th><?php if ($dsvat != ''): echo number_format($dsvat, 2, '.', ''); endif; ?></th>
        <th><?php if ($grandTotalTotal != ''): echo number_format($grandTotalTotal, 2, '.', ''); endif; ?></th>
        <th><?php if ($dsctt != ''): echo number_format($dsctt, 2, '.', ''); endif; ?></th>
        <th><?php if ($netttl != ''): echo number_format($netttl, 2, '.', ''); endif; ?></th>

    </tr>
    </tfoot>
</table>