<style>
    @media print {
        .no-print {
            display: none !important;
        }

        #printShow {
            display: block !important;
        }
    }

    .dataShowTable tr th {
        text-align: center;
        border: 1px solid lightgray !important;
        margin: 2px 0px 2px 0px !important;
        padding: 2px 0px 2px 0px !important;

    }

    .dataShowTable tr td {
        text-align: center;
        border: 1px solid lightgray !important;
        margin: 2px 0px 2px 0px !important;
        padding: 2px 0px 2px 0px !important;
    }

</style>

    <table class="table table-bordered dataShowTable" border="1px">
        <thead>
        <tr>
            <td colspan="7">

                <?php echo $title; ?>
                <small style="text-align:right;"><b>Date:</b> <?php echo date('Y-m-d') ?></small>

            </td>
        </tr>
        <tr>
            <th>Invoice</th>
            <th>Date</th>
            <th>Sale Amount</th>
            <th>Vat(%)</th>
            <th>Vat Amount</th>
            <th>Discount</th>
            <th>Net Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $netttl = 0;
        $dsctt = 0;
        $dsVat = 0;
        foreach ($reports as $report) {
            $dsctt += $report->discount;
            $dsVat += $report->vat;
            $netttl += $report->netTotal;
            ?>
            <tr>

                <td><?php echo $report->invoiceNo; ?></td>
                <td><?php echo $report->salesDate; ?></td>
                <td><?php echo $report->subTotal; ?></td>
                <td><?php echo $report->percentage; ?></td>
                <td><?php echo $report->vat; ?></td>
                <td><?php echo $report->discount; ?></td>
                <td><?php echo $report->netTotal; ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4">Total</th>
            <th><?php echo $dsVat; ?></th>
            <th><?php echo $dsctt; ?></th>
            <th><?php echo $netttl; ?></th>

        </tr>
        </tfoot>
    </table>
