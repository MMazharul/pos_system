<style>

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;

    }

</style>

<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>


<table class="table table-striped" width="100%">
    <thead>
        <tr>
            <td colspan="2">
                <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
            </td>
            <td colspan="2">
                <h3><b>Client Summary  </b></h3>
            </td>

        </tr>
        <tr>
            <th>Sales Date</th>
            <th style="text-align: right">Sales Amount</th>
            <th style="text-align: right">Paid Amount</th>
            <th style="text-align: right">Due Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $ttlsale = 0;
        $ttldue = 0;
        $ttlpay = 0;
        $due = 0;
        foreach ($clientreport as $report) {
            ?>
            <tr>
                <td><?php echo $report->salesDate; ?></td>
                <td style="text-align: right"><?php
        echo $report->netttl;
        $ttlsale += $report->netttl;
            ?>
                </td>
                <td style="text-align: right"><?php
                echo $due = $report->netttl - $report->duettl;
                $ttlpay += $due;
            ?>
                </td>
                <td style="text-align: right"><?php
                echo $report->duettl;
                $ttldue += $report->duettl;
            ?>
                </td>

            </tr>
        <?php } ?>
    </tbody>

    <tfoot>
        <tr>
            <th colspan="">Total</th>
            <th style="text-align: right"><?php echo $ttlsale; ?></th>
            <th style="text-align: right"><?php echo $ttlpay; ?></th>
            <th style="text-align: right"><?php echo $ttldue; ?></th>
        </tr>
    </tfoot>
</table>