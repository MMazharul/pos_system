<style>
    @media print {
        .no-print {
            display: none !important;
        }

        #printShow {
            display: block !important;
        }
    }

    .dataShowTable tr th {
        text-align: center;
        border: 1px solid lightgray !important;
        margin: 2px 0px 2px 0px !important;
        padding: 2px 0px 2px 0px !important;

    }

    .dataShowTable tr td {
        text-align: center;
        border: 1px solid lightgray !important;
        margin: 2px 0px 2px 0px !important;
        padding: 2px 0px 2px 0px !important;
    }

</style>

<section class="content-header">

</section>
<?php
$dateexplode = explode("-", $_SESSION['date_range']);
$first_date = $dateexplode[0];
$last_date = $dateexplode[1];
?>
<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Sales Report
                    <small class="pull-right" id="today"></small>
                </h2>

                <div class="col-xs-9">
                    <div class="row">
                        <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 no-print pull-right">
                        <div class="row">
                            <div class="col-xs-4 pull-right">
                                <button type="button" onclick="window.print();"
                                        class="no-print btn btn-primary pull-right"><i class="fa fa-print"></i> Print
                                </button>

                            </div>
                            <form action="<?php echo site_url('reports/datewisesalessummary_exportbyxls'); ?>" method="post">
                                <div class="col-xs-4 pull-right">
                                    <button type="submit"
                                       class="btn btn-success pull-right">
                                        <i class="fa fa-save"></i> EXCEL
                                    </button>
                                </div>
                                <input type="hidden" name="first_date" value="<?php echo $first_date  ?>">
                                <input type="hidden" name="last_date" value="<?php echo $last_date  ?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered dataShowTable">
                    <thead>
                    <tr>
                        <th>Invoice</th>
                        <th>Date</th>
                        <th>SalesMan</th>
                        <th>Sale Amount</th>
                        <th>Vat(%)</th>
                        <th>Vat Amount</th>
                        <th>Grand Total</th>
                        <th>Discount</th>
                        <th>Net Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $netttl = 0;
                    $dsctt = 0;
                    $dsvat = 0;
                    $grandTotalTotal = 0;
                    foreach ($reports as $report) {
                        $dsvat += $report->vat;
                        $dsctt += $report->discount;
                        $grandTotalTotal += $report->grandTotal;
                        $netttl += $report->netTotal;
                        ?>
                        <tr>
                            <td class="no-print"><a
                                        href="<?php echo site_url('pos/show/' . $report->invoiceNo) ?>"><?php echo $report->invoiceNo; ?></a>
                            </td>
                            <td style="display: none;" id="printShow"><?php echo $report->invoiceNo; ?></td>
                            <td><?php echo $report->salesDate; ?></td>
                            <td><?php echo strtoupper($this->users[$report->sales_persion]); ?></td>
                            <td><?php echo $report->subTotal; ?></td>
                            <td><?php echo $report->percentage; ?></td>
                            <td><?php echo $report->vat; ?></td>
                            <td><?php echo $report->grandTotal; ?></td>
                            <td><?php echo $report->discount; ?></td>
                            <td><?php echo $report->netTotal; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5">Total</th>
                        <th><?php if ($dsvat != '') echo number_format($dsvat, 2, '.', '');  else  echo '0.00'; ?></th>
                        <th><?php if ($grandTotalTotal != ''): echo number_format($grandTotalTotal, 2, '.', ''); endif; ?></th>
                        <th><?php if ($dsctt != ''): echo number_format($dsctt, 2, '.', ''); endif; ?></th>
                        <th><?php if ($netttl != ''): echo number_format($netttl, 2, '.', ''); endif; ?></th>

                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>



