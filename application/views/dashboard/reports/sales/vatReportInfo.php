<style>
    @media print {
        .no-print {
            display: none !important;
        }

        #printShow {
            display: block !important;
        }
    }

    .dataShowTable tr th {
        text-align: center;
        border: 1px solid lightgray !important;
        margin: 2px 0px 2px 0px !important;
        padding: 2px 0px 2px 0px !important;

    }

    .dataShowTable tr td {
        text-align: center;
        border: 1px solid lightgray !important;
        margin: 2px 0px 2px 0px !important;
        padding: 2px 0px 2px 0px !important;
    }

</style>
<section class="content no-print">

    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
                <div class="box-body" style="height: 180px;">
                    <div class="col-sm-offset-4 col-sm-5">
                        <form action="" method="post">
                            <div class="form-group has-feedback">
                                <label>Date Range</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input required="" readonly="" name="date" class="form-control pull-right"
                                           id="reservation">
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-4">
                                    <button type="submit" name="searchBtn" class="btn btn-primary btn-sm">Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>
<?php
if (!empty($reports)):
    ?>
    <section class="content">
        <section class="invoice">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <?php echo $title; ?>
                        <small class="pull-right" id="today"><b>Date:</b> <?php echo date('Y-m-d') ?></small>
                    </h2>
                    <div class="col-xs-9">
                        <div class="row">
                            <h5><b>Report Between: <?php echo $first_date . '-' . $last_date; ?></b></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 no-print pull-right">
                            <div class="row">
                                <div class="col-xs-4 pull-right">
                                    <button type="button" onclick="window.print();"
                                            class="no-print btn btn-primary pull-right"><i class="fa fa-print"></i>
                                        Print
                                    </button>

                                </div>
                                <form action="<?php echo site_url('reports/vatReportInfo_exportbyxls/'); ?>"
                                      method="post">
                                    <div class="col-xs-4 pull-right">
                                        <button type="submit"
                                           class="btn btn-success pull-right">
                                            <i class="fa fa-save"></i> EXCEL
                                        </button>
                                    </div>
                                    <input type="hidden" name="firstData" value="<?php echo $first_date; ?>">
                                    <input type="hidden" name="lastDate" value="<?php echo $last_date; ?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-bordered dataShowTable">
                        <thead>
                        <tr>
                            <th>Invoice</th>
                            <th>Date</th>
                            <th>Sale Amount</th>
                            <th>Vat(%)</th>
                            <th>Vat Amount</th>
                            <th>Discount</th>
                            <th>Net Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $netttl = 0;
                        $dsctt = 0;
                        $dsVat = 0;
                        foreach ($reports as $report) {
                            $dsctt += $report->discount;
                            $dsVat += $report->vat;
                            $netttl += $report->netTotal;
                            ?>
                            <tr>
                                <td class="no-print"><a
                                            href="<?php echo site_url('pos/show/' . $report->invoiceNo) ?>"><?php echo $report->invoiceNo; ?></a>
                                </td>
                                <td style="display: none;" id="printShow"><?php echo $report->invoiceNo; ?></td>
                                <td><?php echo $report->salesDate; ?></td>
                                <td><?php echo $report->subTotal; ?></td>
                                <td><?php echo $report->percentage; ?></td>
                                <td><?php echo $report->vat; ?></td>
                                <td><?php echo $report->discount; ?></td>
                                <td><?php echo $report->netTotal; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <th><?php echo $dsVat; ?></th>
                            <th><?php echo $dsctt; ?></th>
                            <th><?php echo $netttl; ?></th>

                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
    </section>
    <?php
endif;