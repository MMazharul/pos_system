

<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header center">
                    <?php echo $title.'<small>BETWEEN '.$date.'</small>'; ?>
                </h2>

<!--                <div class="col-xs-4">
                    <a href="<?php echo site_url('reports/datewisesalesdetails_exportbyxls'); ?>" class="btn btn-app pull-right">
                        <i class="fa fa-save"></i> EXCEL
                    </a>
                </div>-->
            </div>
        </div>
        <?php 
            if(!empty($result)){ ?>
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table id="example1"  class="table table-striped" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">SL</th>
                                    <th  width="20%">Product Name</th>
                                    <th  width="20%">Product Code</th>
                                    <th  width="10%" class="center">Quantity</th>
                                    <th  width="20%">Invoice No</th>
                                    <th  width="20%" style="text-align: right">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sl = 1;
                                    $quantity = 0;
                                    $price = 0;
                                    foreach ($result as $row) { 
                                        $quantity += $row[1]["quantity"];
                                        $price += ($row[1]["price"] * $row[1]["quantity"]);
                                        echo '<tr>'
                                                . '<td>'.$sl++.'</td>'
                                                . '<td>'.$row["productName"].'</td>'
                                                . '<td>'.$row["productCode"].'</td>'
                                                . '<td class="center">'.$row[1]["quantity"].'</td>';
                                                echo '<td>';
                                                    foreach($row[0] as $row0){ echo $row0["invoiceNo"].', ';  }

                                                echo '</td>'
                                                . '<td  style="margin-left: -10px!important;" class="pull-right">'.$row[1]["price"] * $row[1]["quantity"].'</td>';
                                        echo '</tr>';
                                    } 
                                ?>

                                <tr>
                                    <?php 
                                    $last_balance =$price - $discount; 
                                    
                                        echo '<th colspan="2" class="center">Grand Total</th>'
                                            . '<th></th>'
                                            . '<th class="center">'.$quantity.'</th>'
                                            . '<th></th>'?>
                                    <th class="pull-right">
                                        <table class="table table-bordered">
                                                   <tr>
                                                       <td>Total Sales   : </td>
                                                       <td class="pull-right"><?php echo $price; ?></td>
                                                   </tr>
                                                   <?php
                                                        if(!$product_catagoriesID)
                                                        {
                                                            ?>
                                                            <tr>
                                                               <td>Total Discount : </td>
                                                               <td class="pull-right"><?php echo $discount; ?></td>
                                                           </tr>
                                                           <tr>
                                                               <td>Total Amount : </td>
                                                               <td class="pull-right"><?php echo $last_balance; ?></td>
                                                           </tr>
                                                            <?php
                                                        }
                                                   ?>
                                               </table>
                                               
                                               </th>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        <button type="button" onclick="window.print();" class="no-print  btn btn-success">
                            <i class="fa fa-print"></i> Print
                        </button>
                    </div>
                </div>
            <?php }else{ echo '<b style="color: red;">No data found!</b>'; }
        ?>
    </section>
</section>