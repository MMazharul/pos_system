<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <div class="box-body">
              <form action="<?php echo base_url('reports/returnRep'); ?>" method="post">
              <div class="form-group has-feedback">
                <label>Date Range</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input required="" readonly="" name="date" class="form-control pull-right" id="reservation">
                </div>
              </div>
              
              <div class="row">
                <div class="col-xs-4">
                  
                </div>
                <div class="col-xs-4">
                  
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
              </div>
            </form>
            </div>
        </div>
  </div>
  <div class="col-md-4"></div>
</div>
</section>