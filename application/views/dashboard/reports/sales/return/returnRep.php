<section class="content">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header center">
                    <?php echo $title.'<small>BETWEEN '.$date.'</small>'; ?>
                    
                </h2>
            </div>
        </div>
        <?php 
            if(!empty($result)){ ?>
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped" width="100%">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Product Name</th>
                                    <th>Product Code</th>
                                    <th class="center">Quantity</th>
                                    <th>Invoice No</th>
                                    <th>Return Charge</th>
                                    <th class="center">Returned Amount</th>
                                    <th class="center">Total Returned</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sl = 1;
                                    $returnQuantity = 0;
                                    $price = 0;
                                     $totalChargeAmount='';
                                    $proDiscount='';
//                                    echo "<pre>";
//                                    print_r($result);
                                    foreach ($result as $row) { 
                                        $returnQuantity += $row[1]["returnQuantity"];
                                        $price += $row[2];


                                        echo '<tr>'
                                                . '<td>'.$sl++.'</td>'
                                                . '<td>'.$row["productName"].'</td>'
                                                . '<td>'.$row["productCode"].'</td>'
                                                . '<td class="center">'.$row[1]["returnQuantity"].'</td>';

                                                echo '<td>';
                                                    foreach($row[0] as $row0){
                                                        echo $row0["invoiceNo"].'&nbsp;&nbsp; ';
                                                    }
                                                echo '</td>'
                                        ?>
                                        <td>
                                            <?php
                                            $sumProduct='';
                                            $sumProductSum='';
                                            foreach($row[0] as $row0){
                                                $invId=$row0["invoiceNo"];
                                                $sumProduct .= $this->REPORTS->viewReturnChage($invId).',';
                                                $sumProductSum+=$this->REPORTS->viewReturnChage($invId);
                                            }
                                            $totalChargeAmount+=$sumProductSum;
                                            echo rtrim($sumProduct,",")."[".$sumProductSum."]";
                                            ?>
                                        </td>
                                        <td class="center">
                                            <?php  echo  $sumProdcut=$row[2]-$sumProductSum; $proDiscount+=$sumProdcut; ?>
                                        </td>
                                        <?php

                                               echo  '<td class="center">'.$row[2].'</td>';
                                        echo '</tr>';
                                    } 
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" class="center"><b>Total Amount</b</td>
                                    <td class="center"><b><?php echo $returnQuantity; ?></b></td>
                                    <td></td>
                                    <td class="center"><b><?php echo $totalChargeAmount; ?></b></td>
                                    <td class="center"><b><?php echo $proDiscount; ?></b></td>
                                    <td class="center"><b><?php echo $price; ?></b></td>
                                </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        <button type="button" onclick="window.print();" class="no-print btn btn-block btn-success">
                            <i class="fa fa-print"></i> Print
                        </button>
                    </div>
                </div>
            <?php }else{ echo '<b style="color: red;">No data found!</b>'; }
        ?>
    </section>
</section>