<section class="content invoice">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
                <h3 align="center">Today  <?php echo date("d-m-y"); ?></h3>
                <div class="col-md-4">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
						<div style="height:36px;">
                            <?php
                            $today_sales = $this->COMMON_MODEL->get_today_salesCash(date('Y-m-d'));
                            $today_salesCard = $this->COMMON_MODEL->get_today_salesCard(date('Y-m-d'));?>

                            <h6 style=" text-align: center"><?php echo ($today_sales!='')? "Cash: ".$today_sales: "Cash: 0"; ?></h6>
                            <h6 style=" text-align: center"><?php echo ($today_salesCard!='')? "Card: ".$today_salesCard: "Card: 0"; ?></h6>
							</div>
                            <hr>
                            <p style="font-size: 14px; text-align: center">SALES (BDT)</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <div style="height:35px;">
                                <?php $total_purchase_today_out = $this->COMMON_MODEL->viewTodayTotalPurchase('tbl_pos_purchases', 'netTotal', 'purchaseDate'); ?>
                                <h4 style="font-size: 14px; text-align: center"><?php echo $total_purchase_today_out ?></h4>
                            </div>
                            <hr>

                            <p style="font-size: 14px; text-align: center">PURCHASE (BDT)</p>
                        </div>
                        <div class="icon">

                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <div style="height:35px;">
                                <?php $total_exp_today = $this->COMMON_MODEL->viewTodayTotalExpense('tbl_pos_expense', 'amount', 'created_at'); ?>
                                <h4 style=" text-align: center"><?php echo $total_exp_today ?></h4>
                            </div>
                            <hr>
                            <p style="font-size: 14px; text-align: center">EXPENSES (BDT)</p>
                        </div>
                        <div class="icon">

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

</section>

<br><br>

<section class="content invoice">
    <div class="row">
        <div class="col-md-12 table-responsive">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h4 style="text-align: center"><u>Accounts Summary</u></h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Branch Name</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $balance = 0; ?>
                        <?php $sl = 1; ?>
                        <?php foreach ($accountBalanceHistory as $accountBalance) { ?>
                            <?php $balance = $accountBalance->balance + $balance; ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $accountBalance->accountName; ?></td>
                                <td><?php echo $accountBalance->accountNumber; ?></td>
                                <td><?php echo $accountBalance->accountBranchName; ?></td>
                                <td><?php echo ($accountBalance->balance!='')? $accountBalance->balance: 0; ?></td>
                            </tr>
                            <?php $sl++; ?>
                        <?php } ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th>Balance</th>
                            <th><?php echo $balance; ?></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
</section>