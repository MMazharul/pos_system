<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <base href="http://localhost/istiaq/pos/"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>

    <!-- Bootstrap 3.3.6 -->
    <link rel= "stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <style type="text/css" media="all">
    body { color: #000; }
    #wrapper { max-width: 480px; margin: 0 auto; padding-top: 0px; }
    .btn { border-radius: 0; margin-bottom: 0px; }
    .bootbox .modal-footer { border-top: 0; text-align: center; }
    h3 { margin: 5px 0; }
    .order_barcodes img { float: none !important; margin-top: 5px; }
    @media print {
        .no-print { display: none; }
        #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
        .no-border { border: none !important; }
        .border-bottom { border-bottom: 1px solid #ddd !important; }
    }
</style>
</head>

<body>
    <div id="wrapper">
        <div id="receiptData">
            <div class="no-print"></div>
            <div id="receipt-data">
                <div class="text-center">
                    <!-- <h3 style="text-transform:uppercase;"><?php  echo $appConfig->company_info ?></h3> -->
                    <img height="80px" src="<?=base_url()?>/pos-logo.png">
                    <div class="col-sm-12" style="font-size:11px;"><?php echo $appConfig->address ?></div>
                    <div  class="col-sm-12" style="font-size:12px;"><?php echo $appConfig->contactNo ?></div>
                </div>
                <?php
                    $results = $this->db->select('userID, username')->from('tbl_pos_users')->get()->result_array();
                    $users = array_column($results, 'username', 'userID');
                ?>
                <p>Time: <?php echo date("h:i:s"); ?><br>Date: <?php echo date("d-m-Y",strtotime($sales[0]->salesDate)); ?><br>Sale No/Ref: <?php echo $sales[0]->invoiceNo; ?><br>
                    Served By: <?php echo strtoupper($users[$sales[0]->sales_persion]); ?> </p>
                    <div style="clear:both;"></div>
                    <table class="table table-striped table-condensed">
                        <tbody>
                            <thead>
                                <tr>
                                   <!--<th width="5%">SN</th>-->
                                    <th width="50%">PRODUCTS</th>
									<th width="15%"></th>
                                    <th width="5%">Q.</th>
                                    <th width="15%">PRICE</th>
                                    <th width="15%">TOTAL</th>
                                </tr>
                            </thead>
                            <?php
                                $sl = 1;
                                foreach($sales[0]->products as $product)
                                {
                                    ?>
                                    <tr>
                                       <!-- <td class="border-bottom">
                                            <?php echo $sl; ?>
                                        </td>-->
                                        <td style="font-size:10px;" ><?php echo $product->productName ; ?></td>

										<td style="font-size:13px;" ><?php echo  $product->productCode ; ?></td>

										<!--<td style="font-size:13px;" ><?php echo  $product->productModel ; ?></td>-->

                                        <td class="border-bottom text-right">
                                            <?php echo $product->quantity; ?>
                                        </td>
                                        <td class="border-bottom text-right">
                                            <?php echo $product->price; ?>
                                        </td>
                                        <td class="border-bottom text-right">
                                            <?php echo $product->quantity * $product->price; ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $sl++;
                                }
                            ?>
                            </tbody>
                            <tfoot>
                                        <?php
                                         if($sales[0]->vat > 0)
                                            {
                                                ?>
                                                <tr>
                                                    <th colspan="2">Vat:</th>
                                                    <th class="text-right"  colspan="3"><?php echo $sales[0]->vat; ?></th>
                                                </tr>
                                                <?php
                                            }
                                        ?>
                                        <tr>
                                            <th colspan="2">Grand Total:</th>
                                            <th class="text-right" colspan="3"><?php echo $sales[0]->grandTotal; ?></th>
                                        </tr>
                                        <?php
                                            if($sales[0]->discount > 0)
                                            {
                                                ?>
                                                <tr>
                                                    <th colspan="2">Discount:</th>
                                                    <th class="text-right"  colspan="3"><?php echo $sales[0]->discount; ?></th>
                                                </tr>
                                                <?php
                                            }

                                            if(!empty($sales[0]->CardNum))
                                            {
                                                $cardCharge = $sales[0]->netTotal - $sales[0]->grandTotal;
                                                if($cardCharge > 0)
                                                {
                                                    ?>
                                                    <tr>
                                                        <th colspan="2">Card Charge( <?php echo $sales[0]->percentage.'%' ?> ):</th>
                                                        <th class="text-right"  colspan="3"><?php echo $sales[0]->netTotal - $sales[0]->grandTotal; ?></th>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        ?>
                                        <tr>
                                            <th colspan="2">Net Total:</th>
                                            <th class="text-right" colspan="3"><?php echo $sales[0]->netTotal; ?></th>
                                        </tr>
                            </tfoot>
                                        </table>
                                        <table class="table table-striped">
                                            <tr>
                                                <td style="text-align: left;">Paid by: <?php
                                                if (!empty($sales[0]->CardNum)) {
                                                    echo "Card <span style='font-size:10px;'>[".$sales[0]->CardNum."]</span>";

                                                } else {
                                                    echo 'Cash';
                                                }
                                                ?></td>
                                                <td style="text-align: center;">Pay Amount: <?php echo $sales[0]->posPaying; ?></td>
                                                <td style="text-align: right;">Change: <?php echo $sales[0]->posChange; ?></td>
                                            </tr>
                                            <?php if ($sales[0]->returnValue > 0) { ?>
                                                <tr>
                                                    <td colspan="5" style="text-align: center;">Return Amount: <?php echo $sales[0]->returnValue; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                            <p class="text-center" style="font-size: 10px "> Thank you for shopping with us. Please come again.</p>
                                            <p class="text-center" style="font-size: 14px ">  <?= $sales[0]->note; ?></p>
                                            <p class="text-center" style="font-size: 16px "> পরিবর্তন করার সময় বিল নিয়ে আসবেন</p>
                                        </div>
										<!--<div class="order_barcodes text-center">
                                            <p style="font-size:8px;"> Software Developed By DOKAN</p>
                                        </div> -->
                                        <div style="clear:both;"></div>


                                    </div>

                                    <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
                                        <hr>
                                        <span class="pull-right col-xs-12">
                                            <button onclick="window.print();" class="btn btn-block btn-primary">Print</button>
                                        </span>

                                        <span class="col-xs-12">
                                            <a class="btn btn-block btn-success" href="<?php echo base_url('pos'); ?>">Back to POS</a>
                                        </span>
                                        <div style="clear:both;"></div>
                                        <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
                                            <p style="font-weight:bold;">
                                                Please don't forget to disble the header and footer in browser print settings.
                                            </p>
                                            <p style="text-transform: capitalize;">
                                                <strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp; Header/Footer Make all --blank--
                                            </p>
                                            <p style="text-transform: capitalize;">
                                                <strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer in Option &amp; Set Margins to None
                                            </p>
                                        </div>
                                        <div style="clear:both;"></div>
                                    </div>
                                </div>

                                <script type="text/javascript">

                                    $(window).load(function () {

                                        window.print();
                                        return false;
                                    });


                                </script>
                            </body>
                            </html>
