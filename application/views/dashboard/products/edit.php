<section class="content-header">
</section>
<section class="content">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Product</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('products/update'); ?>" method="post">
                        <input type="hidden" name="productID"  value="<?php echo $products['productID']; ?>">
                        <div class="form-group">
                            <label>Product Category</label>
                            <select name="catagoryID" class="form-control select2" style="width: 100%;">
                                <?php foreach ($productcatagories as $productcatagory) { ?>
                                    <?php if ($productcatagory->product_catagoriesID == $products['catagoryID']) { ?>
                                        <option value="<?php echo $productcatagory->product_catagoriesID; ?>">
                                            <?php echo $productcatagory->catagoryName; ?>
                                        </option>
                                    <?php } ?>
                                <?php } ?>
                                <?php foreach ($productcatagories as $productcatagory) { ?>
                                    <?php if ($productcatagory->product_catagoriesID != $products['catagoryID']) { ?>
                                        <option value="<?php echo $productcatagory->product_catagoriesID; ?>">
                                            <?php echo $productcatagory->catagoryName; ?>
                                        </option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Product Name</label>
                            <input type="hidden" name="original-productName"  value="<?php echo $products['productName']; ?>">
                            <input required="" name="productName" class="form-control" placeholder="Product Name" value="<?php echo $products['productName']; ?>">
                        </div>
                        <div class="form-group has-feedback">
                            <label>Product Sales Price</label>
                            <input required="" name="productPrice" class="form-control" placeholder="Product Sales Price" value="<?php echo $products['productPrice']; ?>">
                        </div>
<!--                        <div class="form-group has-feedback">
                            <label>Product Whole Sale Price</label>
                            <input required="" name="productWholeSalePrice" class="form-control" placeholder="Product Whole Sale Price" value="<?php //echo $products['productWholeSalePrice']; ?>">
                        </div>-->
                        <div class="form-group has-feedback">
                            <label>Product Model</label>

                            <input required="" id="productModel" type="text" name="productModel"
                                   placeholder="Product Model" value="<?php echo $products['productModel']; ?>" class="form-control">
                        </div>
                        <div class="form-group has-feedback">
                            <label>Product Barcode</label>
                            <div class="input-group input-group-sm">
                                <input type="hidden" name="original-productCode" value="<?php echo $products['productCode']; ?>">
                                <input required="" id="productCode" type="text" name="productCode" placeholder="Product Code" class="form-control"  value="<?php echo $products['productCode']; ?>">
                                <span class="input-group-btn">
                                    <button id="random" type="button" class="btn btn-flat"><i class="fa fa-random"></i></button>
                                </span>
                            </div>
                            <span id="productCodeError" class="help-block" style="color: red;font-weight: bold;"></span>
                            <span class="help-block">You can scan your barcode or click random button</span>

                        </div>


                        <div class="row">
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
                            </div>

                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
    var randomString = function(length) {
        var text = "";
        var possible = "0123456";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };

    var codeLenghtCheck = function () {
        var code = $('#productCode').val();
        if( code.length < 6 ){
            $("#productCodeError").text("Product Code must be minimum 6 digit lenght.");
        }else if(code.length > 6){
            $("#productCodeError").text("Product Code must be maximum 6 digit lenght.");
        }else{
            $( "#productCodeError" ).empty();
        }
    };

    $( "#random" ).click(function() {
        $('#productCode').val(randomString(6));
        codeLenghtCheck();
    });

    $( "#productCode" ).keyup(function() {
        codeLenghtCheck();
    });

    $( "#productCode" ).change(function() {
        codeLenghtCheck();
    });
	
</script>