<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-sm-offset-4 col-sm-8" style="margin-bottom:10px;">
            <a href="<?php echo site_url('products/index'); ?>" class="btn btn-primary btn-sm">Product List</a>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">New Product</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('products/store'); ?>" method="post">
                        <div class="form-group">
                            <label>Product Category</label>
                            <select name="catagoryID" class="form-control select2" style="width: 100%;">
                                <option value="">-- Product Catagory --</option>
                                <?php foreach ($productcatagories as $productcatagory) { ?>
                                    <option value="<?php echo $productcatagory->product_catagoriesID; ?>"><?php echo $productcatagory->catagoryName; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Product Name</label>
                            <input required="" name="productName" class="form-control" placeholder="Product Name">
                        </div>
                        <div class="form-group has-feedback">
                            <label>Product Sales Price</label>
                            <input required="" name="productPrice" class="form-control"
                                   placeholder="Product Sales Price">
                        </div>
                        <!--                        <div class="form-group has-feedback">
                                                    <label>Product Whole Sale Price</label>
                                                    <input required="" name="productWholeSalePrice" class="form-control" placeholder="Product Whole Sale Price">
                                                </div>-->
                        <div class="form-group has-feedback">
                            <label>Product Model</label>

                            <input required="" id="productModel" type="text" name="productModel"
                                   placeholder="Product Model" class="form-control">
                        </div>
                        <div class="form-group has-feedback">
                            <label>Product Barcode</label>
                            <div class="input-group input-group-sm">
                                <input required="" id="productCode" type="text" name="productCode"
                                       placeholder="Product Code" class="form-control">
                                <span class="input-group-btn">
                                    <button id="random" type="button" class="btn btn-flat"><i class="fa fa-random"></i></button>
                                </span>
                            </div>
                            <span class="help-block">You can scan your barcode or click random button</span>


                        </div>




                        <div class="row">
                            <div class="col-xs-4">

                            </div>
                            <div class="col-xs-4">

                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                            </div>

                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
    var randomString = function (length) {
        var text = "";
        var possible = "0123456";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };

    var codeLenghtCheck = function () {
        var code = $('#productCode').val();
        if (code.length < 6) {
            $("#productCodeError").text("Product Code must be minimum 6 digit lenght.");
        } else if (code.length > 6) {
            $("#productCodeError").text("Product Code must be maximum 6 digit lenght.");
        } else {
            $("#productCodeError").empty();
        }
    };

    $("#random").click(function () {
        $('#productCode').val(randomString(6));
        codeLenghtCheck();
    });

    $("#productCode").keyup(function () {
        codeLenghtCheck();
    });

    $("#productCode").change(function () {
        codeLenghtCheck();
    });

</script>