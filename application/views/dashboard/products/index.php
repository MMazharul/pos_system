<section class="content-header">
  <div class="col-sm-6" style="margin-bottom:10px;">
            <a href="<?php echo site_url('products/create'); ?>" class="btn btn-primary btn-sm">Add
                Product</a>
				</div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Products</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
				      
		<div class="row">
                <div class="col-xs-12 table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 5%;">SL.</th>
                                <th style="width: 15%;">Product Name</th>
                                <th style="width: 5%;">Model</th>
                                <th style="width: 8%;">BarCode</th>
                                <th style="width: 38%;">Product Category</th>
                                <th style="width: 8%;">Sales Price</th>
                                <?php
                                $user = $this->session->userdata('user');
                                if ($user != 3) {
                                    ?>
                                    <th style="width: 18%;">Action</th>
                                <?php }
                                ?>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $sl = 1; ?>
                            <?php foreach ($productList as $product) { ?>
                                <?php if ($product['softDelete'] == 0) { ?>
                                    <tr>
                                        <td><?php echo $sl; ?></td>
                                        <?php
                                        $productID = $product['productID'];
                                        ?>
                                        <td><a href="#" onclick="updateModal('<?php echo $productID; ?>')" data-toggle="modal" data-target="#myModal"><?php echo $product['productName']; ?></a></td>
                                        <td><?php echo $product['productModel']; ?></td>
                                        <td><?php echo $product['productCode']; ?></td>
                                        <td>
                                            <?php
                                            $cat_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_product_catagories', 'product_catagoriesID', $product['catagoryID']);
                                            echo $cat_name['catagoryName'];
                                            ?>
                                        </td>
                                        <td><?php echo $product['productPrice']; ?></td>
                                        <?php if ($user != 3) { ?>
                                            <td>
                                                <a style="margin-right: 5px;" href="<?php echo base_url('products/edit'); ?>/<?php echo $product['productID']; ?>" class="btn btn-primary btn-sm pull-left">Edit</a>
                                                <a href="<?php echo base_url('products/destroy'); ?>/<?php echo $product['productID']; ?>" onclick="return confirm('Are You sure, Your want to delete This!')" class="btn btn-danger btn-sm pull-left">Delete</a>
                                            </td>
                                        <?php }
                                        ?>
                                    </tr>
                                <?php } ?>

                                <?php $sl++; ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
	 <div class="row no-print">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> Print
            </button>
        </div>
    </div>
</section>



<script>
    function updateModal(productid) {
        //$('#productid').val(productid);
        $.ajax({
            url: "<?php echo site_url('products/viewProductInfo'); ?>",
            data: {sendId: productid},
            type: "POST", success: function (hr) {
                $("#showData").html(hr);


            }
        });
    }
</script>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Products Quantity Details</h4>
            </div>
            <div class="modal-body">
                <div id="showData">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
