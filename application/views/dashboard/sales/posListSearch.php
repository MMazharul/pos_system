<table  class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Sl</th>
        <th>Invoice No</th>
        <th>By sale</th>
        <th>Sales Date</th>
        <th>Net Total</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    

    <?php
    $url=$this->uri->segment('3');
    if($url!=''){
        $sl=$url+1;
    }else {
        $sl = 1;
    }
    $totalAmount='0.00'; foreach ($sales as $sale) {
        ?>
        <tr>
            <td><?php echo $sl; ?></td>
            <td><?php echo $sale->invoiceNo; ?></td>
            <td><?php if(!empty($sale->CardNum)){ echo $sale->CardNum; }else{ echo 'Cash'; } ?></td>
            <td><?php echo $sale->salesDate; ?></td>
            <td><?php echo $sale->netTotal; $totalAmount+=$sale->netTotal; ?></td>
            <td>
                <a style="margin-right: 5px;" target="_blank" href="<?php echo base_url('pos/show'); ?>/<?php echo $sale->invoiceNo; ?>" class="btn btn-success btn-sm pull-left">View</a>
                <?php if ($sale->returnValue <= 0) { ?>
                    <a style="margin-right: 5px;" href="<?php echo base_url('pos/returnSale'); ?>/<?php echo $sale->invoiceNo; ?>" class="btn btn-warning btn-sm pull-left">Return</a>
                <?php } ?>
                <!--                                <a style="margin-right: 5px;" href="--><?php //echo base_url('pos/posedit'); ?><!--/--><?php //echo $sale->invoiceNo; ?><!--" class="btn btn-primary btn-sm pull-left">Edit</a>  -->
                <a href="<?php echo base_url('pos/destroy'); ?>/<?php echo $sale->invoiceNo; ?>" onclick="return confirm('Are You sure, Your want to delete This!')" class="btn btn-danger btn-sm pull-left">Delete</a>
            </td>
        </tr>

        <?php $sl++; } ?>
    </tbody>
    <tfoot>
    <tr>
        <td>Total Amount</td>
        <td colspan="3"></td>
        <td ><?php echo $totalAmount; ?></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="6"><?php echo $links; ?></td>
    </tr>
    </tfoot>
</table>