<?php  $uriid = $this->uri->segment(3); ?>
<div class="row">
    <div class="col-md-2"></div>

    <div class="col-md-8"><br>
        <div class="box box-default">
            <div class="box-body">
                <div class="col-md-12" style="padding-left:5px;padding-right:5px;">
                    <div class="form-group">
                        <select disabled="disabled" class="form-control select2" style="width: 100%;">
                            <option>Walk In Customer</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left:5px;padding-right:5px;">
                    <div class="form-group">
                        <select disabled="disabled" name="warehouseID" class="form-control select2" style="width: 100%;">
                            <option value="4">POS Warehouse</option>
                            <option value=""> Select Warehouse </option>
                            <option value="1">Main Store</option>
                            <option value="2">Warehouse One</option>
                            <option value="3">Warehouse Two</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left:5px;padding-right:5px;">
                    <div class="form-group">
                        <input id="productName" class="form-control ui-autocomplete-input" placeholder="Scan/Search Product by Name/Code" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-12" style="padding-left:5px;padding-right:5px;">
                    <form action="<?php echo base_url('pos/update'); ?>" method="post">
                        <input type="hidden" name="salesDate" value="<?php echo $posLIst['salesDate']; ?>">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Product</th>
                                    <th style="text-align: center;">Price</th>
                                    <th style="width: 20%;text-align: center;">Qty</th>
                                    <th style="text-align: center;">Subtotal</th>
                                    <th><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                </tr>
                            </thead>
                            <tbody id="tableDynamic">
                                <?php foreach ($posLIst2 as $key => $each_item) {

     //dumpVar($each_item);

                                    ?>
                                    <tr>
                                        <td style="text-align: center;">
                                            <?php
                                            $product_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $each_item['productID']);
                                            echo $product_name['productName'];
                                            ?>
                                        </td>
                                        <td style="text-align: center;"><?php echo $each_item['price'] ?></td>
                                        <td style="text-align: center;">
                                            <input id="qty_e<?php echo $key; ?>" type="text" class="quantity form-control" name="qty[]" style="padding:5px;text-align: center;" value="<?php echo $each_item['quantity'] ?>" autocomplete="off">
                                            <input id="productID_e<?php echo $key; ?>" name="productID[]" value="<?php echo $each_item['productID'] ?>" type="hidden">
                                            <input id="inventory_e<?php echo $key; ?>" value="" type="hidden">
                                            <input id="price_e<?php echo $key; ?>" name="price[]" value="<?php echo $each_item['price'] ?>" type="hidden">
                                            <input id="warehouseID" name="warehouseID[]" value="" type="hidden">

                                        </td>
                                        <td class="totalprice" style="text-align: center;" id="total_e<?php echo $key ?>"><?php echo $each_item['price']; ?></td>
                                        <td style="text-align: center;"><div id="removeRow"><i class="fa fa-trash-o" style="cursor:pointer;"></i></div></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="text-align: center;">Vat Rate</th>
                                    <th style="text-align: center;" id="vatRate"><?php echo $config->vatRate; ?></th>
                                    <th style="text-align: center;">Vat Amount</th>
                                    <th style="text-align: center;">
                                        <input autocomplete="off"  value="<?php echo $posLIst['vat']; ?>" id="vatAmount" class="form-control" name="vat">
                                    </th>
                            <input id="subTotal" type="hidden" value="<?php echo $posLIst['subTotal']; ?>" name="subTotal">
                            </tr>
                            <tr>
                                <th width="25%" style="text-align: center;">Discount</th>
                                <th width="25%" style="text-align: center;">
                                    <input id="discount" type="text" value="<?php echo $posLIst['discount']; ?>" class="form-control" name="discount" style="padding:5px;text-align: center;" autocomplete="off">
                                </th>
                                <th width="25%" style="text-align: center;">Total</th>
                                <th width="25%" style="text-align: center;">
                                    <input name="POSEid" value="<?php echo $uriid;?>" type="hidden">
                                    <input name="warehauseid" value="<?php echo $posLIst['discount']; ?>" type="hidden">
                                    <input id="totalAmount" type="text" value="<?php echo $posLIst['netTotal']; ?>"  class="form-control" readonly="" name="totalAmount" style="padding:5px;text-align: center;">
                                </th>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th width="25%" style="text-align: center;">Total Paying</th>
                                    <th width="25%" style="text-align: center;">
                                        <input id="totalPaying" type="text"  value="<?php echo $posLIst['posPaying'] ?>" class="form-control" name="totalPaying" style="padding:5px;text-align: center;" autocomplete="off">
                                    </th>
                                    <th width="25%" style="text-align: center;">Change</th>
                                    <th width="25%" style="text-align: center;">
                                        <input id="balance" type="text" value="<?php echo $posLIst['posChange'] ?>" class="form-control" readonly="" name="balance" style="padding:5px;text-align: center;">
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-md-12" style="padding-left:5px;padding-right:5px;">
                            <div class="form-group">
                              <label>Note</label>
                                <input  class="form-control" name="note" value="<?php echo $posLIst['note'] ?>" placeholder="Note">
                            </div>
                        </div>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="text-align: center;">
                                        <button type="submit"  id="payment" class="btn btn-block btn-success btn-lg" onclick="return checkadd();">Update</button>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 MycssDesign">

    </div>
</div>



<script type="text/javascript">
    $(function() {


        var selectedClass = "";
        $(".fil-cat").click(function(){
            selectedClass = $(this).attr("data-catagory");
            //console.log(selectedClass);
            $(".productsRow .col-md-2").not("."+selectedClass).fadeOut();
            $(".productsRow").fadeTo(100, 0.1);
            $(".productsRow .col-md-2").not("."+selectedClass).fadeOut();
            setTimeout(function() {
                $("."+selectedClass).fadeIn();
                $(".productsRow").fadeTo(300, 1);
            }, 300);

        });

        var jsonProductList = '<?php echo $jsonProduct; ?>';
        //console.log(jsonProductList);

        $( "#productName" ).autocomplete({
            source: <?php echo $jsonProduct; ?>,
            response: function (event, ui) {
                if(ui.content){
                    if(ui.content.length == 1){

                        addRowProduct(ui.content[0].id,ui.content[0].inventory,ui.content[0].productPrice,ui.content[0].productName,ui.content[0].warehouseID);
                        $(this).val('');
                        $(this).focus();
                        $(this).autocomplete('close');
                        return false;
                    }else if(ui.content.length == 0){
                        $(this).val('');
                        $(this).focus();
                        $(this).autocomplete('close');
                        return false;
                    }else{}
                }
            },
            select: function (event, ui) {
                //console.log(ui);
                addRowProduct(ui.item.id,ui.item.inventory,ui.item.productPrice,ui.item.productName,ui.item.warehouseID);
                $(this).val(''); return false;
            }

        });

        var findTotal = function () {
            if ($("#tableDynamic tr").size() == 0){
                return $("#totalAmount").val(0);
            }
            $("#tableDynamic tr").each(function() {
                row_total = 0;
                $(".totalprice").each(function() {

                    row_total += Number(parseFloat($(this).text()));
                });
                vatRate = parseFloat($("#vatRate").text());
                vatAmount = row_total / 100 * vatRate;
                vatAmount = Math.round(vatAmount * 100) / 100;
                //console.log(vatRate);
                $("#subTotal").val(row_total);
                $("#vatAmount").val(vatAmount);
                return $("#totalAmount").val(row_total + vatAmount);

            });
        };

        var addRowProduct = function (id,inventory,price,value,warehouseID) {
            var productID = parseFloat($('#productID_'+id).val());
            var productQuantity = parseInt($('#qty_'+ id).val());
            var productInventory = parseFloat($("#inventory_" + id).val());
            var productPrice = parseFloat($("#price_" + id).val());


            //console.log(productInventory);
            if(productID == id){

                if(productInventory >= productQuantity + 1){
                    $('#qty_'+ id).val(productQuantity + 1);

                    if (!isNaN(productPrice)) {
                        var total = (productQuantity + 1) * productPrice;
                        $('#total_'+ id).text(total);
                    }
                    return findTotal();

                }else{
                    return findTotal();

                }

            }

            $('<tr><td style="text-align: center;">'+ value +'</td><td style="text-align: center;">'+ price +'</td><td style="text-align: center;"><input id="qty_'+id+'" type="text" class="quantity form-control" name="qty[]" style="padding:5px;text-align: center;" value="1" autocomplete="off"><input id="productID_'+id+'" name="productID[]" value="'+id+'" type="hidden"><input id="inventory_'+id+'" value="'+inventory+'" type="hidden"><input id="price_'+id+'" name="price[]" value="'+price+'" type="hidden"><input id="warehouseID" name="warehouseID[]" value="'+warehouseID+'" type="hidden"></td><td class="totalprice" style="text-align: center;" id="total_'+id+'">'+ price*1 +'</td><td style="text-align: center;"><div id="removeRow"><i class="fa fa-trash-o" style="cursor:pointer;"></i></div></td></tr>').appendTo('#tableDynamic');

                            return findTotal();
                        };

                        $(document).on("click", "#removeRow", function(e) {

                            var target = e.target;
                            $(target).closest('tr').remove();
                            findTotal();
                            $('#discount').val('');
                            $('#totalPaying').val('');
                            $('#balance').val('');
                            return $('#payment').prop('disabled',true);
                        });

                        $(document).on("keyup", ".quantity", function (event) {
                           //alert('test');
                            var quantity = parseFloat($(this).val());

                            var id_arr = $(this).attr('id');
                            var id = id_arr.split("_");
                            var element_id = id[id.length - 1];
                            var inventory = parseFloat($("#inventory_" + element_id).val());



                            var price = parseFloat($("#price_" + element_id).val());

                                      //  alert(price);

                            if(!isNaN(quantity)){
                                if(quantity > inventory){
                                    $("#total_"+element_id).text("");
                                    $( this ).css( "border", "2px solid red" );
                                    findTotal();
                                }else{
                                    $("#total_"+element_id).text(price*quantity);
                                    $( this ).css( "border", "1px solid #ddd" );
                                    findTotal();
                                }
                            }else{
                                $("#total_"+element_id).text("");
                            }
                        });


                        $('.singleProduct').on('click', function () {
                            var productID = this.id;
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url('pos/askInventory'); ?>",
                                data: 'productID=' + productID,
                                dataType: 'json',
                                success: function (data) {
                                    addRowProduct(data[0].inventory[0].productID,data[0].inventory[0].inventory,data[0].inventory[0].productPrice,data[0].inventory[0].productName,data[0].inventory[0].warehouseID);
                                }
                            });
                        });

                        $('#discount').keyup(function () {
                            findTotal();
                            var discount = parseFloat(this.value);
                            var total = parseFloat($("#totalAmount").val());
                            if (!isNaN(discount)) {
                                return $("#totalAmount").val(total - discount);
                            }else{
                                findTotal();
                            }

                        });
                        $('#totalPaying').keyup(function () {
                            totalPaying();
                        });

                        var totalPaying = function () {
                            var totalPaying = parseFloat( $("#totalPaying").val() );

                            var total = parseFloat($("#totalAmount").val());
                            if (!isNaN(totalPaying)) {
                                var balance = totalPaying - total;
                                $("#balance").val(Math.round(balance * 100) / 100);
                            }else{
                                $("#balance").val('');
                                return $('#payment').prop('disabled',true);
                            }

                            if($("#balance").val() >= 0){
                                return $('#payment').prop('disabled',false);
                            }else{
                                return $('#payment').prop('disabled',true);
                            }
                        };

                        $(document).on("keyup", "#vatAmount", function (event) {

                            var vatAmount = parseFloat(this.value);


                            var subTotal = parseFloat($('#subTotal').val());
                            var discount = parseFloat($('#discount').val());
                            var totalPaying = parseFloat($('#totalPaying').val());
                            var balance = parseFloat($('#balance').val());

                            if(isNaN(vatAmount)){
                                vatAmount = 0;
                            }
                            if(isNaN(discount)){
                                discount = 0;
                            }
                            if(isNaN(totalPaying)){
                                totalPaying = 0;
                            }

                            $("#totalAmount").val(subTotal + vatAmount - discount);
                            if(totalPaying > 0){
                                var remain = subTotal + vatAmount - discount;
                                var updatedBalance = totalPaying - remain;
                                $('#balance').val(Math.round(updatedBalance * 100) / 100);
                            }
                        });

                    });


                    var checkadd = function() {
                        var chk = confirm("Are you sure to update this record ?");
                        if (chk) {
                            return true;
                        } else {
                            return false;
                        }

                    };
</script>
