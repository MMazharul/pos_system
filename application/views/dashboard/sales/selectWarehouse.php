<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Select Warehouse For Sale</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <div class="box-body">
                    <div class="row" style="padding: 30px 0px;">
                        <div class="col-md-3">
                            <b> Select Warehouse For Sale </b>
                        </div>
                        <div class="col-md-6">
                            <input value="" id="warehouseName" name="warehouseName" class="form-control" placeholder="Warehouse Name">
                        </div>
                        <div class="col-md-3">
                            <a id="sale" class="btn btn-block btn-info">Create Sale</a>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
    $(function() {
        
        // Warehouse Name
        $("#warehouseName").autocomplete({
            source: 'suggestionWarehouseName',
            select: function (event, ui) {
                $("#warehouseName").val(ui.item.value);
                $("#sale").attr('href','<?php echo base_url('sales/create/'); ?>'+ui.item.id);
                return false;
            }
        });

	    
    });

    
</script>