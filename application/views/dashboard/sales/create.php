
<!-- Content Header (Page header) -->
<section class="content-header">
    <?php if ($this->session->flashdata('msg')) { ?>

        <div class="alert box alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>  


    <?php } ?>

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Sale</h3>

                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('sales/store'); ?>" method="post">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Client Name</label>
                                    <input value="" id="clientName" class="form-control" placeholder="Client Name">
                                    <input id="clientID" name="clientID" type="hidden">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Warehouse Name</label>
                                    <input readonly="" value="<?php echo $warehouses[0]->warehouseName; ?>" class="form-control" placeholder="Warehouse Name">
                                    <input id="warehouseID" value="<?php echo $warehouses[0]->warehouseID; ?>" name="warehouseID" type="hidden">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Invoice Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="invoiceDate" id="datepicker" class="form-control" placeholder="YYYY-MM-DD"> <!-- For Limit Today:  data-provide="datepicker" data-date-end-date="0d" -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Sales Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="salesDate" id="datepicker2" class="form-control" placeholder="YYYY-MM-DD"> <!-- For Limit Today:  data-provide="datepicker" data-date-end-date="0d" -->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Invoice No</label>
                                    <input value="" readonly="" id="invoiceNo" name="invoiceNo" class="form-control" placeholder="Invoice No">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>PO Reference</label>
                                    <input value="" name="poRef" class="form-control" placeholder="PO Reference">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Payment Terms</label>
                                    <input value="" name="payTerms" class="form-control" placeholder="Payment Terms">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Delivery Place </label>
                                    <input value="" name="deliveryPlace" class="form-control" placeholder="Delivery Place">
                                </div>
                            </div>

                        </div>
                        <div class="row" style="padding: 30px 0px;">
                            <div class="col-md-3">
                                <b> Search Product By Name or Barcode </b>
                            </div>
                            <div class="col-md-9">
                                <input value="" id="productName" class="form-control" placeholder="Search Product By Name or Barcode">
                            </div>
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">Products *</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th style="width: 12%;">Whole Sale Price</th>
                                            <th style="width: 10%;">Quantity</th>
                                            <th style="width: 11%;">Total Price</th>
                                            <th style="width: 6%;">#</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableDynamic">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">

                            </div>
                            <div class="col-md-4">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <td> <b> Sub Total </b></td>
                                            <td><input readonly="" id="subTotal" name="subTotal" class="form-control" placeholder="Sub Total"></td>
                                        </tr>
                                        <tr>
                                            <td> <b> Vat </b></td>
                                            <td><input readonly="" id="vat" name="vat" class="form-control" placeholder="Vat">
                                                <input type="hidden" id="vatRate" name="vatRate" value="<?php echo $config->vatRate; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td> <b> Grand Total </b></td>
                                            <td><input readonly="" id="grand" name="grandTotal" class="form-control" placeholder="Grand Total"></td>
                                        </tr>
                                        <tr>
                                            <td> <b> Discount </b></td>
                                            <td><input id="discount" name="discount" class="form-control" placeholder="Discount"></td>
                                        </tr>
                                        <tr>
                                            <td> <b> Net Total </b></td>
                                            <td><input readonly="" id="net" name="netTotal" class="form-control" placeholder="Net Total"></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"><button type="submit" class="btn btn-block btn-primary btn-flat" onclick="return checkadd();">Create Sale</button></div>
                            <div class="col-md-2">
                                <a href="<?php echo base_url('sales/create/'); ?><?php echo $warehouses[0]->warehouseID; ?>" class="btn btn-block btn-danger btn-flat">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
	
    // Random String Function
    var randomString = function(length) {
        var text = "";
        var possible = "0123456789";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };

    var addRowProduct = function (id,inventory,price,value,warehouseID,code) {
        var productID = parseFloat($('#productID_'+id).val());
        var productQuantity = parseInt($('#qty_'+ id).val());
        var productInventory = parseFloat($("#inventory_" + id).val());
        var productPrice = parseFloat($("#price_" + id).val());
        //console.log(productInventory);
        if(productID == id){
          
            if(productInventory >= productQuantity + 1){
                $('#qty_'+ id).val(productQuantity + 1);
          
                if (!isNaN(productPrice)) {
                    var total = (productQuantity + 1) * productPrice;
                    $('#total_'+ id).text(total);  
                }
                return findTotal();
          
            }else{
                return findTotal();
            
            }

        }

        $('<tr><td style="text-align: left;">'+ value +' ('+ code +')</td><td style="text-align: center;">'+ price +'</td><td style="text-align: center;"><input id="qty_'+id+'" type="text" class="quantity form-control" name="qty[]" style="padding:5px;text-align: center;" value="1" autocomplete="off"><input id="productID_'+id+'" name="productID[]" value="'+id+'" type="hidden"><input id="inventory_'+id+'" value="'+inventory+'" type="hidden"><input id="price_'+id+'" name="price[]" value="'+price+'" type="hidden"></td><td class="totalprice" style="text-align: center;" id="total_'+id+'">'+ price*1 +'</td><td style="text-align: center;"><div id="removeRow"><i class="fa fa-trash-o" style="cursor:pointer;"></i></div></td></tr>').appendTo('#tableDynamic');

                        return findTotal();
                    };

                    var findTotal = function () {
                        if ($("#tableDynamic tr").size() == 0){
                            return $("#subTotal").val(0);
                        }
                        $("#tableDynamic tr").each(function() {
                            row_total = 0;
                            $(".totalprice").each(function() {
              
                                row_total += Number(parseFloat($(this).text()));
                            });
                            $("#subTotal").val(row_total);
                            var vatRate = parseFloat($("#vatRate").val());
                            var vatAmount = row_total / 100 * vatRate;
                            $("#vat").val(vatAmount);
                            $("#grand").val(vatAmount + row_total);
                            var discount = parseFloat($("#discount").val());
                            if(isNaN(discount)){
                                discount = 0;
                            }
                            return $("#net").val(vatAmount + row_total - discount);

                        });
                    };



                    $("#invoiceNo").val('SALE-'+randomString(3));

                    // Client Name
                    $("#clientName").autocomplete({
                        source: '<?php echo base_url('sales'); ?>/suggestionClientName',
                        select: function (event, ui) {
                            $("#clientName").val(ui.item.value);
                            $("#clientID").val(ui.item.id);
                            return false;
                        }
                    });

	    

                    // Product Name
                    $( "#productName" ).autocomplete({
                        source: <?php echo $jsonProduct; ?>,
                        response: function (event, ui) {
                            if(ui.content){
                                if(ui.content.length == 1){
                
                                    addRowProduct(ui.content[0].id,ui.content[0].inventory,ui.content[0].productWholeSalePrice,ui.content[0].productName,ui.content[0].warehouseID,ui.content[0].productCode);
                                    $(this).val(''); 
                                    $(this).focus();
                                    $(this).autocomplete('close');
                                    return false;
                                }else if(ui.content.length == 0){
                                    $(this).val(''); 
                                    $(this).focus();
                                    $(this).autocomplete('close');
                                    return false;
                                }else{

                                }
                            }
                        },
                        select: function (event, ui) {
                            //console.log(ui);
                            addRowProduct(ui.item.id,ui.item.inventory,ui.item.productWholeSalePrice,ui.item.productName,ui.item.warehouseID,ui.item.productCode);
                            $(this).val(''); return false;
                        }

                    });

                    $(document).on("click", "#removeRow", function(e) {
                        var target = e.target;
                        $(target).closest('tr').remove();
                        $('#discount').val('');
                        findTotal();
          
          
                    });

                    $(document).on("keyup", ".quantity", function (event) {
                        var quantity = parseFloat($(this).val());
                        var id_arr = $(this).attr('id');
                        var id = id_arr.split("_");
                        var element_id = id[id.length - 1];
                        var inventory = parseFloat($("#inventory_" + element_id).val());
                        var price = parseFloat($("#price_" + element_id).val());
                        if(!isNaN(quantity)){
                            if(quantity > inventory){
                                $("#total_"+element_id).text("");
                                $( this ).css( "border", "2px solid red" );
                                findTotal();
                            }else{
                                $("#total_"+element_id).text(price*quantity);
                                $( this ).css( "border", "1px solid #ddd" );
                                findTotal();
                            }
                        }else{
                            $("#total_"+element_id).text("");
                        }
                    });
    
                    $('#discount').keyup(function () {
                        findTotal();
                    });
    
                    var today = new Date();  
                    var dd = today.getDate();  
          
                    var mm = today.getMonth()+1;   
                    var yyyy = today.getFullYear();  
                    if(dd<10)   
                    {  
                        dd='0'+dd;  
                    }   
          
                    if(mm<10)   
                    {  
                        mm='0'+mm;  
                    }   
                    today = yyyy+'-'+mm+'-'+dd;  
                    $("#datepicker").val(today);
                    $("#datepicker2").val(today);
    
                    function checkadd() {
                        var chk = confirm("Are you sure to add this record ?");
                        if (chk) {
                            return true;
                        } else {
                            return false;
                        }
                        ;
                    }
    
</script>