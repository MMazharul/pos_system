<!-- Content Header (Page header) -->
<section class="content-header">
    <?php if ($this->session->flashdata('msg')) { ?>
        <div class="alert box alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    <?php } ?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Return Sale</h3>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('pos/returnPosSaleUpdate'); ?>" method="post">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Client Name</label>
                                    <input value="Walk In Customer" readonly="readonly" id="clientName"
                                           class="form-control" placeholder="Client Name">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Warehouse Name</label>
                                    <input readonly="" value="<?php echo $sales[0]->warehouseName; ?>"
                                           class="form-control" placeholder="Warehouse Name">
                                    <input id="warehouseID" value="<?php echo $sales[0]->warehouseID; ?>"
                                           name="warehouseID" type="hidden">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Invoice Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="invoiceDate" readonly="readonly"
                                               value="<?php echo $sales[0]->invoiceDate; ?>" id="" class="form-control"
                                               placeholder="YYYY-MM-DD">
                                        <!-- For Limit Today:  data-provide="datepicker" data-date-end-date="0d" -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Sales Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="salesDate" readonly="readonly"
                                               value="<?php echo $sales[0]->salesDate; ?>" id="" class="form-control"
                                               placeholder="YYYY-MM-DD">
                                        <!-- For Limit Today:  data-provide="datepicker" data-date-end-date="0d" -->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label>Invoice No</label>
                                    <input value="<?php echo $sales[0]->invoiceNo; ?>" readonly="" id="invoiceNo"
                                           name="invoiceNo" class="form-control" placeholder="Invoice No">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <?php
                                $this->db->select('*');
                                $this->db->from('tbl_pos_accounts');
                                $this->db->where("accountID","3");
                                $this->db->order_by('accountID', 'DESC');
                                $query = $this->db->get();
                                $restults = $query->result_array();
                                 ?>
                                <div class="form-group has-feedback">
                                    <label>Return Surcharge *</label>
                                    <select id="bank_id" name="bank_id" class="proname form-control select2">
                                        <?php foreach ($restults as $item) { ?>
                                            <option value="<?php echo $item['accountID']; ?>" ><?php echo $item['accountName'] . '(' . $item['accountType'] . ')'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                        </div>

<!--                        <div class="col-sm-12" style="margin-bottom:10px;">-->
<!--                            <div class="row">-->
<!--                                <div class="col-sm-offset-8 col-md-4">-->
<!--                                    <div class="row">-->
<!--                                    <div class="form-group">-->
<!--                                        <div class="col-sm-6" for="returnCharge"-->
<!--                                             style="font-weight:bold;text-align:right">Return Charge-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-6">-->
<!--                                            <div class="row">-->
<!---->
<!--                                        </div>-->
<!--                                        </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped" style="border:1px solid #eee;">
                                    <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th style="width: 15%;">Sale Price</th>
                                        <th style="width: 15%;">Quantity</th>
                                        <th style="width: 15%;">Return Qty</th>
                                        <th style="width: 15%;">Total Price</th>
                                        <th style="width: 6%;">#</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tableDynamic">
                                    <?php foreach ($sales[0]->products as $product) { ?>
                                        <tr>
                                            <td><?php echo $product->productName; ?><input name="productID[]"
                                                                                           type="hidden"
                                                                                           value="<?php echo $product->productID; ?>">
                                            </td>
                                            <td id="price_<?php echo $product->productID; ?>"
                                                style="width: 12%;"><?php echo $product->price; ?></td>
                                            <td id="qty_<?php echo $product->productID; ?>"
                                                style="width: 10%;"><?php echo $product->quantity; ?></td>
                                            <td><input name="returnQuantity[]"
                                                       id="returnQuantity_<?php echo $product->productID; ?>_<?php echo $product->price; ?>"
                                                       class="returnQty form-control" placeholder="Return">
                                                <input name="quantity[]" value="<?php echo $product->quantity; ?>"
                                                       class="hidden"></td>
                                            <td id="total_<?php echo $product->productID; ?>" class="totalprice"
                                                style="width: 11%;"><?php echo $product->price * $product->quantity; ?></td>
                                            <td style="width: 6%;">#</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div style="font-size: 18px;" class="returnMsg"></div>
                            </div>
                            <div class="col-md-4">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td><b> Sub Total </b></td>
                                        <td><input value="<?php echo $sales[0]->subTotal; ?>" readonly="" id="subTotal"
                                                   name="subTotal" class="form-control" placeholder="Sub Total"></td>
                                    </tr>
                                    <tr>
                                        <td><b> Vat </b></td>
                                        <td><input value="<?php echo $sales[0]->vat; ?>" readonly="" id="vat" name="vat"
                                                   class="form-control" placeholder="Vat">
                                            <input value="<?php echo $config->vatRate; ?>" type="hidden" id="vatRate"
                                                   name="vatRate">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b> Grand Total </b></td>
                                        <td><input value="<?php echo $sales[0]->grandTotal; ?>" readonly="" id="grand"
                                                   name="grandTotal" class="form-control" placeholder="Grand Total">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b> Discount </b></td>
                                        <td><input readonly="readonly" value="<?php echo $sales[0]->discount; ?>"
                                                   id="discount" name="discount" class="form-control"
                                                   placeholder="Discount"></td>
                                    </tr>
                                    <tr>
                                        <td><b> Net Total </b></td>
                                        <td><input value="<?php echo $sales[0]->netTotal; ?>" readonly="" id="net"
                                                   name="netTotal" class="form-control" placeholder="Net Total"></td>
                                    </tr>
                                    <tr>
                                        <td><b> Paid </b></td>
                                        <td><input value="<?php echo $sales[0]->netTotal; ?>" readonly="" id="paid"
                                                   name="paid" class="form-control" placeholder="Paid">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b> Return Charged </b></td>
                                        <td><input value="0" id="returnCharge" name="returnCharge" class="form-control"
                                                   placeholder="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b> Return Amount </b></td>
                                        <td><input id="returnValue" name="returnValue" readonly class="form-control"
                                                   placeholder="Return Amount">

                                        </td>
                                    </tr>

                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-8 col-md-4">
                                <button type="submit" class=" btn btn-primary "
                                        onclick="return checkadd();">Return Sale
                                </button>

                                <a href="<?php echo base_url('pos/returnSale/'); ?><?php echo $sales[0]->invoiceNo; ?>"
                                   class=" btn btn-warning ">Reset</a>

                                <a href="<?php echo base_url('pos/salesList/'); ?><?php echo $sales[0]->invoiceNo; ?>"
                                   class=" btn btn-danger ">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<script type="text/javascript">
    $(document).on("keyup", ".returnQty", function (event) {

        var returnQty = parseFloat($(this).val());
        var id_arr = $(this).attr('id');
        var id = id_arr.split("_");
        var element_id = id[id.length - 2];
        var saleQty = parseFloat($('#qty_' + element_id).text());
        var price = parseFloat($('#price_' + element_id).text());

        if (!isNaN(returnQty)) {
            if (returnQty > saleQty) {
                alert('Try to return same or lower value!');
                $(this).val('');
                findTotal();
                return false;
            }

            var total = parseFloat($('#total_' + element_id).text());

            var newTotal = returnQty * price;


            total_return_price = 0;
            $(".returnQty").each(function () {
                var deduct_total_quantity = Number(parseFloat($(this).val()));
                var product_price = this.id.split('_');
                var product_ori_price = parseFloat(product_price[2]);
                if (!isNaN(deduct_total_quantity)) {
                    total_return_price += deduct_total_quantity * product_ori_price;
                }
            });
            //alert(total_return_price);


            if (total > 0) {
                $('#total_' + element_id).text(total - newTotal);
                findTotal();
            } else {
                findTotal();
            }

            var returnChrge = $("#returnCharge").val();

            var netTotal = $("#net").val();

            var paid = $("#paid").val();

            var returnValue = total_return_price - returnChrge;

            total_return_price = Math.round(returnValue * 100) / 100;
            $("#returnValue").val(returnValue);
            $('.returnMsg').html('Return Amount <b>' + total_return_price + '</b>');
        } else {
            $('#total_' + element_id).text(saleQty * price);
            findTotal();
            var netTotal = $("#net").val();
            var paid = $("#paid").val();
            var returnValue = paid - netTotal;
            returnValue = Math.round(returnValue * 100) / 100;
            $("#returnValue").val(returnValue);
            $('.returnMsg').html('Return Amount <b>' + returnValue + '</b>');
        }

    });

    var findTotal = function () {
        if ($("#tableDynamic tr").size() == 0) {
            return $("#subTotal").val(0);
        }
        $("#tableDynamic tr").each(function () {
            row_total = 0;
            $(".totalprice").each(function () {

                row_total += Number(parseFloat($(this).text()));
            });
            $("#subTotal").val(row_total);
            var vatRate = parseFloat($("#vatRate").val());
            var vatAmount = row_total / 100 * vatRate;
            vatAmount = Math.round(vatAmount * 100) / 100;
            $("#vat").val(vatAmount);
            $("#grand").val(vatAmount + row_total);
            var discount = parseFloat($("#discount").val());
            if (isNaN(discount)) {
                discount = 0;
            }
            var netTotal = vatAmount + row_total - discount;
            netTotal = Math.round(netTotal * 100) / 100;
            return $("#net").val(netTotal);

        });
    };


    function checkadd() {
        var chk = confirm("Are you sure to add this record ?");
        if (chk) {
            return true;
        } else {
            return false;
        }
        ;
    }

</script>