<style type="text/css">
  address p {
    margin: 0px;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  
</section>

<!-- Main content -->
<section class="content">

	<section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          Invoice
          <small class="pull-right" id="today"></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Client
        <address>
          <strong><?php echo $sales[0]->clientName ;?></strong><br>
          <?php echo $sales[0]->clientAddress ;?>
          Phone: <?php echo $sales[0]->clientPhone ;?><br>
          Email: <?php echo $sales[0]->clientEmail ;?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Warehouse
        <address>
          <strong><?php echo $sales[0]->warehouseName ;?></strong><br>
          <br>
          <b>PO Reference:</b> <?php echo $sales[0]->poRef ;?><br>
        	<b>Payment Terms:</b> <?php echo $sales[0]->payTerms ;?><br>
        	<b>Delivery Place:</b> <?php echo $sales[0]->deliveryPlace ;?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Invoice: <?php echo $sales[0]->invoiceNo ;?></b><br>
        <br>
        <b>Invoice Date:</b> <?php echo $sales[0]->invoiceDate ;?><br>
        <b>Sale Date:</b> <?php echo $sales[0]->salesDate ;?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Product Catagory</th>
            <th>Whole Sale Price</th>
            <th>Subtotal</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach($sales[0]->products as $product){?>
          <tr>
            <td><?php echo $product->quantity; ?></td>
            <td><?php echo $product->productName; ?></td>
            <td><?php echo $product->catagoryName; ?></td>
            <td><?php echo $product->price; ?></td>
            <td><?php echo $product->price * $product->quantity; ?></td>
          </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">
        <h4>Payment Details</h4>
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th>Paid Amount</th>
              <th>Date</th>
            </tr>
            <?php $payment = 0;?>
            <?php foreach($dues as $due){?>
            <?php $payment = $due->transactionAmount + $payment;?>
            <tr>
              <td><?php echo $due->transactionAmount; ?></td>
              <td><?php echo $due->created_at; ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>        
      </div>
      <!-- /.col -->
      <div class="col-xs-6">
        <p class="lead">Amount</p>

        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Sub Total:</th>
              <td><?php echo $sales[0]->subTotal ;?></td>
            </tr>
            <tr>
              <th>Vat</th>
              <td><?php echo $sales[0]->vat ;?></td>
            </tr>
            <tr>
              <th>Grand Total:</th>
              <td><?php echo $sales[0]->grandTotal ;?></td>
            </tr>
            <tr>
              <th>Discount:</th>
              <td><?php echo $sales[0]->discount ;?></td>
            </tr>
            <tr>
              <th>Net Total:</th>
              <td><?php echo $sales[0]->netTotal ;?></td>
            </tr>
            <?php if($payment > 0){?>
            <tr>
              <th>Paid Amount:</th>
              <td><?php echo $payment ;?></td>
            </tr>
            <?php } ?>
            <?php if($sales[0]->dueAmount > 0){?>
            <tr>
              <th>Due Amount:</th>
              <td><?php echo $sales[0]->dueAmount ;?></td>
            </tr>
            <?php } ?>
            <?php if($sales[0]->returnValue){?>
            <tr>
              <th>Returned Amount:</th>
              <td><?php echo $sales[0]->returnValue ;?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
    	<div class="col-xs-2">
    		<button type="button" onclick="window.print();" class="no-print btn btn-block btn-success btn-flat"><i class="fa fa-print"></i>  Print</button>
    	</div>
      <div class="col-xs-2">
        <button type="button" data-toggle="modal" data-target="#payModal" class="no-print btn btn-block btn-default btn-flat">Pay</button>
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

</section>
<!-- /.content -->


<div class="modal fade" id="payModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Add Payment</h4>
      </div>
      <form action="<?php echo base_url('sales/pay'); ?>" method="post" class="form-horizontal">
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    <label class="col-xs-4">Invoice No</label>
                    <div class="col-xs-8">
                    <input readonly="readonly" value="<?php echo $sales[0]->invoiceNo ;?>" name="invoiceNo" class="form-control col-xs-8" placeholder="Invoice No">
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-xs-4">Due Amount</label>
                    <div class="col-xs-8">
                    <input readonly="readonly" value="<?php echo $sales[0]->dueAmount ;?>" name="dueAmount" id="dueAmount" class="form-control col-xs-8" placeholder="Due Amount">
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-xs-4">Select Account</label>
                    <div class="col-xs-8">
                      <select name="accountID" id="accountID" class="form-control select2" style="width: 100%;">
                        <option value="">Select Account</option>
                        <?php foreach($accounts as $account){?>
                        <option value="<?php echo $account->accountID;?>"><?php echo $account->accountName;?></option>
                        <?php } ?>
                      </select>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-xs-4">Account Balance</label>
                    <div class="col-xs-8">
                    <input readonly="readonly" id="accountBalance" name="accountBalance" class="form-control col-xs-8" placeholder="Account Balance">
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-xs-4">Amount</label>
                    <div class="col-xs-8">
                    <input readonly="readonly" id="paid" name="paid" class="form-control col-xs-8" placeholder="Amount">
                    <div style="color: red; font-weight: bold;" id="errors"></div>
                    </div>
                    
                </div>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button id="submit" disabled="" type="submit" class="no-print btn  btn-success btn-flat">Pay</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = yyyy+'/'+mm+'/'+dd;
	//document.write(today);
	$('#today').text('Date - '+today);


  $('#accountID').on('change', function() {
    var accountID = this.value;
    $.ajax({
            type: "POST",
            url: "<?php echo base_url('cashbook/askAccountBalance'); ?>",
            data: 'accountID=' + accountID,
            dataType: 'json',
            success: function (data) {
                $('#accountBalance').val(data[0].balance);
                var dueAmount = parseFloat($('#dueAmount').val());
                if(data[0].balance >= dueAmount){
                  $("#paid").attr("readonly", false); 
                  $('#submit').prop("disabled", false);
                }else{
                  $("#paid").attr("readonly", true); 
                  $('#submit').prop("disabled", true);
                }
            }
        });
  });

  $("#paid").keyup(function() {
    var paid = this.value;
    var dueAmount = parseFloat($('#dueAmount').val());
    if(dueAmount < paid){
      $('#errors').html('Please enter valid amount');
      $('#submit').prop("disabled", true);
    }else{
      $('#errors').html('');
      $('#submit').prop("disabled", false);
    }
  });
</script>
