<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Pos Sales</h3>
            <?php if ($this->session->flashdata('msg')) { ?>
                <?php echo $this->session->flashdata('msg'); ?>
                <?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-sm-offset-8 col-sm-4" style="margin-bottom:10px;">
                    <form action="" method="post">
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="date" class="form-control pull-right" value="<?php echo    date('Y/m/1 - Y/m/d') ?>" id="reservation">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <button type="submit" name="submitBtn" class="btn btn-primary"><i
                                class="fa fa-search"></i>&nbsp;Search
                            </button>
                        </div>
                    </form>
                </div>
                <!--            <div class="col-sm-3 col-sm-offset-3">-->
                    <!--                <input type="text" name="searchData" id="searchData" class="form-control" placeholder="Search....">-->
                    <!--            </div>-->
                    <script>
                       $( "#searchData" ).keypress(function( event ) {
                        var searchData = $(this).val();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('pos/salesList'); ?>",
                            data: 'searchData=' + searchData,
                            success: function (data) {
                                $("#showAjaxData").html(data);

                            }
                        });
                    });
                </script>
				<div class="row">
                <div class="col-xs-12 table-responsive">
                <table  id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Invoice No</th>
                            <th>By sale</th>
                            <th>Sales Date</th>
                            <th>Sales Time</th>
                            <th>Net Total</th>
                            <th style="width:195px">Note</th>
                            <th style="width:213px">Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <?php
                        $url=$this->uri->segment('3');
                        if($url!=''){
                            $sl=$url+1;
                        }else {
                            $sl = 1;
                        }
                        $totalAmount='0.00'; foreach ($sales as $sale) {
                           ?>
                           <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $sale->invoiceNo; ?></td>
                            <td><?php if(!empty($sale->CardNum)){ echo $sale->CardNum; }else{ echo 'Cash'; } ?></td>
                            <td><?php echo $sale->salesDate; ?></td>
                            <td><?php $date=date_create($sale->created_at); echo date_format($date,"h:i:s a"); ?></td>
                            <td><?php echo $sale->netTotal; $totalAmount+=$sale->netTotal; ?></td>
                            <td><?=$sale->note ?></td>
                            <td>
							<?php
                                    $user=$this->session->userdata('user');
                                    if($user!=3)
                                    {
                                        if($sale->returnValue <= 0)
                                        {
                                            ?>
                                <a style="margin-right: 5px;" target="_blank" href="<?php echo base_url('pos/show'); ?>/<?php echo $sale->invoiceNo; ?>" class="btn btn-success btn-sm pull-left">View</a>

                                            <a style="margin-right: 5px;" href="<?php echo base_url('pos/returnSale'); ?>/<?php echo $sale->invoiceNo; ?>" class="btn btn-warning btn-sm pull-left">
                                                Return
                                            </a>
                                            <?php
                                        }
                                        ?>
                                                                        <a style="margin-right: 5px;" href="<?php echo base_url('pos/posedit'); ?>/<?php echo $sale->invoiceNo; ?>"
																		class="btn btn-primary btn-sm pull-left">Edit</a>
                                        <a href="<?php echo base_url('pos/destroy'); ?>/<?php echo $sale->invoiceNo; ?>" onclick="return confirm('Are You sure, Your want to delete This!')" class="btn btn-danger btn-sm pull-left">Delete</a>
                                        <?php
                                    }
                                ?>
                                    </td>
                                </tr>

                                <?php $sl++; } ?>
                            </tbody>
                            <!--<tfoot>
                                                   <!-- <tr>-->
                                    <!--                        <td>Total Amount</td>-->
                                    <!--                        <td colspan="3"></td>-->
                                    <!--                        <td >--><?php //echo $totalAmount; ?><!--</td>-->
                                    <!--                        <td></td>-->
                                    <!--                    </tr>-->
                                   <!-- <tr>-->
                                        <!--<td colspan="6"><?php // echo $links; ?></td>-->
                                   <!-- </tr>-->
                                <!--</tfoot>-->
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </section>
<!-- /.content -->
