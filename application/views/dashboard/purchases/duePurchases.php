<style>
    .mycssClass{height: 365px}
</style>
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Unpaid Purchases</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Purchase No</th>
                                <th>Supplier Name</th>
                                <th>Primary Warehouse</th>
                                <th>Due Date</th>
                                <th>Sub Total</th>
                                <th>Due Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($purchases as $purchase) { ?>
                                <tr>
                                    <td><?php echo $purchase->purchaseNo; ?></td>
                                    <td><?php echo $purchase->supplierName; ?></td>
                                    <td><?php echo $purchase->warehouseName; ?></td>
                                    <td><?php echo $purchase->dueDate; ?></td>
                                    <td><?php echo $purchase->subTotal; ?></td>
                                    <td><?php echo $purchase->dueAmount; ?></td>
                                    <td>
                                        <a style="margin-right: 5px;" href="<?php echo base_url('purchases/show'); ?>/<?php echo $purchase->purchaseNo; ?>" class="btn btn-success btn-sm pull-left">View</a>
                                        <a style="margin-right: 5px;" onclick="updateModal('<?php echo $purchase->purchaseID; ?>')"  data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm pull-left">Pay</a> </td>
                                </tr>

                            <?php } ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

    </div>
</section>
<!-- /.content -->




<script>
    function updateModal(purchaseid){
        $('#purchaseid').val(purchaseid);

        $.ajax({
            url:"<?php echo site_url('purchases/viewPurchaseInfo'); ?>",
            data:{sendId:purchaseid},
            type:"POST",
            success:function(hr){
                $("#showData").html(hr);	
                        

            }
        });
    }
</script>
<script>

    $(function () {
        $(".bank_id").change(function () {
            var bank_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>cashbook/getbankavailable/",
                data: 'bank_id=' + bank_id,
                dataType: 'json',
                success: function (data) {
                    $('.av_amount').val(data.bavbalance);
                }
            });
        });
    });
</script>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Due Pay</h4>
            </div>
            <div class="modal-body mycssClass">
                <form action="<?php echo site_url('purchases/pay'); ?>" method="post">
                    <div class="form-group has-feedback">
                        <div id="showData">

                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Product Category</label>
                        <select name="account_id" class="form-control select2 bank_id" style="width: 100%;">
                            <option value="">-- Account--</option>
                            <?php foreach ($account as $productcatagory) { ?>
                                <option value="<?php echo $productcatagory->accountID; ?>"><?php echo $productcatagory->accountName; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Remaining Amount</label>
                        <input  id="mySelect" onchange="myFunction()" readonly class="form-control av_amount" placeholder="Pay Amount">
                    </div>
                    <div class="form-group has-feedback">
                        <label>Pay Amount</label>
                        <input name="payamount" id="mySelect2" onchange="myFunction()" class="form-control" placeholder="Pay Amount" required>
                    </div>
                    <div class="form-group has-feedback pull-right">
                        <button type="submit" name="submitBtn" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

<script>
    function myFunction() {
        var cashamount = document.getElementById("mySelect").value;
        var payamount = document.getElementById("mySelect2").value;
        var dueamount = document.getElementById("myid3").value;
     
        if(parseInt(cashamount) < parseInt(payamount)){
            alert('Amount can not be lessthan bank amount');
            $("#mySelect2").val('');
        }
        
        if( parseInt(dueamount) < parseInt(payamount)){
            alert('Due Amount can not be lessthan Paid amount');
            $("#mySelect2").val('');
        }
    }
</script>