<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Purchases</h3>
                    <?php if ($this->session->flashdata('msg')) { ?>

                        <?php echo $this->session->flashdata('msg'); ?>

                    <?php } ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Purchase No</th>
                                <th>Supplier Name</th>
                                <th>Primary Warehouse</th>
                                <th>Due Date</th>
                                <th>Sub Total</th>
                                <th>Due Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($purchases as $purchase) { ?>
                                <tr>
                                    <td><?php echo $purchase->purchaseNo; ?></td>
                                    <td><?php echo $purchase->supplierName; ?></td>
                                    <td><?php echo $purchase->warehouseName; ?></td>
                                    <td><?php echo $purchase->dueDate; ?></td>
                                    <td><?php echo $purchase->subTotal; ?></td>
                                    <td><?php echo $purchase->dueAmount; ?></td>
                                    <td>
                                        <a style="margin-right: 5px;" href="<?php echo base_url('purchases/show'); ?>/<?php echo $purchase->purchaseNo; ?>" class="btn btn-success btn-sm pull-left">View</a>
                                        <a style="margin-right: 5px;" href="<?php echo base_url('purchases/pur_edit'); ?>/<?php echo $purchase->purchaseNo; ?>" class="btn btn-primary btn-sm pull-left">Edit</a> 
                                        <a href="<?php echo base_url('purchases/destroy'); ?>/<?php echo $purchase->purchaseNo; ?>" class="btn btn-danger btn-sm pull-left">Delete</a></td>
                                </tr>

                            <?php } ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

    </div>
</section>
<!-- /.content -->