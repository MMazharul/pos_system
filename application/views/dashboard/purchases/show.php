<style type="text/css">
    address p {
        margin: 0px;
    }
</style>

<!-- Content Header (Page header) -->
<section class="content-header">

</section>

<!-- Main content -->
<section class="content">

    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    Purchase
                    <small class="pull-right" id="today"></small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Supplier
                <address>
                    <strong><?php echo $purchases[0]->supplierName; ?></strong><br>
                    Phone: <?php echo $purchases[0]->supplierPhone; ?><br>
                    Email: <?php echo $purchases[0]->supplierEmail; ?><br>
                    <?php echo $purchases[0]->supplierAddress; ?><br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                Warehouse
                <address>
                    <strong><?php echo $purchases[0]->warehouseName; ?></strong><br>
                    Phone: <?php echo $purchases[0]->warehousePhone; ?><br>
                    <?php // echo $purchases[0]->warehouseAddress; ?><br>

                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Purchase No <?php echo $purchases[0]->purchaseNo; ?></b><br>
                <br>
                <b>PO Reference:</b> <?php echo $purchases[0]->poRef; ?><br>
                <b>Payment Terms:</b> <?php echo $purchases[0]->payTerms; ?><br>
                <b>Purchase Date:</b> <?php echo $purchases[0]->purchaseDate; ?><br>
                <b>Due Date:</b> <?php echo $purchases[0]->dueDate; ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>Code</th>
                            <th>Product</th>
                            <th>Product Category</th>
                            <th>Qty</th>
                            <th>Unit Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($purchases[0]->products as $product) { ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $product->productCode; ?></td>
                                <td><?php echo $product->productName; ?></td>
                                <td><?php echo $product->catagoryName; ?></td>
                                <td><?php echo $product->quantity; ?></td>
                                <td><?php echo $product->purchasePrice; ?></td>
                                <td><?php echo $product->quantity * $product->purchasePrice; ?></td>
                            </tr>	
                        <?php } ?>

                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-8">


                <b>Note:</b> <?php echo $purchases[0]->note; ?>

            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <p class="lead">Amount</p>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:71%">Subtotal:</th>
                            <td><?php echo $purchases[0]->subTotal; ?></td>
                        </tr>
                        <tr>
                            <th>Discount:</th>
                            <td><?php echo $purchases[0]->discount; ?></td>
                        </tr>
                        <tr>
                            <th>Net Total:</th>
                            <td><?php echo $purchases[0]->netTotal; ?></td>
                        </tr>
                        <tr>
                            <th>Due Amount:</th>
                            <td><?php echo $purchases[0]->dueAmount; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</section>
<!-- /.content -->

<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '/' + mm + '/' + dd;
    //document.write(today);
    $('#today').text(today);
</script>