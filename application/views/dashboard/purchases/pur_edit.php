<!-- Content Header (Page header) -->
<section class="content-header">
    <?php if ($this->session->flashdata('msg')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    <?php } ?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">New Purchase</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('purchases/update'); ?>" method="POST" role="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div id="select_box">
                                        <label>Supplier<sup>*</sup></label>
                                        <input type="text" id="supplierName" placeholder="Supplier Name" class="form-control" required>
                                        <input type="hidden" name="supplierID" id="supplierID"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Purchase No.</label>
                                    <input name="purchaseNo" value="<?php echo $edit_info1['purchaseNo']; ?>" type="text" readonly  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>PO Reference</label>
                                    <input name="poRef" type="text" value="<?php echo $edit_info1['poRef'] ?>" placeholder="Po / Reference" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Payment Terms</label>
                                    <input name="payTerms" value="<?php echo $edit_info1['payTerms'] ?>" type="text" placeholder="Payment Terms" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Purchase Date<sup>*</sup></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="purchaseDate" id="datepicker" value="<?php echo $edit_info1['purchaseDate'] ?>" class="form-control" placeholder="YYYY-MM-DD"> <!-- For Limit Today:  data-provide="datepicker" data-date-end-date="0d" -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Due Date<sup>*</sup></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="dueDate" id="datepicker2" value="<?php echo $edit_info1['dueDate'] ?>" class="form-control" placeholder="YYYY-MM-DD">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                    $this->db->select('*');
                                    $this->db->from('tbl_pos_warehouses');
                                    $this->db->order_by("warehouseID","desc");
                                    $query = $this->db->get();
                                    $result = $query->result_array();
//                                    dumpVar($result);
                                    ?>
                                    <label>Warehouse</label>
                                    <select name="warehouseID" class="form-control select2" style="width: 100%;">
                                        <?php foreach ($result as $eachhead) { ?>
                                            <option value="<?php echo $eachhead['warehouseID']; ?>"><?php echo $eachhead['warehouseName']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="box-header">
                        <h3 class="box-title" style="margin-bottom: 15px">Products *</h3>
                            <input type="text" id="productName_1" data-type="productName" placeholder="Scan/Search Product by Name/Code" class="productName form-control">
                        </div><!-- /.box-header -->
                        <div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">SL.</th>
                                        <th>Product Name</th>
                                        <th style="width: 10%;">Quantity</th>
                                        <th style="width: 12%;">Purchase Price</th>
                                        <th style="width: 11%;">Total Price</th>
                                        <th style="width: 6%;">#</th>
                                    </tr>
                                </thead>
                                <tbody id="tableDynamic">

                                    <?php
                                    $i = 1;
                                    foreach ($edit_info as $key => $each_item) {
                                        ?>
                                        <tr>
                                            <td id="p_serial"><?php echo $i++; ?></td>
                                            <td>
                                                <?php
                                                $product_name = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $each_item['productID']);
                                                ?>
                                                <input type="text" id="productName_<?php echo $key; ?>" value="<?php echo $product_name['productName']?>" data-type="productName" placeholder="Product Name" class="productName form-control">
                                                <input type="hidden" name="productID[]" id="productID_<?php echo $key; ?>" value="<?php echo $each_item['productID']?>" class="form-control">
                                            </td>
                                            <td><input type="text" name="quantity[]" id="quantity_<?php echo $key; ?>" value="<?php echo $each_item['quantity']?>" placeholder="Quantity" class="quant form-control"></td>
                                            <td><input type="text" name="purchasePrice[]"  placeholder="Purchase Price" id="unitcost_<?php echo $key; ?>" value="<?php echo $each_item['purchasePrice']?>"  class="purchaseprice form-control"></td>

                                            <td><input type="text" id="totalprice_<?php echo $key; ?>" value="<?php echo $valueofprice = $each_item['quantity'] *  $each_item['purchasePrice']?>" name="totalPrice[]"  readonly placeholder="Total Price.." class="totalprice form-control"></td>
                                            <td><a href="javascript:void(0);" id="deleteRow_<?php echo $key; ?>"  class="deleteRow btn btn-danger btn-flat btn-sm">Delete</a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <table class="table">
                                <tr>
                                    <td style="width: 66%;border-top: medium none;"></td>
                                    <td style="width: 15%;border-top: medium none; text-align: right;" colspan="">Sub Total</td>
                                    <td style="border-top: medium none;">
                                        <input name="subTotal" type="text" id="subtotal" value="<?php echo $edit_info1['subTotal'] ?>" readonly placeholder="Sub Total Price.." class="form-control"></td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="border-top: medium none;">
                                        <label for="">Note<sup>*</sup></label>
                                        <textarea class="textarea" name="note"  placeholder="Note" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $edit_info1['note'] ?></textarea>
                                    </td>
                                </tr>

                                <tr>

                                    <td style="border-top: medium none; text-align: right;" colspan="">Discount</td>
                                    <td style="border-top: medium none;">
                                        <input type="text" name="discount" id="discount" value="<?php echo $edit_info1['discount'] ?>"  name="discount" placeholder="Discount.." class="form-control" value="0"></td>
                                </tr>


                                <tr>
                                    <td style="border-top: medium none; text-align: right;" colspan="">Net Total</td>
                                    <td style="border-top: medium none;">
                                        <input  type="text" id="net_total" name="netTotal"  value="<?php echo $edit_info1['netTotal'] ?>" readonly placeholder="Net Total.." class="form-control"></td>
                                </tr>
                                <tr>
                                    <td style="border-top: medium none; text-align: right;" colspan="">Due Amount</td>
                                    <td style="border-top: medium none;">
                                        <input type="text" id="dueamount"  readonly value="<?php echo $edit_info1['dueAmount'] ?>"  name="dueAmount" placeholder="Due Amount.." class="form-control"></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat"  onclick="return checkadd();">Update Purchase</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->


<script type="text/javascript">


    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }
    today = yyyy+'-'+mm+'-'+dd;
    $("#datepicker").val(today);
    $("#datepicker2").val(today);


    // Random String Function
    var randomString = function(length) {
        var text = "";
        var possible = "0123456789";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    };

    // Document Ready Function
    $(function() {
        $("#purchaseNo").val('PUR-'+randomString(3));
    });

    // Supplier Name
    $("#supplierName").autocomplete({
        source: '<?php echo site_url('Purchases/supplierNameSuggestions')?>',
        select: function (event, ui) {
            $("#supplierName").val(ui.item.value);
            $("#supplierID").val(ui.item.id);
            return false;
        }
    });

    // Warehouse
    $("#warehouseName").autocomplete({
        source: '<?php echo site_url('Purchases/warehouseNameSuggestions')?>',
        select: function (event, ui) {
            $("#warehouseName").val(ui.item.value);
            $("#warehouseID").val(ui.item.id);
            return false;
        }
    });

    // Product Name
    $(".productName").autocomplete({
        source: function (request, response) {
            $.getJSON("<?php echo base_url('sales'); ?>/get_product_list_by_branch", {term: request.term},
                response);
        },
        response: function (event, ui) {
            if (ui.content) {
                if (ui.content.length == 1) {
                    addRowProduct(ui.content[0].id, ui.content[0].productName, ui.content[0].productCode, ui.content[0].productModel);
                    var dataIns = $(this).val();
                    $(this).val('');
                    $(this).focus();
                    $(this).autocomplete('close');
                    return false;
                } else if (ui.content.length == 0) {
                    $(this).val('');
                    $(this).focus();
                    $(this).autocomplete('close');
                    return false;
                }
            }
        },
        select: function (event, ui) {
            addRowProduct(ui.item.id, ui.item.productName, ui.item.productCode, ui.item.model);
            $(this).val('');
            return false;
        }
    });

    var addRowProduct = function (id, value, productCode, model) {
        var i = $('#tableDynamic tr').size() + 1;

        if (model === undefined) {
            txt = "";
        } else {
            txt = '[' + model + ']';
        }

        $('<tr>\n\
            <td id="p_serial">' + i + '</td>\n\
            <td>\n\
                <input type="text" required id="productName_' + i + '" data-type="productName"  placeholder="Product Name" value="'+ value + ' [ ' + productCode + ']   ' + txt +'" class="productName form-control">\n\
                <input type="hidden" name="productID[]" id="productID_' + i + '" value="'+id+'"  class="productid form-control">\n\
            </td>\n\
            <td>\n\
                <input type="text" required name="quantity[]" id="quantity_' + i + '"  placeholder="Quantity" class="quant form-control">\n\
            </td>\n\
            <td>\n\
                <input type="text" required name="purchasePrice[]"  required placeholder="Purchasing Price" id="unitcost_' + i + '" class="purchaseprice form-control">\n\
            </td>\n\
            <td>\n\
                <input type="text" id="totalprice_' + i + '"   name="totalPrice[]" readonly placeholder="Total Price.." class="totalprice form-control">\n\
            </td>\n\
            <td>\n\
                <a href="javascript:void(0);" id="deleteRow_' + i + '"  class="deleteRow btn btn-danger btn-flat btn-sm">Delete</a>\n\
            </td>\n\
        </tr>').appendTo('#tableDynamic');
    };


    $(document).on("click", ".deleteRow", function (e) {
        var target = e.target;

        var id_arr = $(this).attr('id');
        var id = id_arr.split("_");
        var element_id = id[id.length - 1];

        var totalprice = parseFloat($("#totalprice_" + element_id).val());

        var subtotal = parseFloat($("#subtotal").val());
        var net_total = parseFloat($("#net_total").val());
        var dueamount = parseFloat($("#dueamount").val());

        if (!isNaN(totalprice)) {
            $("#subtotal").val(subtotal - totalprice);
            $("#net_total").val(net_total - totalprice);
            $("#dueamount").val(dueamount - totalprice);

            var subtotal = parseFloat($("#subtotal").val());
            var discount = parseFloat($("#discount").val());

            $("#net_total").append().val(subtotal - discount);
            $("#dueamount").append().val(subtotal - discount);
        }

        $(target).closest('tr').remove();

        var serial = 1;
        $("#tableDynamic tr").each(function () {
            $('tr:nth-child('+serial+')').find('#p_serial').text(serial);
            serial++;
        });
    });


    $(document).on("keyup", ".purchaseprice", function (event) {
        var unitprice = $(this).val();

        var id_arr = $(this).attr('id');
        var id = id_arr.split("_");
        var element_id = id[id.length - 1];
        var total = unitprice * $("#quantity_" + element_id).val();

        $("#totalprice_" + element_id).val(total);

        function findTotals() {
            $("#tableDynamic tr").each(function () {
                row_total = 0;
                $("td .totalprice").each(function () {
                    row_total += Number($(this).val());
                });
                $("#subtotal").append().val(row_total);
            });
        }
        row = findTotals();

        var subtotal = parseFloat($("#subtotal").val());
        var discount = parseFloat($("#discount").val());
        $("#net_total").append().val(subtotal - discount);
        $("#dueamount").append().val(subtotal - discount);
    });

    $(document).on("keyup", ".quant", function (event) {
        var quantity = $(this).val();
        var id_arr = $(this).attr('id');
        var id = id_arr.split("_");
        var element_id = id[id.length - 1];
        var total = quantity * $("#unitcost_" + element_id).val();
        $("#totalprice_" + element_id).val(total);

        function findTotals() {
            $("#tableDynamic tr").each(function () {
                row_total = 0;
                $("td .totalprice").each(function () {
                    row_total += Number($(this).val());
                });
                $("#subtotal").append().val(row_total);
            });
        }
        row = findTotals();

        var subtotal = parseFloat($("#subtotal").val());
        var discount = parseFloat($("#discount").val());

        $("#net_total").append().val(subtotal - discount);
        $("#dueamount").append().val(subtotal - discount);
    });

    $(document).on("keyup", "#discount", function (event) {
        var discount = $(this).val();
        var subtotal = parseFloat($("#subtotal").val());
        var dueamount = parseFloat($("#dueamount").val());

        $("#net_total").append().val(subtotal - discount);
        $("#dueamount").append().val(subtotal - discount);
    });

    function checkadd() {
        var chk = confirm("Are you sure to add this record ?");
        if (chk) {
            return true;
        } else {
            return false;
        }
    }


</script>
