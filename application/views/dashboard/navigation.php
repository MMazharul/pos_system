<?php
$uriValue = $this->uri->segment(1);
$uriValue2 = $this->uri->segment(2);
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?php
        $user = $this->session->userdata('user');
        if ($user == 3) {
            ?>
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="">
                    <a href="<?php echo base_url(); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboar</span>
                    </a>
                </li>
                <li <?php if ($uriValue == 'pos' || $uriValue == 'sales') { ?> class="treeview active"  <?php } ?> >
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Sales</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('pos/salesList'); ?>"><i class="fa fa-circle-o"></i> List of POS Sales</a></li>
                        <!--<li><a href="<?php // echo base_url('sales/index');                          ?>"><i class="fa fa-circle-o"></i> List of Sales</a></li>-->
                        <!--<li><a href="<?php // echo base_url('sales/dueSales');                   ?>"><i class="fa fa-circle-o"></i> List of Due Sales</a></li>-->
                        <!--<li><a href="<?php // echo base_url('sales/selectWarehouseForSale');                          ?>"><i class="fa fa-circle-o"></i> Add New Sale</a></li>-->
                    </ul>
                </li>
                <!--<li <?php if ($uriValue == 'products') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Products</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('products/index'); ?>"><i class="fa fa-circle-o"></i> List of Products</a></li>

                        <li><a href="<?php echo base_url('products/printBarcodes'); ?>"><i class="fa fa-circle-o"></i> Print Barcode / Label</a></li>
                    </ul>
                </li>-->
                <!--<li <?php if ($uriValue == 'inventory') { ?> class="treeview active" <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Inventory</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('inventory/index'); ?>"><i class="fa fa-circle-o"></i> Inventory </a></li>
                        --><!--<li><a href="<?php // echo base_url('inventory/adjustment');                 ?>"><i class="fa fa-circle-o"></i> Adjustment </a></li>--><!--

                    </ul>
                </li>-->

               <li <?php if ($uriValue == 'expenses' && $uriValue2 == '') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i>
                        <span>Expenses</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('expenses/index'); ?>"><i class="fa fa-circle-o"></i> List of Expenses</a></li>
                        <li><a href="<?php echo base_url('expenses/create'); ?>"><i class="fa fa-circle-o"></i> Add New Expense</a></li>
                    </ul>
                </li>
                
               <li <?php if ($uriValue == 'reports') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                      <i class="fa fa-share"></i>
                        <span>Report</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
					<ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Expense
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                               <!-- <li><a href="<?php echo base_url('reports/date_wise_expense_all_form'); ?>"><i class="fa fa-circle-o"></i>All Expense Reports</a></li>-->
                                <li><a href="<?php echo base_url('reports/date_wise_expense_form'); ?>"><i class="fa fa-circle-o"></i>Expense Reports</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!--<ul class="treeview-menu">
                        <li <?php if ($uriValue2 == 'dateWiseSales' || $uriValue2 == 'detailsSales') { ?> class="treeview active"  <?php } ?> >
                            <a href="#"><i class="fa fa-circle-o"></i> Sales
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWiseSales'); ?>"><i class="fa fa-circle-o"></i> Date Wise Sales Report</a></li>
                                <li><a href="<?php echo base_url('reports/detailsSales'); ?>"><i class="fa fa-circle-o"></i> Details Sales Report</a></li>
                                <li><a href="<?php echo base_url('reports/returnRepForm'); ?>"><i class="fa fa-circle-o"></i> Returned Report</a></li>
                                <li><a href="<?php  echo base_url('reports/vatReportInfo');                       ?>"><i class="fa fa-circle-o"></i> Vat Report</a></li>
                                --><!--<li><a href="<?php // echo base_url('reports/detailsClient');                       ?>"><i class="fa fa-circle-o"></i> Client Report</a></li>--><!--

                            </ul>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Inventory
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('inventory/index'); ?>"><i class="fa fa-circle-o"></i>All Inventory Reports</a></li>

                            </ul>
                        </li>

                    </ul>
                </li>-->

                <li>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Upload</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo base_url('welcome/postFile'); ?>">
                                <i class="fa fa-circle-o"></i>
                                Database Backup Online
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>

        <?php } elseif ($user == 2) {
            ?>
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="">
                    <a href="<?php echo base_url(); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li <?php if ($uriValue == 'pos' || $uriValue == 'sales') { ?> class="treeview active"  <?php } ?> >
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Sales</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('pos/salesList'); ?>"><i class="fa fa-circle-o"></i> List of POS Sales</a></li>
                        <!--<li><a href="<?php // echo base_url('sales/index');                          ?>"><i class="fa fa-circle-o"></i> List of Sales</a></li>-->
                        <li><a href="<?php echo base_url('sales/dueSales'); ?>"><i class="fa fa-circle-o"></i> List of Due Sales</a></li>
                        <!--<li><a href="<?php // echo base_url('sales/selectWarehouseForSale');                          ?>"><i class="fa fa-circle-o"></i> Add New Sale</a></li>-->
                    </ul>
                </li>
                <li <?php if ($uriValue == 'products') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Products</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('products/index'); ?>"><i class="fa fa-circle-o"></i> List of Products</a></li>
                        <li><a href="<?php echo base_url('products/create'); ?>"><i class="fa fa-circle-o"></i> Add New Product</a></li>
                        <li><a href="<?php echo base_url('products/printBarcodes'); ?>"><i class="fa fa-circle-o"></i> Print Barcode / Label</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'purchases') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Purchases</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('purchases/index'); ?>"><i class="fa fa-circle-o"></i> List of Purchases</a></li>
<!--                        <li><a href="--><?php //echo base_url('purchases/duePurchases'); ?><!--"><i class="fa fa-circle-o"></i> List of Unpaid Purchases</a></li>-->
                        <li><a href="<?php echo base_url('purchases/create'); ?>"><i class="fa fa-circle-o"></i> Add New Purchases</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'inventory') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Inventory</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('inventory/index'); ?>"><i class="fa fa-circle-o"></i> Inventory </a></li>
                        <li><a href="<?php echo base_url('inventory/adjustment'); ?>"><i class="fa fa-circle-o"></i> Adjustment </a></li>

                    </ul>
                </li>
                <li <?php if ($uriValue == 'expenses' && $uriValue2 == '') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i>
                        <span>Expenses</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('expenses/index'); ?>"><i class="fa fa-circle-o"></i> List of Expenses</a></li>
                        <li><a href="<?php echo base_url('expenses/create'); ?>"><i class="fa fa-circle-o"></i> Add New Expense</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'reports') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i>
                        <span>Report</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php if ($uriValue2 == 'dateWiseSales' || $uriValue2 == 'detailsSales') { ?> class="treeview active"  <?php } ?> >
                            <a href="#"><i class="fa fa-circle-o"></i> Sales
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWiseSales'); ?>"><i class="fa fa-circle-o"></i> Date Wise Sales Report</a></li>
                                <li><a href="<?php echo base_url('reports/detailsSales'); ?>"><i class="fa fa-circle-o"></i> Details Sales Report</a></li>
                                <li><a href="<?php echo base_url('reports/returnRepForm'); ?>"><i class="fa fa-circle-o"></i> Returned Report</a></li>
                                <li><a href="<?php  echo base_url('reports/vatReportInfo');                       ?>"><i class="fa fa-circle-o"></i> Vat Report</a></li>
                                <!--<li><a href="<?php // echo base_url('reports/detailsClient');                       ?>"><i class="fa fa-circle-o"></i> Client Report</a></li>-->

                            </ul>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li <?php if ($uriValue2 == 'dateWiseSales' || $uriValue2 == 'detailsSales') { ?> class="treeview active"  <?php } ?> >
                            <a href="#"><i class="fa fa-circle-o"></i> Purchase
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWisePurchse'); ?>"><i class="fa fa-circle-o"></i> Date Wise Purchase Report</a></li>
                                <!--<li><a href="<?php // echo base_url('reports/dateWiseSupplier');                      ?>"><i class="fa fa-circle-o"></i> Date Wise Supplier Report</a></li>-->

                            </ul>
                        </li>

                    </ul>


                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Inventory
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('inventory/index'); ?>"><i class="fa fa-circle-o"></i>All Inventory Reports</a></li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Expense
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/date_wise_expense_all_form'); ?>"><i class="fa fa-circle-o"></i>All Expense Reports</a></li>
                                <li><a href="<?php echo base_url('reports/date_wise_expense_form'); ?>"><i class="fa fa-circle-o"></i>Expense Reports</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i>Supplier 
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWiseSupplier'); ?>"><i class="fa fa-circle-o"></i>Supplier Ledger</a></li>
                                <!--here-->
                            </ul>
                        </li>

                    </ul>

                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Upload</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo base_url('welcome/postFile'); ?>">
                                <i class="fa fa-circle-o"></i>
                                Database Backup Online
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        <?php } else { ?>
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="">
                    <a href="<?php echo base_url(); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>

                </li>
                <li <?php if ($uriValue == 'pos' || $uriValue == 'sales') { ?> class="treeview active"  <?php } ?> >
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Sales</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('pos/salesList'); ?>"><i class="fa fa-circle-o"></i> List of POS Sales</a></li>
                       <!--<li><a href="<?php // echo base_url('sales/index');                          ?>"><i class="fa fa-circle-o"></i> List of Sales</a></li>-->
                        <!--<li><a href="<?php // echo base_url('sales/dueSales');                       ?>"><i class="fa fa-circle-o"></i> List of Due Sales</a></li>-->
                        <!--<li><a href="<?php // echo base_url('sales/selectWarehouseForSale');                          ?>"><i class="fa fa-circle-o"></i> Add New Sale</a></li>-->
                    </ul>
                </li>
                <li <?php if ($uriValue == 'products') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Products</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('products/index'); ?>"><i class="fa fa-circle-o"></i> List of Products</a></li>
                        <li><a href="<?php echo base_url('products/create'); ?>"><i class="fa fa-circle-o"></i> Add New Product</a></li>
                        <li><a href="<?php echo base_url('products/printBarcodes'); ?>"><i class="fa fa-circle-o"></i> Print Barcode / Label</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'inventory') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Inventory</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('inventory/index'); ?>"><i class="fa fa-circle-o"></i> Inventory </a></li>
                        <li><a href="<?php echo base_url('inventory/adjustment'); ?>"><i class="fa fa-circle-o"></i> Adjustment </a></li>

                    </ul>
                </li>
                           <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-share"></i> <span>Transfer</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="<?php echo base_url('transfers/index'); ?>"><i class="fa fa-circle-o"></i> List of Transfers</a></li>
                                    <li><a href="<?php echo base_url('transfers/create'); ?>"><i class="fa fa-circle-o"></i> Add New Transfers</a></li>
                                </ul>
                            </li>
                <li <?php if ($uriValue == 'purchases') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Purchases</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('purchases/index'); ?>"><i class="fa fa-circle-o"></i> List of Purchases</a></li>
                          <li><a href="<?php echo base_url('purchases/duePurchases'); ?>"><i class="fa fa-circle-o"></i> List of Unpaid Purchases</a></li>
                        <li><a href="<?php echo base_url('purchases/create'); ?>"><i class="fa fa-circle-o"></i> Add New Purchases</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'cashbook') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Cashbook</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Accounts
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('cashbook/Accountindex'); ?>"><i class="fa fa-circle-o"></i> List of Accounts</a></li>
                                <li><a href="<?php  echo base_url('cashbook/Accountcreate');                       ?>"><i class="fa fa-circle-o"></i> Add New Account</a></li>
                            </ul>
                        </li>

                        <li><a href="<?php echo base_url('cashbook/OpeningBalanceCreate'); ?>"><i class="fa fa-circle-o"></i> Add New Opening Balance</a></li>
                        <li>
                            <a href="<?php echo base_url('cashbook/transferadd') ?>">
                                <i class="fa fa-circle-o">

                        </i> Transfer Balance</a>
                        </li>
                        <li><a href="<?php echo base_url('cashbook/transactionHistory'); ?>"><i class="fa fa-circle-o"></i> Transaction History</a></li>
                        <li><a href="<?php echo base_url('cashbook/transferHistory'); ?>"><i class="fa fa-circle-o"></i> Transfer History</a></li>
                        <li><a href="<?php echo base_url('cashbook/balanceStatement'); ?>"><i class="fa fa-circle-o"></i> Balance Statement</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'expenses' && $uriValue2 == '') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i>
                        <span>Expenses</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('expenses/index'); ?>"><i class="fa fa-circle-o"></i> List of Expenses</a></li>
                        <li><a href="<?php echo base_url('expenses/create'); ?>"><i class="fa fa-circle-o"></i> Add New Expense</a></li>
                    </ul>
                </li>
                           <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-share"></i> <span>Payroll</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> List of Employee Expenses</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Add New Employee Expense</a></li>
                                </ul>
                            </li>
                  <li <?php if ($uriValue == 'clients') { ?> class="treeview active"  <?php } ?> >
                    <a href="#">
                        <i class="fa fa-circle-o"></i> 
                        <span>Client</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php  echo base_url('clients/index');                       ?>"><i class="fa fa-circle-o"></i> List of Clients</a></li>
                        <li><a href="<?php  echo base_url('clients/create');                       ?>"><i class="fa fa-circle-o"></i> Add New Client</a></li>
                    </ul>
                </li>
                <li <?php if ($uriValue == 'suppliers') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i> 
                        <span>Suppliers</span>

                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('suppliers/index'); ?>"><i class="fa fa-circle-o"></i> List of Suppliers</a></li>
                        <li><a href="<?php echo base_url('suppliers/create'); ?>"><i class="fa fa-circle-o"></i> Add New Supplier</a></li>
                        <li><a href="<?php echo base_url('Suppliers/supplierPayment'); ?>"><i class="fa fa-circle-o"></i>Supplier Payment</a></li>
                        <li><a href="<?php echo base_url('Suppliers/supplier_payment_summary'); ?>"><i class="fa fa-circle-o"></i>Payment Report</a></li>


                    </ul>
                </li>
                           <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-share"></i> <span>People</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Employees
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li><a href="<?php echo base_url('employees/index'); ?>"><i class="fa fa-circle-o"></i> List of Employees</a></li>
                                            <li><a href="<?php echo base_url('employees/create'); ?>"><i class="fa fa-circle-o"></i> Add New Employee</a></li>
                                        </ul>
                                    </li>
                                   
                                </ul>
                            </li>


                <li <?php if ($uriValue == 'reports') { ?> class="treeview active"  <?php } ?>>
                    <a href="#">
                        <i class="fa fa-share"></i>
                        <span>Report</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <?php if ($uriValue2 == 'dateWiseSales' || $uriValue2 == 'detailsSales') { ?> class="treeview active"  <?php } ?> >
                            <a href="#"><i class="fa fa-circle-o"></i> Sales
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWiseSales'); ?>"><i class="fa fa-circle-o"></i> Date Wise Sales Report</a></li>
                                <li><a href="<?php echo base_url('reports/detailsSales'); ?>"><i class="fa fa-circle-o"></i> Details Sales Report</a></li>
                                <li><a href="<?php echo base_url('reports/returnRepForm'); ?>"><i class="fa fa-circle-o"></i> Returned Report</a></li>
                                <li><a href="<?php  echo base_url('reports/vatReportInfo');                       ?>"><i class="fa fa-circle-o"></i> Vat Report</a></li>

                               <li><a href="<?php  echo base_url('reports/detailsClient');                       ?>"><i class="fa fa-circle-o"></i> Client Report</a></li>-->

                            </ul>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li <?php if ($uriValue2 == 'dateWiseSales' || $uriValue2 == 'detailsSales') { ?> class="treeview active"  <?php } ?> >
                            <a href="#"><i class="fa fa-circle-o"></i> Purchase
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWisePurchse'); ?>"><i class="fa fa-circle-o"></i> Date Wise Purchase Report</a></li>
                                <li><a href="<?php echo base_url('reports/dateWiseSupplier');                      ?>"><i class="fa fa-circle-o"></i> Date Wise Supplier Report</a></li>

                            </ul>
                        </li>

                    </ul>
                                       <ul class="treeview-menu">
                                            <li>
                                                <a href="#"><i class="fa fa-circle-o"></i> Product
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="<?php echo base_url('reports/date_wise_product_form');   ?>"><i class="fa fa-circle-o"></i> Date Wise Product Report</a></li>
                    
                                                </ul>
                                            </li>
                    
                                        </ul>


                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i>Supplier 
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/dateWiseSupplier'); ?>"><i class="fa fa-circle-o"></i>Supplier Ledger</a></li>
                                <!--here-->
                            </ul>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Inventory
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('inventory/index'); ?>"><i class="fa fa-circle-o"></i>All Inventory Reports</a></li>
                                <li><a href="<?php echo base_url('reports/itemReportForm'); ?>"><i class="fa fa-circle-o"></i>Item Wise</a></li>
                                <li><a href="<?php echo base_url('reports/damage_report'); ?>"><i class="fa fa-circle-o"></i>Damage Report</a></li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Expense
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/date_wise_expense_all_form'); ?>"><i class="fa fa-circle-o"></i>All Expense Reports</a></li>
                                <li><a href="<?php echo base_url('reports/date_wise_expense_form'); ?>"><i class="fa fa-circle-o"></i>Expense Reports</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Profit/loss
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/date_wise_Profitandloss_form'); ?>"><i class="fa fa-circle-o"></i>Profit/loss</a></li>
                                <li><a href="<?php echo base_url('reports/monthly_sum_rep'); ?>"><i class="fa fa-circle-o"></i>Overall Report</a></li>
                                <li><a href="<?php echo base_url('reports/bstatementForm'); ?>"><i class="fa fa-circle-o"></i>Account Statement</a></li>
<!--                                <li><a href="<?php echo base_url('reports/daily_report'); ?>"><i class="fa fa-circle-o"></i>Daily Statement</a></li>
                                <li><a href="<?php echo base_url('reports/monthly_report'); ?>"><i class="fa fa-circle-o"></i>Monthly Statement</a></li>-->
                            </ul>
                        </li>
                    </ul>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Summary
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('reports/daily_summary'); ?>"><i class="fa fa-circle-o"></i>Daily Summary</a></li>
                                <li><a href="<?php echo base_url('reports/monthly_summary'); ?>"><i class="fa fa-circle-o"></i>Monthly Summary</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!--                    <ul class="treeview-menu">
                                            <li>
                                                <a href="#"><i class="fa fa-circle-o"></i> Statement
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="<?php echo base_url('reports/daily_report'); ?>"><i class="fa fa-circle-o"></i>Daily Statement</a></li>
                                                    <li><a href="<?php echo base_url('reports/monthly_report'); ?>"><i class="fa fa-circle-o"></i>Monthly Statement</a></li>
                                                </ul>
                                            </li>
                                        </ul>-->
                </li>
                <li <?php if ($uriValue2 == 'ProductCatagoryIndex' || $uriValue2 == 'addexphead' || $uriValue2 == 'expHeadlist' || $uriValue2 == 'addexpsubhead' || $uriValue2 == 'expsubheadlist' || $uriValue2 == 'AppConfigIndex' || $uriValue2 == 'PosConfigIndex') { ?> class="treeview active"  <?php } ?> >
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Settings</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> User Management
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('settings/listUser'); ?>"><i class="fa fa-circle-o"></i> List of Users</a></li>
                                <li><a href="<?php echo base_url('settings/addUser'); ?>"><i class="fa fa-circle-o"></i> Add New User</a></li>
                            </ul>
                        </li>
                        <li <?php if ($uriValue2 == 'addexphead' || $uriValue2 == 'expHeadlist' || $uriValue2 == 'addexpsubhead' || $uriValue2 == 'expsubheadlist') { ?> class="treeview active"  <?php } ?>>
                            <a href="#">
                                <i class="fa fa-circle-o"></i> Expense Headings
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('expenses/addexphead'); ?>"><i class="fa fa-circle-o"></i> Add New Head</a></li>
                                <li><a href="<?php echo base_url('expenses/expHeadlist'); ?>"><i class="fa fa-circle-o"></i> List of Head</a></li>
                                <li><a href="<?php echo base_url('expenses/addexpsubhead'); ?>"><i class="fa fa-circle-o"></i> Add New Sub Head</a></li>
                                <li><a href="<?php echo base_url('expenses/expsubheadlist'); ?>"><i class="fa fa-circle-o"></i> List of Sub Head</a></li>
                            </ul>
                        </li>
                                           <li>
                                                <a href="#"><i class="fa fa-circle-o"></i> Payroll Payment Categories
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="<?php echo base_url('settings/DepartmentIndex'); ?>"><i class="fa fa-circle-o"></i> List of Payment Catagories</a></li>
                                                    <li><a href="<?php echo base_url('settings/DepartmentIndex'); ?>"><i class="fa fa-circle-o"></i> Add New Payment Catagory</a></li>
                                                </ul>
                                            </li>
                        <!--                    <li>
                                                <a href="#"><i class="fa fa-circle-o"></i> Departments
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="<?php echo base_url('settings/DepartmentIndex'); ?>"><i class="fa fa-circle-o"></i> List of Departments</a></li>
                                                    <li><a href="<?php echo base_url('settings/DepartmentCreate'); ?>"><i class="fa fa-circle-o"></i> Add New Department</a></li>
                                                </ul>
                                            </li>-->
                        <li <?php if ($uriValue2 == 'ProductCatagoryIndex' || $uriValue2 == 'ProductCatagoryCreate') { ?> class="treeview active"  <?php } ?>>
                            <a href="#"><i class="fa fa-circle-o"></i> Product Categories
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('settings/ProductCatagoryIndex'); ?>"><i class="fa fa-circle-o"></i> List of Catagories</a></li>
                                <li><a href="<?php echo base_url('settings/ProductCatagoryCreate'); ?>"><i class="fa fa-circle-o"></i> Add New Catagory</a></li>
                            </ul>
                        </li>
                                            <li>
                                                <a href="#"><i class="fa fa-circle-o"></i> Warehouses
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="<?php echo base_url('settings/WarehouseIndex'); ?>"><i class="fa fa-circle-o"></i> List of Warehouses</a></li>
                                                    <li><a href="<?php echo base_url('settings/WarehouseCreate'); ?>"><i class="fa fa-circle-o"></i> Add New Warehouse</a></li>
                                                </ul>
                                            </li>
                        <li><a href="<?php echo base_url('settings/AppConfigIndex'); ?>"><i class="fa fa-circle-o"></i> VAT Config</a></li>
                        <li><a href="<?php  echo base_url('settings/PosConfigIndex');                       ?>"><i class="fa fa-circle-o"></i> POS Config</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Upload</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo base_url('welcome/postFile'); ?>" id="database_backup_confirmation">
                                <i class="fa fa-circle-o"></i>
                                Database Backup Online
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        <?php }
        ?>


    </section>

    <script>
        $('#database_backup_confirmation').click(function(e) {
            var sure = confirm('Are you sure, you want to upload database to online and update your database?');
            if(!sure)
            {
                e.preventDefault();
            }
        });
    </script>

    <!-- /.sidebar -->
</aside>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       