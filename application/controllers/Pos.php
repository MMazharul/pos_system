<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Pos_model', 'POS', TRUE);
        $this->load->model('Sales_model', 'SALES', TRUE);
        $this->load->model('Inventory_model', 'INVENTORY', TRUE);
        $this->load->model('Settings_model', 'SETTINGS', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {

        $data['posConfig'] = $this->SETTINGS->posConfig();
        $data['warehouses'] = $this->SETTINGS->warehouses();
        $data['productcatagories'] = $this->POS->productcatagories($data['posConfig']->warehouseID);

        $data['inventory']         = $this->INVENTORY->inventory($data['posConfig']->warehouseID);
        $data['config']            = $this->SETTINGS->config();
        $data['jsonProduct']       = json_encode($this->POS->productInventoryWarehouseSuggestion('', $data['posConfig']->warehouseID));
        $this->load->view('dashboard/pos', $data);
    }
    function store()
    {
        if (isset($_POST['submitBtn'])) {
            $data['posConfig'] = $this->SETTINGS->posConfig();
            $invoiceNo         = 'POS-' . $this->generateRandomString();
            $products = $this->serializeProduct($invoiceNo, $this->input->post('productID'), $this->input->post('qty'), $this->input->post('price'), $this->input->post('purchasePrice'), $this->input->post('warehouseID'));

            if (!empty($products)) {
                if ($this->POS->store($invoiceNo, $this->input->post())) {
                    foreach ($products as $product) {
                        $this->SALES->storeSalesProduct($product, $this->input->post("saleDate"));

                        $this->SALES->deductInventory($product);
                    }
                    $values = $this->input->post('totalAmount_forCard');


                    $this->db->trans_start();
                    $this->POS->increaseIncome($invoiceNo, $values, $data['posConfig']->accountID, $this->input->post());
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        redirect(base_url('pos'));
                    } else {
                        redirect(base_url('pos/show/') . $invoiceNo);
                    }
                }
            } else {
                redirect(base_url('pos'));
            }
        }
    }

    public function serializePurchase($productID, $price)
    {
      $group = array();
      if (!empty($productID)) {
          foreach ($productID as $k => $name) {
              if (is_numeric($name) && is_numeric($price[$k])) {
                  $group[] = array(
                      'productID' => $name,
                      'purchasePrice' => $price[$k],
                  );
              }
          }
          return $group;
      } else {
          return false;
      }
    }

    function show($posNo)
    {
        $data              = array();
        $data['sales']     = $this->SALES->sales($posNo, 'POS');

        $data['appConfig'] = $this->COMMON_MODEL->getConfigInfo('*', 'app_config');
        $this->load->view('dashboard/pos-print', $data);
    }
    function askInventory()
    {
        $data['posConfig'] = $this->SETTINGS->posConfig();
        echo json_encode($this->INVENTORY->inventory($data['posConfig']->warehouseID, $this->input->post('productID')));
    }
    function askInventory_for_edit()
    {
        $data['posConfig'] = $this->SETTINGS->posConfig();
        echo json_encode($this->INVENTORY->inventory($data['posConfig']->warehouseID, $this->input->post('productID')));
    }
    private function generateRandomString($length = 6)
    {
        $characters       = '0123456789';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $pos   = 'POS-' . $randomString;
        $exits = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_sales', 'invoiceNo', $pos);
        if (!empty($exits)) {
            $this->generateRandomString();
        } else {
            return $randomString;
        }
    }
    function salesList()
    {
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if (isset($_POST['submitBtn'])) {
            extract($_POST);
            $expDate       = explode("-", $date);
            $data['sales'] = $this->SALES->salesDate('', 'POS', $expDate[0], $expDate[1], $page, '500');
        } else {
            $data['sales'] = $this->SALES->salesNew('', 'POS', $page, '500');
        }
        $pagination      = array(
            'url' => base_url() . "pos/salesList",
            'table' => 'tbl_pos_sales',
            'order_id' => 'saleID',
            'perPage' => '500',
            'fetchData' => $data['sales']
        );

        $pagination1     = $this->SALES->get_pagination($pagination);
        $data["sales"]   = $pagination1['result'];
        $data["links"]   = $pagination1['links'];
        $view            = array();
        $data['title']   = "Sales List";
        $view['content'] = $this->load->view('dashboard/sales/posList', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function destroy($id)
    {
        $this->COMMON_MODEL->delete_data('tbl_pos_sale_products', 'invoiceNo', $id);
        $this->COMMON_MODEL->delete_data('tbl_pos_sales', 'invoiceNo', $id);
        $this->COMMON_MODEL->delete_data('tbl_pos_inventory', 'refNo', $id);
        $this->COMMON_MODEL->delete_data('tbl_pos_transactions', 'refNo', $id);
        redirect(base_url('pos/salesList'));
    }
    function posedit($posID)
    {
        $data['posLIst']           = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_sales', 'invoiceNo', $posID);
        $data['posLIst2']          = $this->COMMON_MODEL->get_single_data_by_single_column2('tbl_pos_sales', 'invoiceNo', $posID);
        $data['posConfig']         = $this->SETTINGS->posConfig();
        $data['warehouses']        = $this->SETTINGS->warehouses();
        $data['productcatagories'] = $this->POS->productcatagories($data['posConfig']->warehouseID);
        $data['inventory']         = $this->INVENTORY->inventory($data['posConfig']->warehouseID);
        $data['config']            = $this->SETTINGS->config();
        $data['jsonProduct']       = json_encode($this->POS->productInventoryWarehouseSuggestion('', $data['posConfig']->warehouseID));
        $view                      = array();
        $data['title']             = "Pos Edit";
        $view['content']           = $this->load->view('dashboard/sales/posedit', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function update()
    {
        $data['vat']       = $this->input->post('vat');
        $data['subTotal']  = $this->input->post('subTotal');
        $data['discount']  = $this->input->post('discount');
        $data['netTotal']  = $this->input->post('totalAmount');
        $data['posPaying'] = $this->input->post('totalPaying');
        $data['posChange'] = $this->input->post('balance');
        $data['note'] = $this->input->post('note');
        $id = $this->input->post('POSEid');

        $wid = $this->input->post('warehauseid');

        $this->COMMON_MODEL->update_data('tbl_pos_sales', $data, 'invoiceNo', $id);
        $this->COMMON_MODEL->delete_data('tbl_pos_sale_products', 'invoiceNo', $id);
        extract($_POST);
        $fildCnt = count($qty);
        if ($fildCnt >= 1) {
            for ($i = 0; $i < $fildCnt; $i++) {
                $datas = array(
                    'quantity' => $qty[$i],
                    'productID' => $productID[$i],
                    'price' => $price[$i],
                    'invoiceNo' => $id,
                    'salesDate' => $this->input->post("salesDate")
                );
                $this->COMMON_MODEL->insert_data('tbl_pos_sale_products', $datas);
            }
        }
        $this->COMMON_MODEL->delete_data('tbl_pos_inventory', 'refNo', $id);
        extract($_POST);
        $fildCnt = count($qty);
        if ($fildCnt >= 1) {
            for ($i = 0; $i < $fildCnt; $i++) {
                $datass = array(
                    'warehouseID' => $wid,
                    'quantity' => $qty[$i],
                    'productID' => $productID[$i],
                    'type' => 'OUT',
                    'timestamp' => date("Y-m-d h:i:sa"),
                    'refNo' => $id
                );
                $this->COMMON_MODEL->insert_data('tbl_pos_inventory', $datass);
            }
        }
        $this->db->trans_start();
        $dataUpdate = $this->POS->increaseIncomeUpdate($id,$data['netTotal']);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            redirect(base_url('pos/posedit/').$id);
        } else {
          //  redirect(base_url('pos/show/') . $invoiceNo);
          redirect(base_url('pos/salesList'));
        }

    }
    function returnSale($posNo)
    {
        $data['sales']   = $this->SALES->sales($posNo, 'POS');
        $data['config']  = $this->SETTINGS->config();
        $data['title']   = "Return Sale";
        $data['content'] = $this->load->view('dashboard/sales/returnSaleForm', $data, TRUE);
        $this->load->view('dashboard/index', $data);
    }
    function returnPosSaleUpdate()
    {
        $products = $this->serializeReturnProduct($this->input->post('invoiceNo'), $this->input->post('productID'), $this->input->post('returnQuantity'), $this->input->post('warehouseID'));
        if (empty($products)) {
            $this->session->set_flashdata('msg', '<div style="font-weight:bold;display: inline-block;padding-right:5px;">Return Quantity should not be empty.</div> ');
            redirect(base_url('pos/returnSale/') . $this->input->post('invoiceNo'));
        }
        if ($this->POS->updateSaleByReturningProduct($this->input->post())) {
            foreach ($products as $key => $value) {
                $this->POS->updateSaleProductByReturningProduct($value);
                $this->POS->increaseInventoryByReturningProduct($value);
            }
            if ($this->POS->deductIncomeByReturningProduct($this->input->post())) {
                redirect(base_url('pos/show/') . $this->input->post('invoiceNo'));
            }
        }
    }
    private function serializeReturnProduct($invoiceNo, $productID, $quantity, $warehouseID)
    {
        $group = array();
        foreach ($productID as $k => $name) {
            if (is_numeric($quantity[$k])) {
                $group[] = array(
                    'invoiceNo' => $invoiceNo,
                    'productID' => $name,
                    'quantity' => $quantity[$k],
                    'warehouseID' => $warehouseID
                );
            }
        }
        return $group;
    }
    private function serializeProduct($invoiceNo, $productID, $quantity, $price,$purchasePrice, $warehouseID)
    {
        $group = array();
        if (!empty($productID)) {
            foreach ($productID as $k => $name) {
                if (is_numeric($quantity[$k]) && is_numeric($price[$k])) {
                    $group[] = array(
                        'invoiceNo' => $invoiceNo,
                        'productID' => $name,
                        'quantity' => $quantity[$k],
                        'price' => $price[$k],
                        'purchasePrice' => $purchasePrice[$k],
                        'warehouseID' => $warehouseID[$k]
                    );
                }
            }
            return $group;
        } else {
            return false;
        }
    }
}
