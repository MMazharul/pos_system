<?php defined('BASEPATH') or exit('No direct script access allowed');
class Reports extends CI_Controller
{
  public $users;
  function __construct()
  {
    parent::__construct();
    $user = $this->session->userdata('user');
    if (empty($user)) {
      $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
      redirect(base_url('login'));
    }
    $this->load->model('Reports_model', 'REPORTS', TRUE);
    $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    $this->load->model('Inventory_model', 'INVENTORYMODEL', TRUE);
    $this->load->model('Suppliers_model', 'SUPPLIERS', TRUE);
    $results = $this->db->select('userID, username')->from('tbl_pos_users')->get()->result_array();
    foreach ($results as $result) {
      $this->users[$result['userID']] = $result['username'];
    }
  }
  function monthly_summary()
  {
    $data = array();
    $view = array();
    $data['monthnames'] = array('01' => 'January', '02' => 'Febuary', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'Octabar', '11' => 'November', '12' => 'December');
    $data['title'] = " Report || Monthly Summary ";
    $view['content'] = $this->load->view('dashboard/summary/monthly_summary', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function monthly_summary_report()
  {
    $view = array();
    $date = $this->input->post('date');
    if (!empty($date)) {
      $search_month = $date;
      $data['search_month'] = $this->REPORTS->viewSearchingMonth($date);
    }
    $data = $this->REPORTS->monthly_purchases_avg_price_new($search_month);
    $data['monthly_account_oppening_summary'] = $this->REPORTS->monthly_account_oppening_summary($search_month);
    $data['monthly_account_closing_summary'] = $this->REPORTS->monthly_account_closing_summary($search_month);
    $data['monthly_sales_oppening_summary'] = $this->REPORTS->monthly_sales_oppening_summary($search_month);
    $data['monthly_sales_closing_summary'] = $this->REPORTS->monthly_sales_closing_summary($search_month);
    $data['monthly_expense_oppening_summary'] = $this->REPORTS->monthly_expense_oppening_summary($search_month);
    $data['monthly_expense_closing_summary'] = $this->REPORTS->monthly_expense_closing_summary($search_month);
    $data['monthly_purchases_oppening_summary'] = $this->REPORTS->monthly_purchases_oppening_summary($search_month);
    $data['monthly_purchases_closing_summary'] = $this->REPORTS->monthly_purchases_closing_summary($search_month);
    $data['total_quantities_opening'] = $this->db->select('SUM(quantity) total_quantity, productID')->from('tbl_pos_inventory')->where('MONTH(date) <', $search_month)->where('YEAR(date) <', date('Y'))->group_by('productID')->get()->result_array();
    $data['total_quantities_closing'] = $this->db->select('SUM(quantity) total_quantity, productID')->from('tbl_pos_inventory')->where('MONTH(date) <=', $search_month)->where('YEAR(date) <=', date('Y'))->group_by('productID')->get()->result_array();
    $data['title'] = "Report || Monthly Summary Result";
    $view['content'] = $this->load->view('dashboard/summary/monthly_summary_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function daily_summary()
  {
    $data = array();
    $view = array();
    $data['title'] = " Report || Daily Summary ";
    $view['content'] = $this->load->view('dashboard/summary/daily_summary', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function daily_summary_report()
  {
    $view = array();
    $date = $this->input->post('date');
    $data['date'] = $this->input->post('date');
    $data['daily_account_oppening_summary'] = $this->REPORTS->daily_account_oppening_summary($date);
    $data['daily_account_closing_summary'] = $this->REPORTS->daily_account_closing_summary($date);
    $data['daily_sales_oppening_summary'] = $this->REPORTS->daily_sales_oppening_summary($date);
    $data['daily_sales_closing_summary'] = $this->REPORTS->daily_sales_closing_summary($date);
    $data['daily_expense_oppening_summary'] = $this->REPORTS->daily_expense_oppening_summary($date);
    $data['daily_expense_closing_summary'] = $this->REPORTS->daily_expense_closing_summary($date);
    $data['daily_purchases_oppening_summary'] = $this->REPORTS->daily_purchases_oppening_summary($date);
    $data['daily_purchases_closing_summary'] = $this->REPORTS->daily_purchases_closing_summary($date);
    $data['total_quantities_opening'] = $this->db->select('SUM(quantity) total_quantity, productID')->from('tbl_pos_inventory')->where('date <', $date)->group_by('productID')->get()->result_array();
    $data['total_quantities_closing'] = $this->db->select('SUM(quantity) total_quantity, productID')->from('tbl_pos_inventory')->where('date <=', $date)->group_by('productID')->get()->result_array();
    $results = $this->db->select('AVG(pp.purchasePrice) avg_price, pp.productID')->from('tbl_pos_purchase_products pp')->join('tbl_pos_purchases p', 'p.purchaseNo = pp.purchaseNo', 'inner')->where('p.purchaseDate <=', $date)->where('p.warehouseID', 4)->group_by('pp.productID')->get()->result_array();
    $data['avg_price_purchase'] = [];
    foreach ($results as $result) {
      $data['avg_price_purchase'][$result['productID']] = $result['avg_price'];
    }
    $results = $this->db->select('AVG(price) avg_price, productID')->from('tbl_pos_inventory')->where('date <=', $date)->where('refNo', '')->where('type', 'IN')->group_by('productID')->get()->result_array();
    $data['avg_price_inventory'] = [];
    foreach ($results as $result) {
      $data['avg_price_inventory'][$result['productID']] = $result['avg_price'];
    }
    $data['title'] = "Report || Daily Summary Result";
    $view['content'] = $this->load->view('dashboard/summary/daily_summary_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function monthly_report()
  {
    $data = array();
    $view = array();
    $data['title'] = " Report || Monthly Report ";
    $view['content'] = $this->load->view('dashboard/overallreport/monthly_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function monthly_report_result()
  {
    $view = array();
    $date = $this->input->post('date');
    if (!empty($date)) {
      $search_month = date('m', strtotime($date));
      $data['search_year'] = date('Y', strtotime($date));
      $data['report_date'] = $date;
    }
    $data['purchases_pre_stock'] = $this->REPORTS->monthly_purchases_pre_stock($search_month);
    $data['purchases_today_stock'] = $this->REPORTS->monthly_purchases_today_stock($search_month);
    $data['purchases_closing_stock'] = $this->REPORTS->monthly_purchases_close_stock($search_month);
    $data['sales_pre_stock_inventory'] = $this->REPORTS->monthly_sales_pre_inventory($search_month);
    $data['sales_pre_stock'] = $this->REPORTS->monthly_sales_pre_stock_avg($search_month);
    $data['sales_today_inventory'] = $this->REPORTS->monthly_sales_today_inventory($search_month);
    $data['sales_today_stock'] = $this->REPORTS->monthly_sales_today_stock_avg($search_month);
    $data['sales_closing_inventory'] = $this->REPORTS->monthly_sales_close_inventory($search_month);
    $data['sales_closing_stock'] = $this->REPORTS->monthly_sales_close_stock_avg($search_month);
    $data['inventory_avg_price_stock'] = $this->REPORTS->monthly_inventory_avg_price_stock($search_month);
    $data['inventory_pre_stock'] = $this->REPORTS->monthly_inventory_pre_stock($search_month);
    $data['inventory_today_stock'] = $this->REPORTS->monthly_inventory_today_stock($search_month);
    $data['inventory_closing_stock'] = $this->REPORTS->monthly_inventory_close_stock($search_month);
    $data['total_pre_Amount'] = $this->REPORTS->monthly_account_pre_stock($search_month);
    $data['total_today_Amount'] = $this->REPORTS->monthly_account_today_stock($search_month);
    $data['total_closing_Amount'] = $this->REPORTS->monthly_account_close_stock($search_month);
    $data['total_expense_pre_Amount'] = $this->REPORTS->monthly_expense_pre_stock($search_month);
    $data['total_expense_today_Amount'] = $this->REPORTS->monthly_expense_today_stock($search_month);
    $data['total_expense_closing_Amount'] = $this->REPORTS->monthly_expense_close_stock($search_month);
    $data['title'] = "Report || Daily Report Result";
    $view['content'] = $this->load->view('dashboard/overallreport/monthly_report_result', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function daily_report()
  {
    $data = array();
    $view = array();
    $data['title'] = "Report || Daily Report";
    $view['content'] = $this->load->view('dashboard/overallreport/daily_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function daily_statement()
  {
    $view = array();
    $date = $this->input->post('date');
    $data['report_date'] = $date;
    $data['purchases_pre_stock'] = $this->REPORTS->daily_purchases_pre_stock($date);
    $data['purchases_today_stock'] = $this->REPORTS->daily_purchases_today_stock($date);
    $data['purchases_closing_stock'] = $this->REPORTS->daily_purchases_close_stock($date);
    $data['sales_pre_inventory'] = $this->REPORTS->daily_sales_pre_inventory($date);
    $data['sales_pre_stock'] = $this->REPORTS->daily_sales_pre_stock($date);
    $data['sales_today_inventory'] = $this->REPORTS->daily_sales_today_inventory($date);
    $data['sales_today_stock'] = $this->REPORTS->daily_sales_today_stock($date);
    $data['sales_closing_inventory'] = $this->REPORTS->daily_sales_close_inventory($date);
    $data['sales_closing_stock'] = $this->REPORTS->daily_sales_close_stock($date);
    $data['inventory_avg_price_stock'] = $this->REPORTS->daily_inventory_avg_price_stock($date);
    $data['inventory_pre_stock'] = $this->REPORTS->daily_inventory_pre_stock($date);
    $data['inventory_today_stock'] = $this->REPORTS->daily_inventory_today_stock($date);
    $data['inventory_closing_stock'] = $this->REPORTS->daily_inventory_close_stock($date);
    $data['total_pre_Amount'] = $this->REPORTS->daily_account_pre_stock($date);
    $data['total_today_Amount'] = $this->REPORTS->daily_account_today_stock($date);
    $data['total_closing_Amount'] = $this->REPORTS->daily_account_close_stock($date);
    $data['total_expense_pre_Amount'] = $this->REPORTS->daily_expense_pre_stock($date);
    $data['total_expense_today_Amount'] = $this->REPORTS->daily_expense_today_stock($date);
    $data['total_expense_closing_Amount'] = $this->REPORTS->daily_expense_close_stock($date);
    $data['title'] = "Report || Daily Report Result";
    $view['content'] = $this->load->view('dashboard/overallreport/dailyReportResult', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function dateWiseSales()
  {
    $data = array();
    $view = array();
    $data['title'] = "Report || Date Wise Sale";
    $view['content'] = $this->load->view('dashboard/reports/sales/date_wise_sales_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function date_wise_expense_form()
  {
    $view = array();
    $data['title'] = "Report || All Expense";
    $view['content'] = $this->load->view('dashboard/reports/Expense/date_wise_expense_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function itemReportForm()
  {
    $data = array();
    $view = array();
    $data['title'] = "Report || Item Wise Inventory ";
    $view['content'] = $this->load->view('dashboard/reports/inventory/itemReportForm', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  public function itemReportByDate()
  {
    $dt = new DateTime();
    $data['current_date'] = $dt->format('Y/m/d');
    $productID = $this->input->post('productID');
    $date_range = $this->input->post('date_range');
    if (empty($productID)) {
      $failed = "Please Select a product";
      $this->session->set_flashdata('failed', $failed);
      redirect('reports/itemReportForm');
    }
    if (empty($date_range)) {
      $failed = "Please enter Date Range";
      $this->session->set_flashdata('failed', $failed);
      redirect('itemReportForm');
    }
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $data['first_date'] = $dateexplode[0];
    $data['last_date'] = $dateexplode[1];
    $data['avail_stock'] = $this->INVENTORYMODEL->avail_stock($productID);
    $data['catSubItem'] = $this->INVENTORYMODEL->catSubItem($productID);
    $data['itemReportByDate'] = $this->INVENTORYMODEL->itemReportByDate($first_date, $last_date, $productID);
    $data['itemReportByDate2'] = $this->INVENTORYMODEL->itemReportByDate2($first_date, $last_date, $productID);
    $data['title'] = "Report || Item Repoert By Date";
    $view['content'] = $this->load->view('dashboard/reports/inventory/printItemReport', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function ajaxSubheadload()
  {
    $item_id = $this->input->post('head_id');
    $data['sub_head'] = $this->COMMON_MODEL->get_data_list_by_single_column('tbl_pos_exp_sub_head', 'head_id', $item_id);
    return $this->load->view('ajax_form_datas', $data);
  }
  function dateWisePurchse()
  {
    $view = array();
    $data['title'] = "Report || Date Wise Purchase";
    $view['content'] = $this->load->view('dashboard/reports/purchase/date_wise_purchase_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function dateWiseSupplier()
  {
    $view = array();
    $data['title'] = "Report || Supplier Ledger";
    $view['content'] = $this->load->view('dashboard/reports/purchase/details_supplier_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function date_wise_Profitandloss_form()
  {
    $view = array();
    $data['title'] = "Report || Profit / Loss form";
    $view['content'] = $this->load->view('dashboard/reports/profitandloss/date_wise_Profitandloss_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function date_wise_expense_all_form()
  {
    $view = array();
    $data['title'] = "Report || Expense";
    $view['content'] = $this->load->view('dashboard/reports/Expense/date_wise_expense_all_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function detailsSales()
  {
    $view = array();
    $data['title'] = "Sales Details Report Form";
    $view['content'] = $this->load->view('dashboard/reports/sales/details_sales_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function detailsClient()
  {
    $data = array();
    $view = array();
    $view['content'] = $this->load->view('dashboard/reports/sales/details_client_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function date_wise_product_form()
  {
    $data = array();
    $view = array();
    $view['content'] = $this->load->view('dashboard/reports/product/date_wise_product_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askDateWiseSalesReport()
  {
    $cDate = $this->input->post('date');
    if (empty($cDate)) {
      $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Date is Required!!!</div>');
      redirect(base_url('reports/askDateWiseSalesReport'));
    }
    unset($_SESSION['date_range']);
    $date_range = $this->input->post('date');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $data['date_range'] = $this->input->post('date');
    $data['reports'] = $this->REPORTS->salesReport($first_date, $last_date);
    $view = array();
    $data['title'] = "Report || Date Wise Sales Report";
    $view['content'] = $this->load->view('dashboard/reports/sales/date_wise_sales_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askDateWiseExpenseAllReport()
  {
    $cData = $this->input->post('date');
    if (empty($cData)) {
      $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Date is Required!!!</div>');
      redirect(base_url('reports/askDateWiseSalesReport'));
    }
    unset($_SESSION['date_range']);
    $date_range = $this->input->post('date');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $data['date_range'] = $this->input->post('date');
    $data['expense'] = $this->REPORTS->detailsExpenseReport($first_date, $last_date);
    $view = array();
    $data['title'] = "Report || Expense";
    $view['content'] = $this->load->view('dashboard/reports/Expense/date_wise_expense_all_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function profitLoss()
  {
    $date_range = $this->input->post('date_range');

    $type = $this->input->post('type');
    $dateexplode = explode("-", $date_range);

    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $data['first_date'] = $dateexplode[0];
    $data['last_date'] = $dateexplode[1];
    $data['totalsale'] = $this->REPORTS->querycurrenttotalsale($first_date, $last_date);
    $data['profitLoss'] = $this->COMMON_MODEL->profitLosss2($first_date, $last_date);
    $view = array();
    $data['total_discount'] = 0;
    if ($this->input->post('type') == 2) {
      $data['duePurchase'] = $this->REPORTS->duePurchase($first_date, $last_date);
      $data['accountBalanceHistory'] = $this->REPORTS->accountBalance();
      $data['totalSales'] = $this->REPORTS->totalSales($first_date, $last_date);
      $salesGoods = $this->REPORTS->salesGoods($first_date, $last_date);
      $data['costOfGoodsSold'] = $this->REPORTS->avgCostGoods($salesGoods, $first_date, $last_date);
      $data['totalPurchases'] = $this->REPORTS->totalPurchases($first_date, $last_date);

      $data['totalExpenses'] = $this->REPORTS->totalExpenses($first_date, $last_date);
      $view['content'] = $this->load->view('dashboard/reports/profitandloss/netProfitLoss', $data, TRUE);
      $this->load->view('dashboard/index', $view);
    } else {
      $data['title'] = "Report || Profit / Loss";
      $data['total_discount'] = $this->REPORTS->detailSalesRepDiscount($first_date, $last_date);
      $view['content'] = $this->load->view('dashboard/reports/profitandloss/profitLoss', $data, TRUE);
      $this->load->view('dashboard/index', $view);
    }
  }
  public function damage_report()
  {
    $data['title'] = "Report || Damage Report";
    $view['content'] = $this->load->view('dashboard/reports/inventory/damage_report_form', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  public function total_damage_report()
  {
    $date_range = $this->input->post('date_range');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $data['first_date'] = $dateexplode[0];
    $data['last_date'] = $dateexplode[1];
    $data['damage_report'] = $this->REPORTS->total_damage_report($first_date, $last_date);
    $data['title'] = "Report || Damage Report Result";
    $view['content'] = $this->load->view('dashboard/reports/inventory/damage_report_result', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askdetailsDateWiseExpenseHWReport()
  {
    $accHead = $this->input->post('head_id');
    if (empty($accHead)) {
      $this->session->set_flashdata('msg', '<div  style="color: red;text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Account Head is Required!!!</div>');
      redirect(base_url('reports/date_wise_expense_form'));
    }
    unset($_SESSION['date_range']);
    unset($_SESSION['head_id']);
    unset($_SESSION['sub_head_id']);
    $date_range = $this->input->post('date');
    $head = $this->input->post('head_id');
    $subhead = $this->input->post('sub_head_id');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $_SESSION['head_id'] = $this->input->post('head_id');
    $_SESSION['sub_head_id'] = $this->input->post('sub_head_id');
    $data['date_range'] = $this->input->post('date');
    if (!empty($subhead)) {
      $data['expenseHW'] = $this->REPORTS->detailsHWExpenseReportSH($first_date, $last_date, $subhead);
    } else {
      $data['expenseHW'] = $this->REPORTS->detailsHWExpenseReportH($first_date, $last_date, $head);
    }
    $view = array();
    $data['title'] = "Report || Details Expense Report";
    $view['content'] = $this->load->view('dashboard/reports/Expense/date_wise_expense_hw_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askdetailsDateWisePurchseReport()
  {
    unset($_SESSION['date_range']);
    $date_range = $this->input->post('date');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $data['date_range'] = $this->input->post('date');
    $category_id = false;
    if ($this->input->post('product_catagoriesID')) {
      $category_id = $this->input->post('product_catagoriesID');
    }
    $data['purchasereport'] = $this->REPORTS->detailsPurchseReport($first_date, $last_date, $category_id);
    $view = array();
    $data['title'] = "Report || Details Purchase Report";
    $view['content'] = $this->load->view('dashboard/reports/purchase/date_wise_purchse_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function detailSalesRep()
  {
    $date = explode('-', $this->input->post('date'));
    $data['date'] = $date[0] . '-' . $date[1];
    $data['product_catagoriesID'] = $this->input->post('product_catagoriesID');
    if ($data['product_catagoriesID']) {
      $data['result'] = $this->REPORTS->detailSalesRep($date[0], $date[1], $data['product_catagoriesID']);
    } else {
      $data['result'] = $this->REPORTS->detailSalesRep($date[0], $date[1], false);
      $data['discount'] = $this->REPORTS->detailSalesRepDiscount($date[0], $date[1]);
    }
    $data['title'] = "Detail Sales Report";
    $view['content'] = $this->load->view('dashboard/reports/sales/detailSalesRep', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askdetailsDateWiseClientReport()
  {
    $clientId = $this->input->post('clientid');
    if (empty($clientId)) {
      $this->session->set_flashdata('msg', '<div style="color: red ; text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Client is Required!!!</div>');
      redirect(base_url('reports/detailsClient'));
    }
    unset($_SESSION['date_range']);
    unset($_SESSION['clientid']);
    $date_range = $this->input->post('date');
    $clientid = $this->input->post('clientid');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $_SESSION['clientid'] = $this->input->post('clientid');
    $data['date_range'] = $this->input->post('date');
    $data['clientreport'] = $this->REPORTS->detailsClientReport($first_date, $last_date, $clientid);
    $view = array();
    $view['content'] = $this->load->view('dashboard/reports/sales/details_date_wise_client_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askdetailsDateWiseSupplierReport()
  {
    $supId = $this->input->post('supplierID');
    if (empty($supId)) {
      $this->session->set_flashdata('msg', '<div style="color: red; text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Supplier is Required!!!</div>');
      redirect(base_url('reports/dateWiseSupplier'));
    }
    unset($_SESSION['date_range']);
    unset($_SESSION['supplierID']);
    $supplierID = $this->input->post('supplierID');
    $date_range = $this->input->post('date');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $_SESSION['supplierID'] = $this->input->post('supplierID');
    $data['date_range'] = $this->input->post('date');
    $data['supplierInfo'] = $this->COMMON_MODEL->get_single_data_by_single_column("tbl_pos_suppliers", "supplierID", $supplierID);
    $data['supplierreport'] = $this->REPORTS->detailsSupplierReport($first_date, $last_date, $supplierID);
    $data['suppliers_payment'] = $this->REPORTS->supplierPaymentCollection($first_date, $last_date, $supplierID);
    $suppliersAllPayemnt = $this->SUPPLIERS->allpayment_by_id($supplierID);
    $suppliersAllDue = $this->SUPPLIERS->allDue_by_id($supplierID);
    $data['supplierPresentVal'] = $suppliersAllDue - $suppliersAllPayemnt;
    $view = array();
    $data['title'] = "Report || Supplier Ledger";
    $view['content'] = $this->load->view('dashboard/reports/purchase/details_date_wise_supplier_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function askdetailsDateWiseProductReport()
  {
    $productId = $this->input->post('productid');
    if (empty($productId)) {
      $this->session->set_flashdata('msg', '<div style="color: red; text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product is Required!!!</div>');
      redirect(base_url('reports/date_wise_product_form'));
    }
    unset($_SESSION['date_range']);
    unset($_SESSION['productid']);
    $productid = $this->input->post('productid');
    $date_range = $this->input->post('date');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $_SESSION['productid'] = $this->input->post('productid');
    $data['date_range'] = $this->input->post('date');
    $data['productreport'] = $this->REPORTS->detailsprocuctReport($first_date, $last_date, $productid);
    $view = array();
    $view['content'] = $this->load->view('dashboard/reports/product/details_date_wise_product_report', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function datewisesalessummary_exportbyxls($param = '')
  {
    $file = 'Date_Wise_Sales_Summary_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    extract($_POST);
    $data['first_date'] = $first_date;
    $data['last_date'] = $last_date;
    $data['reports'] = $this->REPORTS->salesReport($first_date, $last_date);
    $this->load->view('dashboard/reports/sales/date_wise_sales_report_export', $data);
  }
  function datewisesalesdetails_exportbyxls()
  {
    $file = 'Details_Date_Wise_Sales_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $data['reports'] = $this->REPORTS->detailssalesReport($first_date, $last_date);
    $this->load->view('dashboard/reports/sales/details_date_wise_sales_report_export', $data);
  }
  function datewisepurchse_exportbyxls()
  {
    $file = 'Date_Wise_Purchse_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $data['purchasereport'] = $this->REPORTS->detailsPurchseReport($first_date, $last_date);
    $this->load->view('dashboard/reports/purchase/date_wise_purchase_report_export', $data);
  }
  function datewiseproduct_exportbyxls()
  {
    $file = 'Date_Wise_product_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $productid = $_SESSION['productid'];
    $data['productreport'] = $this->REPORTS->detailsprocuctReport($first_date, $last_date, $productid);
    $this->load->view('dashboard/reports/product/date_wise_product_report_export', $data);
  }
  function datewiseClientdetails_exportbyxls()
  {
    $file = 'Date_Wise_client_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $clientid = $_SESSION['clientid'];
    $data['clientreport'] = $this->REPORTS->detailsClientReport($first_date, $last_date, $clientid);
    $this->load->view('dashboard/reports/sales/date_wise_client_report_export', $data);
  }
  function datewiseSupplierdetails_exportbyxls()
  {
    $file = 'Date_Wise_supplier_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $supplierID = $_SESSION['supplierID'];
    $data['supplierInfo'] = $this->COMMON_MODEL->get_single_data_by_single_column("tbl_pos_suppliers", "supplierID", $supplierID);
    $data['supplierreport'] = $this->REPORTS->detailsSupplierReport($first_date, $last_date, $supplierID);
    $data['suppliers_payment'] = $this->REPORTS->supplierPaymentCollection($first_date, $last_date, $supplierID);
    $this->load->view('dashboard/reports/purchase/date_wise_supplier_report_export', $data);
  }
  function datewiseExpensedetails_exportbyxls()
  {
    $file = 'Date_Wise_expense_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $data['expense'] = $this->REPORTS->detailsExpenseReport($first_date, $last_date);
    $this->load->view('dashboard/reports/Expense/datewiseExpensedetails_exportbyxls', $data);
  }
  function datewiseExpenseHW_exportbyxls()
  {
    $file = 'Date_Wise_expense_report_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    $dateexplode = explode("-", $_SESSION['date_range']);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $head = $_SESSION['head_id'];
    $subhead = $_SESSION['sub_head_id'];
    if (!empty($subhead)) {
      $data['expenseHW'] = $this->REPORTS->detailsHWExpenseReportSH($first_date, $last_date, $subhead);
    } else {
      $data['expenseHW'] = $this->REPORTS->detailsHWExpenseReportH($first_date, $last_date, $head);
    }
    $this->load->view('dashboard/reports/Expense/datewiseExpensedetailsHW_exportbyxls', $data);
  }
  public function monthly_sum_rep()
  {
    $dt = new DateTime();
    $data['current_date'] = $dt->format('Y/m/d');
    $view = array();
    $data['title'] = "Report || Date Wise Sale";
    $view['content'] = $this->load->view('dashboard/reports/profitandloss/monthly_sum_rep', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  public function printmonthly_sum_rep()
  {
    $dt = new DateTime();
    $data['month'] = $this->input->post('month');
    $data['year'] = $this->input->post('year');
    $month = $this->input->post('month');
    $year = $this->input->post('year');
    if (strlen($month) == 1) {
      $month = "0$month";
    }
    $firstDate = $year . "/" . $month . "/01";
    $lastDate = $year . "/" . $month . "/31";
    $createdDate = date_create($firstDate);
    $data['firstDate'] = $firstDate;
    $data['lastDate'] = $lastDate;
    $data['totalpurchase'] = $this->REPORTS->totalpurchase($data['month'], $data['year']);
    $data['overallcost'] = $this->REPORTS->overallcost($data['month'], $data['year']);
    $data['totalpurchasedue'] = $this->REPORTS->totalpurchasedue($data['month'], $data['year']);
    $data['totalsale'] = $this->REPORTS->totalsale($data['month'], $data['year']);
    $data['totalexpense'] = $this->REPORTS->totalexpense($data['month'], $data['year']);
    $data['title'] = "Report || Overall Reports";
    $view['content'] = $this->load->view('dashboard/reports/profitandloss/printview_overallreport', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function bstatementForm()
  {
    $view = array();
    $data['title'] = "Report || Bank Statement";
    $view['content'] = $this->load->view('dashboard/reports/profitandloss/bstatementForm', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  public function askdetailsDateWiseAccountReport()
  {
    $accNo = $this->input->post('accountID');
    if (empty($accNo)) {
      $this->session->set_flashdata('msg', '<div style="color: red; text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Account is Required!!!</div>');
      redirect(base_url('reports/bstatementForm'));
    }
    unset($_SESSION['date_range']);
    unset($_SESSION['accountID']);
    $accountID = $this->input->post('accountID');
    $date_range = $this->input->post('date');
    $dateexplode = explode("-", $date_range);
    $first_date = $dateexplode[0];
    $last_date = $dateexplode[1];
    $_SESSION['date_range'] = $this->input->post('date');
    $_SESSION['accountID'] = $this->input->post('accountID');
    $data['date_range'] = $this->input->post('date');
    $view = array();
    $data['title'] = "Report || Bank Statement";
    $view['content'] = $this->load->view('dashboard/reports/profitandloss/bank_statement_details', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function returnRepForm()
  {
    $data['title'] = "Returned Report Form";
    $data['content'] = $this->load->view('dashboard/reports/sales/return/returnRepForm', $data, TRUE);
    $this->load->view('dashboard/index', $data);
  }
  function returnRep()
  {
    $date = explode('-', $this->input->post('date'));
    $data['date'] = $date[0] . '-' . $date[1];
    $data['result'] = $this->REPORTS->returnRep($date[0], $date[1]);
    $data["returnCharge"] = $this->common_model->get_row("sum(returnCharge)as returnCharge", "tbl_pos_sales", array("returnDate >= " => $date[0], "returnDate <= " => $date[1]));
    $data['title'] = "Sales Return Report";
    $data['content'] = $this->load->view('dashboard/reports/sales/return/returnRep', $data, TRUE);
    $this->load->view('dashboard/index', $data);
  }
  function vatReportInfo()
  {
    if (isset($_POST['searchBtn'])) {
      extract($_POST);
      $firstData = explode("-", $date);
      $data['first_date'] = $firstData['0'];
      $data['last_date'] = $firstData['1'];
      $data['reports'] = $this->REPORTS->vatReport($firstData['0'], $firstData['1']);
    }
    $data['title'] = "Report || Vat Report";
    $view['content'] = $this->load->view('dashboard/reports/sales/vatReportInfo', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function vatReportInfo_exportbyxls()
  {
    $file = 'Date_Wise_Vat_Reports_' . date('d.m.Y') . '.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    header('Cache-Control: max-age=0');
    extract($_POST);
    $data['title'] = "Report || Vat Report";
    $data['reports'] = $this->REPORTS->vatReport($firstData, $lastDate);
    $this->load->view('dashboard/reports/sales/vatReportInfo_exportbyxls', $data);
  }
}
