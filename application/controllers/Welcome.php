<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
        $this->load->model('Cashbook_model', 'CASHBOOK', TRUE);
    }
    function index()
    {
        $data = array();
        $data['accountBalanceHistory'] = $this->CASHBOOK->accountBalance();

        $view  = array();
        $data['title'] = "Dashboard";

        $view['content'] = $this->load->view('dashboard/welcome', $data, TRUE);


        $this->load->view('dashboard/index', $view);
    }
    public function send_email()
    {
      $this->load->dbutil();
      $prefs = array(
          'format' => 'zip',
          'filename' => 'pos_system.sql'
      );
      $backup =& $this->dbutil->backup($prefs);

      $db_name = 'ptime-backup-on-' . date("Y-m-d-H-i-s") . '.zip';

      $save    = 'db/' . $db_name;

      $this->load->helper('file');
      write_file($save, $backup);

      $this->load->library('email');
		  $config = array();
		  $config['protocol'] = 'smtp';
		  $config['smtp_host'] = 'mail.sng27.com';
		  $config['smtp_user'] = 'mega@sng27.com';
		  $config['smtp_pass'] = 'i80ddnkViyls';
		  $config['smtp_port'] = 587;
		  $this->email->initialize($config);
		  $this->email->set_newline("\r\n");
		  $this->email->from('mega@sng27.com', 'MEGAMART BK');
		  $this->email->to('mazharulislam10000@gmail.com');
		  $this->email->subject('Email Verification');
		  $message = "Please click this link to verify your account:
      http://best_life_limited/verified/";
		  $this->email->message($message);
      $this->email->attach($save);

	 	  $send = $this->email->send();
      if($send)
      echo "Send";
      else
      echo "fali";

    }
    function dbmail()
    {
        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip',
            'filename' => 'pos_system.sql'
        );
        $backup =& $this->dbutil->backup($prefs);

        $db_name = 'ptime-backup-on-' . date("Y-m-d-H-i-s") . '.zip';
        $save    = 'db/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
        $this->load->library('email');
        $date = date('Y-m-d');
        $this->email->from('backup@abhworld.com', 'ptime');
        $this->email->to('mazharulislam10000@gmail.com');
        $this->email->subject('Database ' . $date);
        $this->email->message('Database Backup');
        $this->email->attach($save);
        $send = $this->email->send();
        if($send)
        {
          echo "send";
        }
        else
        {
          echo "Fail";
        }
        die;
        unlink($save);
    }
    public function postFile()
    {
        $this->load->dbutil();
        $prefs   = array(
            'format' => 'sql',
            'filename' => 'pos_system.sql'
        );
        $backup  = $this->dbutil->backup($prefs);
        $db_name = 'pos_system.sql';
        $save    = 'db/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
        $fields    = array(
            "f1" => "value1",
            "another_field2" => "anothervalue"
        );
        $filenames = array(
            $save
        );
        $files     = array();
        foreach ($filenames as $key => $f) {
            $files[$f] = file_get_contents($f);
        }
        $url       = "http://megamart.abh.one/api/getPostFile";
        $curl      = curl_init();
        $url_data  = http_build_query($fields);
        $boundary  = uniqid();
        $delimiter = '-------------' . $boundary;
        $post_data = $this->build_data_files($boundary, $fields, $files);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)
            )
        ));
        $response = curl_exec($curl);
        $info     = curl_getinfo($curl);
        var_dump($response);
        $err = curl_error($curl);
        curl_close($curl);
    }
    function build_data_files($boundary, $fields, $files)
    {
        $data      = '';
        $eol       = "\r\n";
        $delimiter = '-------------' . $boundary;
        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol . 'Content-Disposition: form-data; name="' . $name . "\"" . $eol . $eol . $content . $eol;
        }
        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol . 'Content-Transfer-Encoding: binary' . $eol;
            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--" . $eol;
        return $data;
    }
}
