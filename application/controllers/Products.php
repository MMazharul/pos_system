<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Products_model', 'PRODUCTS', TRUE);
        $this->load->model('Settings_model', 'SETTINGS', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {
        $data                = array();
        $data['productList'] = $this->COMMON_MODEL->get_data_list('tbl_pos_products', "productID", "DESC");
        $view                = array();
        $data['title']       = "Product List";
        $view['content']     = $this->load->view('dashboard/products/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function create()
    {
        $data                      = array();
        $data['productcatagories'] = $this->SETTINGS->productcatagories();
        $view                      = array();
        $data['title']             = "New Product";
        $view['content']           = $this->load->view('dashboard/products/create', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function store()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('catagoryID', 'Product Catagory', 'required');
        $this->form_validation->set_rules('productName', 'Product Name', 'required|min_length[3]|is_unique[tbl_pos_products.productName]');
        $this->form_validation->set_rules('productPrice', 'Product Sales Price', 'required|numeric');
        $this->form_validation->set_rules('productCode', 'Product Code', 'required|min_length[6]|is_unique[tbl_pos_products.productCode]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('products/create'));
        }
        if ($this->PRODUCTS->storeProduct($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product  Saved!!!</div>');
            redirect(base_url('products/index'));
        }
    }
    function edit($productID)
    {
        $data                      = array();
        $data['productcatagories'] = $this->SETTINGS->productcatagories();
        $data['products']          = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'productID', $productID);
        $view                      = array();
        $view['content']           = $this->load->view('dashboard/products/edit', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function update()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('catagoryID', 'Product Catagory', 'required');
        if ($this->input->post('productName') != $this->input->post('original-productName')) {
            $is_unique = '|is_unique[tbl_pos_products.productName]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('productName', 'Product Name', 'required|min_length[3]' . $is_unique);
        $this->form_validation->set_rules('productPrice', 'Product Sales Price', 'required|numeric');
        if ($this->input->post('productCode') != $this->input->post('original-productCode')) {
            $is_unique = '|is_unique[tbl_pos_products.productCode]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('productCode', 'Product Code', 'required|min_length[6]' . $is_unique);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('products/edit/') . $this->input->post('productID'));
        }
        if ($this->PRODUCTS->updateProduct($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product Updated!!!</div>');
            redirect(base_url('products/index'));
        }
    }
    function destroy($productID)
    {
        if ($this->PRODUCTS->destroyProduct($productID)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product Catagory Deleted!!!</div>');
            redirect(base_url('products/index'));
        }
    }
    private function productSerialize($productName, $productCode, $productPrice, $productQuantity)
    {
        foreach ($productName as $k => $name) {
            $group[] = array(
                'productName' => $name,
                'productCode' => $productCode[$k],
                'productPrice' => $productPrice[$k],
                'productQuantity' => $productQuantity[$k]
            );
        }
        return $group;
    }
    function get_suggestions()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            echo json_encode($this->PRODUCTS->productSuggestion($q));
        }
    }
    function printBarcodes()
    {
        $data = array();
        if ($this->input->post()) {
            $data['barcodes'] = $this->productSerialize($this->input->post('productName'), $this->input->post('productCode'), $this->input->post('productPrice'), $this->input->post('productQuantity'));
        }
        $view            = array();
        $data['title']   = "Print Barcode";
        $view['content'] = $this->load->view('dashboard/products/printBarcodes', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function genBarcode($productID)
    {
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $imageResource = Zend_Barcode::render('code128', 'image', array(
            'text' => $productID
        ), array());
        header("Content-Type: image/png");
        return $imageResource;
    }
    public function viewProductInfo()
    {
        extract($_POST);
        $this->db->select('SUM(quantity) as ttlbalance,tbl_pos_inventory.warehouseID');
        $this->db->from('tbl_pos_inventory');
        $this->db->group_by('warehouseID');
        $this->db->where('productID', $sendId);
        $query_results   = $this->db->get();
        $data['results'] = $query_results->result_array();
        return $this->load->view('ajax_form_data', $data);
    }
}
