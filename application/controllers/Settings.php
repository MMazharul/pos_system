<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Settings_model', 'SETTINGS', TRUE);
        $this->load->model('Cashbook_model', 'CASHBOOK', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {
    }
    function ProductCatagoryIndex()
    {
        $data                      = array();
        $data['productcatagories'] = $this->SETTINGS->productcatagories();
        $view                      = array();
        $data['title']             = "Product Cat List";
        $view['content']           = $this->load->view('dashboard/settings/productcatagories/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function ProductCatagoryCreate()
    {
        $data            = array();
        $view            = array();
        $data['title']   = "New Product Catagory";
        $view['content'] = $this->load->view('dashboard/settings/productcatagories/create', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function profile()
    {
        $data = array();
        if (isset($_POST['subtn'])) {
            extract($_POST);
            $admin_id     = $this->input->post('admin_ids');
            $old_password = md5($this->input->post('old_password'));
            $new_password = md5($this->input->post('new_password'));
            $password     = $this->db->get_where('tbl_pos_users', array(
                'userID' => $admin_id
            ))->row('password');
            if ($password == $old_password) {
                $data = array(
                    'password' => $new_password
                );
                $this->db->where('userID', $admin_id);
                $this->db->update('tbl_pos_users', $data);
                $this->session->set_flashdata('messages', 'Admin Password Updated Successfully.');
                $this->session->keep_flashdata('messages');
                redirect('settings/profile');
            } else {
                $this->session->set_flashdata('errors', 'Old Password doesnt matched! Please try again..');
                $this->session->keep_flashdata('errors');
                redirect('settings/profile');
            }
        } else {
            $view            = array();
            $view['content'] = $this->load->view('dashboard/profile', $data, TRUE);
            $this->load->view('dashboard/index', $view);
        }
    }
    function addUser()
    {
        $data = array();
        if (isPostBack()) {
            $data['username'] = $this->input->post('username');
            $data['email']    = $this->input->post('email');
            $data['password'] = md5($_POST['password']);
            $data['roleID']   = $this->input->post('roleID');
            $last_id          = $this->COMMON_MODEL->insert_data('tbl_pos_users', $data);
            if (!empty($data['roleID'])) {
                if ($data['roleID'] == 1) {
                    $datas['admin_id']        = $last_id;
                    $datas['roleName']        = 'Owner';
                    $datas['roleDescription'] = 'Owner';
                } elseif ($data['roleID'] == 2) {
                    $datas['admin_id']        = $last_id;
                    $datas['roleName']        = 'Master';
                    $datas['roleDescription'] = 'Master';
                } else {
                    $datas['admin_id']        = $last_id;
                    $datas['roleName']        = 'Sales';
                    $datas['roleDescription'] = 'Sales';
                }
                $this->COMMON_MODEL->insert_data('tbl_pos_roles', $datas);
                redirect('settings/listUser');
            }
        }
        $view            = array();
        $data['title']   = " Add User";
        $view['content'] = $this->load->view('dashboard/settings/user/addUser', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function listUser()
    {
        $data               = array();
        $data['title']      = " User List";
        $data['admin_list'] = $this->COMMON_MODEL->get_data_list('tbl_pos_users');
        $view['content']    = $this->load->view('dashboard/settings/user/listUser', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function editadmin($id)
    {
        $data = array();
        if (isPostBack()) {
            $data['username'] = $this->input->post('username');
            $data['email']    = $this->input->post('email');
            if (!empty($_POST['password'])) {
                $data['password'] = md5($_POST['password']);
            }
            $data['roleID'] = $this->input->post('roleID');
            $last_id        = $this->COMMON_MODEL->update_data('tbl_pos_users', $data, 'userID', $id);
            if (!empty($data['roleID'])) {
                if ($data['roleID'] == 1) {
                    $datas['admin_id']        = $last_id;
                    $datas['roleName']        = 'Owner';
                    $datas['roleDescription'] = 'Owner';
                } elseif ($data['roleID'] == 2) {
                    $datas['admin_id']        = $last_id;
                    $datas['roleName']        = 'Master';
                    $datas['roleDescription'] = 'Master';
                } else {
                    $datas['admin_id']        = $last_id;
                    $datas['roleName']        = 'Sales';
                    $datas['roleDescription'] = 'Sales';
                }
                $this->COMMON_MODEL->update_data('tbl_pos_roles', $datas, 'admin_id', $datas['admin_id']);
                redirect('settings/listUser');
            }
        }
        $data['title']      = " Edit User";
        $data['edit_admin'] = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_users', 'userID', $id);
        $view['content']    = $this->load->view('dashboard/settings/user/editadmin', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function deleteadmin($id)
    {
        $this->COMMON_MODEL->delete_data('tbl_pos_users', 'userID', $id);
        $this->COMMON_MODEL->delete_data('tbl_pos_roles', 'admin_id', $id);
        redirect('settings/listUser');
    }
    function ProductCatagoryStore()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('catagoryName', 'Product Catagory', 'required|min_length[3]|is_unique[tbl_pos_product_catagories.catagoryName]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('settings/ProductCatagoryCreate'));
        }
        if ($this->SETTINGS->storeProductCatagory($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product Catagory Saved!!!</div>');
            redirect(base_url('settings/ProductCatagoryIndex'));
        }
    }
    function ProductCatagoryShow()
    {
    }
    function ProductCatagoryEdit($product_catagoriesID)
    {
        $data                      = array();
        $data['productcatagories'] = $this->SETTINGS->productcatagories($product_catagoriesID);
        $view                      = array();
        $data['title']             = "Product Cat Edit";
        $view['content']           = $this->load->view('dashboard/settings/productcatagories/edit', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function ProductCatagoryUpdate()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        if ($this->input->post('catagoryName') != $this->input->post('original-catagoryName')) {
            $is_unique = '|is_unique[tbl_pos_product_catagories.catagoryName]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('catagoryName', 'Product Catagory', 'required|min_length[3]' . $is_unique);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('settings/ProductCatagoryEdit/') . $this->input->post('product_catagoriesID'));
        }
        if ($this->SETTINGS->updateProductCatagory($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product Catagory Updated!!!</div>');
            redirect(base_url('settings/ProductCatagoryIndex'));
        }
    }
    function ProductCatagoryDestroy($product_catagoriesID)
    {
        $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_products', 'catagoryID', $product_catagoriesID);
        $affRow = $this->db->affected_rows();
        if ($affRow > 0) {
            $this->session->set_flashdata('usingTransaction', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Sorry, Product Category is linked Product Information!</div>');
            redirect('settings/ProductCatagoryIndex');
        } else {
            if ($this->SETTINGS->destroyProductCatagory($product_catagoriesID)) {
                $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Product Catagory Deleted!!!</div>');
                redirect(base_url('settings/ProductCatagoryIndex'));
            }
        }
    }
    function WarehouseIndex()
    {
        $data               = array();
        $data['warehouses'] = $this->SETTINGS->warehouses();
        $view               = array();
        $view['content']    = $this->load->view('dashboard/settings/warehouses/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function WarehouseCreate()
    {
        $data            = array();
        $view            = array();
        $view['content'] = $this->load->view('dashboard/settings/warehouses/create', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function WarehouseStore()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('warehouseName', 'Warehouse Name', 'required|min_length[4]|is_unique[tbl_pos_warehouses.warehouseName]');
        $this->form_validation->set_rules('warehousePhone', 'Warehouse Phone Number', 'required|min_length[7]');
        $this->form_validation->set_rules('warehouseAddress', 'Warehouse Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('settings/WarehouseCreate'));
        }
        if ($this->SETTINGS->storeWarehouse($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Warehouse Saved!!!</div>');
            redirect(base_url('settings/WarehouseIndex'));
        }
    }
    function WarehouseShow()
    {
    }
    function WarehouseEdit($warehouseID)
    {
        $data               = array();
        $data['warehouses'] = $this->SETTINGS->warehouses($warehouseID);
        $view               = array();
        $view['content']    = $this->load->view('dashboard/settings/warehouses/edit', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function WarehouseUpdate()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        if ($this->input->post('warehouseName') != $this->input->post('original-warehouseName')) {
            $is_unique = '|is_unique[tbl_pos_warehouses.warehouseName]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('warehouseName', 'Warehouse Name', 'required|min_length[4]' . $is_unique);
        $this->form_validation->set_rules('warehousePhone', 'Warehouse Phone Number', 'required|min_length[7]');
        $this->form_validation->set_rules('warehouseAddress', 'Warehouse Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('settings/WarehouseEdit/') . $this->input->post('warehouseID'));
        }
        if ($this->SETTINGS->updateWarehouse($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Warehouse Updated!!!</div>');
            redirect(base_url('settings/WarehouseIndex'));
        }
    }
    function WarehouseDestroy($warehouseID)
    {
        if ($this->SETTINGS->destroyWarehouse($warehouseID)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Warehouse Deleted!!!</div>');
            redirect(base_url('settings/WarehouseIndex'));
        }
    }
    function AppConfigIndex()
    {
        $data            = array();
        $data['config']  = $this->SETTINGS->config();
        $view            = array();
        $data['title']   = "App Config";
        $view['content'] = $this->load->view('dashboard/settings/config/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function updateStoreVatRate()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('vatRate', 'Vat Rate', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('settings/AppConfigIndex'));
        }
        if ($this->SETTINGS->updateVatRate($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Vat Rate Updated!!!</div>');
            redirect(base_url('settings/AppConfigIndex'));
        }
    }
    function PosConfigIndex()
    {
        $data               = array();
        $data['config']     = $this->SETTINGS->posConfig();
        $data['warehouses'] = $this->SETTINGS->warehouses();
        $data['accounts']   = $this->CASHBOOK->accounts();
        $view               = array();
        $view['content']    = $this->load->view('dashboard/settings/pos-config/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function PosConfigUpdate()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('warehouseID', 'Warehouse', 'required');
        $this->form_validation->set_rules('accountID', 'Account', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('settings/PosConfigIndex'));
        }
        if ($this->SETTINGS->updatePosConfig($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Pos Config Updated!!!</div>');
            redirect(base_url('settings/PosConfigIndex'));
        }
    }
}
