<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
    public $defaults = array();
    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model', 'LOGIN', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {

        $user = $this->session->userdata('user');
        if (!empty($user)) {
            redirect(site_url('welcome'));
        }
        $this->load->view('auth/login');
    }
    function checkLogin()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">', '</div>');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('login'));
        }
        if ($this->LOGIN->checkUser($this->input->post()))
         {
            $user = $this->LOGIN->checkUser($this->input->post());
            $this->session->set_userdata('user', $user->roleID);
            $this->session->set_userdata('user_id', $user->userID);
            $this->session->set_userdata('user_name', $user->username);
            redirect("welcome", "location");
        } else {
            $this->session->set_flashdata('msg', '<div style="color: red ; text-align: center;font-weight:bold;padding-bottom: 5px;">Invalid User!!!</div>');
            redirect("login", "location");
        }
    }
    function logOut()
    {
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_name');
        $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Successfully Logout</div>');
        redirect("login", "location");
    }
}
