<?php defined('BASEPATH') or exit('No direct script access allowed');
class Purchases extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $user = $this->session->userdata('user');
    if (empty($user)) {
      $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
      redirect(base_url('login'));
    }
    $this->load->model('Purchases_model', 'PURCHASE', TRUE);
    $this->load->model('Products_model', 'PRODUCT', TRUE);
    $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    $this->load->model('Settings_model', 'SETTINGS', TRUE);
  }
  function index()
  {
    $data = array();
    $data['purchases'] = $this->PURCHASE->purchases();
    $view = array();
    $data['title'] = "Purchase List";
    $view['content'] = $this->load->view('dashboard/purchases/index', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function create()
  {
    $data = array();
    $view = array();
    $data['title'] = "New Purchase";
    $view['content'] = $this->load->view('dashboard/purchases/create', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function store()
  {
    $this->form_validation->set_error_delimiters('<div style="font-weight:bold;display: inline-block;padding-right:5px;">', '</div>');
    $this->form_validation->set_rules('supplierID', 'Supplier Name', 'required');
    $this->form_validation->set_rules('purchaseDate', 'Purchase Date', 'required');
    $this->form_validation->set_rules('dueDate', 'Due Date', 'required');
    $this->form_validation->set_rules('warehouseID', 'Warehouse Name', 'required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('msg', validation_errors());
      redirect(base_url('purchases/create'));
    }
    if (empty($this->input->post('productID')[0]) && empty($this->input->post('quantity')[0]) && empty($this->input->post('purchasePrice')[0])) {
      $this->session->set_flashdata('msg', '<div style="font-weight:bold;display: inline-block;padding-right:5px;">Product Name, Quantity, Purchase Price should not be empty.</div> ');
      redirect(base_url('purchases/create'));
    }
    $purchaseNo = $this->PURCHASE->store($this->input->post());
    if ($purchaseNo) {
      $purchaseProducts = $this->serializePurchasedProduct($purchaseNo, $this->input->post('productID'), $this->input->post('quantity'), $this->input->post('purchasePrice'));
      foreach ($purchaseProducts as $purchaseProduct) {
        $this->PURCHASE->storeProduct($purchaseProduct);
        $this->PURCHASE->storeInventory($purchaseProduct, $this->input->post('warehouseID'), $this->input->post('purchaseDate'));
      }
      redirect(base_url('purchases/show/') . $purchaseNo);
    }
  }
  function show($purchaseNo)
  {
    $data = array();
    $data['purchases'] = $this->PURCHASE->purchases($purchaseNo);
    $view = array();
    $view['content'] = $this->load->view('dashboard/purchases/show', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function pur_edit($purchaseNo)
  {
    $data = array();
    $data['edit_info'] = $this->COMMON_MODEL->get_single_data_by_single_column3('tbl_pos_purchase_products', 'purchaseNo', $purchaseNo);
    $data['edit_info1'] = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_purchases', 'purchaseNo', $purchaseNo);
    $view = array();
    $view['content'] = $this->load->view('dashboard/purchases/pur_edit', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  public function viewPurchaseInfo()
  {
    extract($_POST);
    $this->db->select('*');
    $this->db->from('tbl_pos_purchases');
    $this->db->where('purchaseID', $sendId);
    $query_results = $this->db->get();
    $data['results'] = $query_results->row_array();
    return $this->load->view('ajax_form_dataes', $data);
  }
  function update()
  {
    $data['supplierID'] = $this->input->post('supplierID');
    $data['purchaseNo'] = $this->input->post('purchaseNo');
    $data['poRef'] = $this->input->post('poRef');
    $data['payTerms'] = $this->input->post('payTerms');
    $data['purchaseDate'] = $this->input->post('purchaseDate');
    $data['dueDate'] = $this->input->post('dueDate');
    $data['warehouseID'] = $this->input->post('warehouseID');
    $data['subTotal'] = $this->input->post('subTotal');
    $data['note'] = $this->input->post('note');
    $data['discount'] = $this->input->post('discount');
    $data['netTotal'] = $this->input->post('netTotal');
    $data['dueAmount'] = $this->input->post('dueAmount');
    $purNo = $data['purchaseNo'];
    $wid = $data['warehouseID'];
    $this->COMMON_MODEL->update_data('tbl_pos_purchases', $data, 'purchaseNo', $purNo);
    $this->COMMON_MODEL->delete_data('tbl_pos_purchase_products', 'purchaseNo', $purNo);
    extract($_POST);
    $fildCnt = count($quantity);
    if ($fildCnt >= 1) {
      for ($i = 0; $i < $fildCnt; $i++) {
        $datas = array('productID' => $productID[$i], 'quantity' => $quantity[$i], 'purchasePrice' => $purchasePrice[$i], 'purchaseNo' => $purNo);
        $this->COMMON_MODEL->insert_data('tbl_pos_purchase_products', $datas);
      }
    }
    $this->COMMON_MODEL->delete_data('tbl_pos_inventory', 'refNo', $purNo);
    extract($_POST);
    $fildCnt = count($quantity);
    if ($fildCnt >= 1) {
      for ($i = 0; $i < $fildCnt; $i++) {
        $datass = array('productID' => $productID[$i], 'quantity' => $quantity[$i], 'refNo' => $purNo, 'warehouseID' => $wid, 'type' => 'IN', 'date' => $data['purchaseDate'], 'timestamp' => date("Y-m-d h:i:sa"));
        $this->COMMON_MODEL->insert_data('tbl_pos_inventory', $datass);
      }
    }
    redirect(base_url('purchases/index'));
  }
  function destroy($purchaseNo)
  {
    $this->COMMON_MODEL->delete_data('tbl_pos_purchase_products', 'purchaseNo', $purchaseNo);
    $this->COMMON_MODEL->delete_data('tbl_pos_inventory', 'refNo', $purchaseNo);
    $this->COMMON_MODEL->delete_data('tbl_pos_purchases', 'purchaseNo', $purchaseNo);
    redirect(base_url('purchases/index'));
  }
  function duePurchases()
  {
    $data['account'] = $this->SETTINGS->account();
    $data['purchases'] = $this->PURCHASE->purchases('', 0);
    $view = array();
    $data['title'] = "Unpaid Purchase";
    $view['content'] = $this->load->view('dashboard/purchases/duePurchases', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  function pay()
  {
    if (isset($_POST['submitBtn'])) {
      $data['transactionAccountID'] = $this->input->post('account_id');
      $transactionAmount = $this->input->post('payamount');
      $data['transactionAmount'] = '-' . $transactionAmount;
      $data['transactionType'] = 'DUE PAID';
      $data['created_at'] = date("Y-m-d h:i:sa");
      $data['date'] = date("Y-m-d");
      $this->COMMON_MODEL->insert_data('tbl_pos_transactions', $data);
      $purchaseid = $this->input->post('purchaseid');
      $hdndueamount = $this->input->post('hdndueamount');
      $dueAmounts = $this->input->post('payamount');
      $datas['dueAmount'] = $hdndueamount - $dueAmounts;
      $this->COMMON_MODEL->update_data('tbl_pos_purchases', $datas, 'purchaseNo', $purchaseid);
      redirect('purchases/duePurchases');
    }
    $dueDetail = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_purchases', 'purchaseNo', $purchaseNo);
    $view['content'] = $this->load->view('dashboard/purchases/duePurchases', $data, TRUE);
    $this->load->view('dashboard/index', $view);
  }
  private function serializePurchasedProduct($purchaseNo, $productID, $quantity, $purchasePrice)
  {
    $group = array();
    foreach ($productID as $k => $name) {
      if (is_numeric($quantity[$k]) && is_numeric($purchasePrice[$k])) {
        $group[] = array('purchaseNo' => $purchaseNo, 'productID' => $name, 'quantity' => $quantity[$k], 'purchasePrice' => $purchasePrice[$k]);
      }
    }
    return $group;
  }
  function supplierNameSuggestions()
  {
    if (isset($_GET['term'])) {
      $q = strtolower($_GET['term']);
      echo json_encode($this->PURCHASE->supplierSuggestion($q));
    }
  }
  function warehouseNameSuggestions()
  {
    if (isset($_GET['term'])) {
      $q = strtolower($_GET['term']);
      echo json_encode($this->PURCHASE->warehouseSuggestion($q));
    }
  }
  function productNameSuggestions()
  {
    if (isset($_GET['term'])) {
      $q = strtolower($_GET['term']);
      echo json_encode($this->PRODUCT->productSuggestion($q));
    }
  }
}
