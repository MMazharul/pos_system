<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Suppliers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Purchases_model', 'PURCHASE', TRUE);
        $this->load->model('Settings_model', 'SETTINGS', TRUE);
        $this->load->model('Suppliers_model', 'SUPPLIERS', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {
        $data              = array();
        $data['suppliers'] = $this->SUPPLIERS->suppliers();
        $view              = array();
        $data['title']     = "Supplier List";
        $view['content']   = $this->load->view('dashboard/suppliers/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function insert_payment()
    {
        if (isset($_POST['submitBtn'])) {
            extract($_POST);
            if ($remain_due_amount != '') {
                $presentDue = $remain_due_amount;
            } else {
                $presentDue = "-" . $advanced_amount;
            }
            $data['supplier_id']           = $this->input->post('supplier');
            $data['date']                  = $this->input->post('paymentDate');
            $data['amount']                = $this->input->post('payamount');
            $data['totalDueAmount']        = $this->input->post('due_amount');
            $data['presentDue']            = $presentDue;
            $data['transactionAccountID']  = $this->input->post('account_id');
            $data['note']                  = $this->input->post('note');
            $insert_id                     = $this->COMMON_MODEL->insert_data('tbl_pos_supplier_payment', $data);
            $datas['suplier_pay_id']       = $insert_id;
            $datas['transactionAccountID'] = $this->input->post('account_id');
            $datas['transactionAmount']    = '-' . $data['amount'];
            $datas['transactionType']      = 'SUPPLIER PAYMENT';
            $datas['created_at']           = date("Y-m-d h:i:sa");
            $datas['date']                 = $this->input->post('paymentDate');
            $this->db->insert('tbl_pos_transactions', $datas);
            if ($insert_id) {
                redirect('Suppliers/supplierPaymentDetailsInfo/' . $insert_id, "location");
            }
        }
    }
    function supplier_payment_summary($supplier_id = null)
    {
        $data['account']     = $this->SETTINGS->account();
        $data['title']       = "Supplier Payment Summary";
        $data['supplier_id'] = $supplier_id;
        $view['content']     = $this->load->view('dashboard/suppliers/payment_summary', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function supplierPaymentDetailsInfo()
    {
        $paymentId            = $this->uri->segment('3');
        $data['paymentInfo']  = $this->COMMON_MODEL->get_single_data_by_single_column("tbl_pos_supplier_payment", "payment_id", $paymentId);
        $data['supplierInfo'] = $this->COMMON_MODEL->get_single_data_by_single_column("tbl_pos_suppliers", "supplierID", $data['paymentInfo']['supplier_id']);
        $data['title']        = "Supplier Payment Summary";
        $view['content']      = $this->load->view('dashboard/suppliers/supplierPaymentDetailsInfo', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function supplierPayment()
    {
        $data['account']            = $this->SETTINGS->account();
        $data['purchases']          = $this->PURCHASE->purchases('', 0);
        $data['suppliers_all_dues'] = $this->SUPPLIERS->allDue();
        $data['title']              = "Supplier Payment List";
        $view['content']            = $this->load->view('dashboard/suppliers/payment_list', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function supplierPayment_balance()
    {
        $supplier_id = $this->input->post('supplier_id');
        if (!empty($supplier_id)) {
            $due         = $this->SUPPLIERS->allDue_by_id($supplier_id);
            $payment     = $this->SUPPLIERS->allpayment_by_id($supplier_id);
            $last_amount = $due - $payment;
            echo $last_amount;
        }
    }
    function create()
    {
        $data            = array();
        $view            = array();
        $data['title']   = "New Supplier";
        $view['content'] = $this->load->view('dashboard/suppliers/create', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function store()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('supplierName', 'Supplier Name', 'required|min_length[4]|is_unique[tbl_pos_suppliers.supplierName]');
        $this->form_validation->set_rules('supplierPhone', 'Supplier Phone Number', 'required|min_length[7]|is_unique[tbl_pos_suppliers.supplierPhone]');
        $this->form_validation->set_rules('supplierAddress', 'Supplier Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('suppliers/create'));
        }
        if ($this->SUPPLIERS->store($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Supplier Saved!!!</div>');
            redirect(base_url('suppliers/index'));
        }
    }
    function show()
    {
    }
    function edit($supplierID)
    {
        $data              = array();
        $data['suppliers'] = $this->SUPPLIERS->suppliers($supplierID);
        $view              = array();
        $view['content']   = $this->load->view('dashboard/suppliers/edit', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function update()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        if ($this->input->post('supplierName') != $this->input->post('original-supplierName')) {
            $is_unique = '|is_unique[tbl_pos_suppliers.supplierName]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('supplierName', 'Supplier Name', 'required|min_length[4]' . $is_unique);
        if ($this->input->post('supplierEmail') != $this->input->post('original-supplierEmail')) {
            $is_unique = '|is_unique[tbl_pos_suppliers.supplierEmail]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('supplierEmail', 'Supplier Email', 'required|valid_email' . $is_unique);
        if ($this->input->post('supplierPhone') != $this->input->post('original-supplierPhone')) {
            $is_unique = '|is_unique[tbl_pos_suppliers.supplierPhone]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('supplierPhone', 'Supplier Phone Number', 'required|min_length[7]' . $is_unique);
        $this->form_validation->set_rules('supplierAddress', 'Supplier Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('suppliers/edit/') . $this->input->post('supplierID'));
        }
        if ($this->SUPPLIERS->update($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Supplier Updated!!!</div>');
            redirect(base_url('suppliers/index'));
        }
    }
    function destroy($supplierID)
    {
        $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_purchases', 'supplierID', $supplierID);
        $affRow = $this->db->affected_rows();
        $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_supplier_payment', 'supplier_id', $supplierID);
        $affRowPayment = $this->db->affected_rows();
        if ($affRow > 0) {
            $this->session->set_flashdata('usingPurchase', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Sorry, This suppplier is linked product purchase!</div>');
            redirect(base_url('suppliers/index'));
        } else if ($affRowPayment > 0) {
            $this->session->set_flashdata('usingTransaction', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Sorry, This suppplier is linked product purchase payment!</div>');
            redirect(base_url('suppliers/index'));
        } else {
            if ($this->SUPPLIERS->destroy($supplierID)) {
                $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Successfully!! Supplier Deleted!!!</div>');
                redirect(base_url('suppliers/index'));
            }
        }
    }
}
