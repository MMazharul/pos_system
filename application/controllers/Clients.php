<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Clients extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Clients_model', 'CLIENTS', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {
        $data = array();
        $data['clients'] = $this->CLIENTS->clients();
        $view  = array();
        $view['content'] = $this->load->view('dashboard/clients/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function create()
    {
        $data = array();
        $view = array();
        $view['content'] = $this->load->view('dashboard/clients/create', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function store()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        $this->form_validation->set_rules('clientName', 'Client Name', 'required|min_length[4]|is_unique[tbl_pos_clients.clientName]');
        $this->form_validation->set_rules('clientEmail', 'Client Email', 'required|valid_email|is_unique[tbl_pos_clients.clientEmail]');
        $this->form_validation->set_rules('clientPhone', 'Client Phone Number', 'required|min_length[7]|is_unique[tbl_pos_clients.clientPhone]');
        $this->form_validation->set_rules('clientAddress', 'Client Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('clients/create'));
        }
        if ($this->CLIENTS->store($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Client Saved!!!</div>');
            redirect(base_url('clients/index'));
        }
    }
    function show()
    {
    }
    function edit($clientID)
    {
        $data = array();
        $data['clients'] = $this->CLIENTS->clients($clientID);
        $view = array();
        $view['content'] = $this->load->view('dashboard/clients/edit', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function update()
    {
        $this->form_validation->set_error_delimiters('<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">', '</div>');
        if ($this->input->post('clientName') != $this->input->post('original-clientName')) {
            $is_unique = '|is_unique[tbl_pos_clients.clientName]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('clientName', 'Client Name', 'required|min_length[4]' . $is_unique);
        if ($this->input->post('clientEmail') != $this->input->post('original-clientEmail')) {
            $is_unique = '|is_unique[tbl_pos_clients.clientEmail]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('clientEmail', 'Client Email', 'required|valid_email' . $is_unique);
        if ($this->input->post('clientPhone') != $this->input->post('original-clientPhone')) {
            $is_unique = '|is_unique[tbl_pos_clients.clientPhone]';
        } else {
            $is_unique = '';
        }
        $this->form_validation->set_rules('clientPhone', 'Client Phone Number', 'required|min_length[7]' . $is_unique);
        $this->form_validation->set_rules('clientAddress', 'Client Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('clients/edit/') . $this->input->post('clientID'));
        }
        if ($this->CLIENTS->update($this->input->post())) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Client Updated!!!</div>');
            redirect(base_url('clients/index'));
        }
    }
    function destroy($clientID)
    {
        if ($this->CLIENTS->destroy($clientID)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Client Deleted!!!</div>');
            redirect(base_url('clients/index'));
        }
    }
}
