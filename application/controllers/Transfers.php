<?php defined('BASEPATH') or exit('No direct script access allowed');
class Transfers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user))
        {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Transfers_model', 'TRANSFERS', true);
        $this->load->model('Settings_model', 'SETTINGS', true);
        $this->load->model('Inventory_model', 'INVENTORY', true);
        $this->load->model('Common_model', 'COMMON_MODEL', true);
    }
    function index()
    {
      
        $data = array();
        $data['transfers'] = $this->TRANSFERS->transfers();
        $view = array();
        $view['content'] = $this->load->view('dashboard/transfers/index', $data, true);
        $this->load->view('dashboard/index', $view);
    }
    function create()
    {
        $data = array();
        $data['warehouses'] = $this->SETTINGS->warehouses();
        $view = array();
        $view['content'] = $this->load->view('dashboard/transfers/create', $data, true);
        $this->load->view('dashboard/index', $view);
    }
    function store()
    {
        $this->form_validation->set_error_delimiters('<div style="font-weight:bold;display: inline-block;padding-right:5px;">', '</div>');
        $this->form_validation->set_rules('selectedFromWareHouse', 'From Warehouse', 'required');
        $this->form_validation->set_rules('selectedToWareHouse', 'To Warehouse', 'required');
        $this->form_validation->set_rules('transferDate', 'Transfer Date', 'required');
        $this->form_validation->set_rules('transferNo', 'Transfer No', 'required');
        if ($this->form_validation->run() == false)
        {
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('transfers/create'));
        }
        if (empty($this->input->post('productID') [0]) && empty($this->input->post('quantity') [0]))
        {
            $this->session->set_flashdata('msg', '<div style="font-weight:bold;display: inline-block;padding-right:5px;">Product Name, Quantity should not be empty.</div> ');
            redirect(base_url('transfers/create'));
        }
        $transferProduct = $this->serializeTransferProduct($this->input->post('transferNo') , $this->input->post('productID') , $this->input->post('quantity'));
        if ($this->TRANSFERS->store($this->input->post()))
        {
            foreach ($transferProduct as $transferPro)
            {
                $this->TRANSFERS->storeTransferProduct($transferPro);
                $this->TRANSFERS->storeTransferedInventory($transferPro, $this->input->post('selectedFromWareHouse') , $this->input->post('selectedToWareHouse'));
            }
            redirect(base_url('transfers/show/') . $this->input->post('transferNo'));
        }
    }
    function show($transferNo)
    {
        $data = array();
        $data['transfers'] = $this->TRANSFERS->transfers($transferNo);
        $view = array();
        $view['content'] = $this->load->view('dashboard/transfers/show', $data, true);
        $this->load->view('dashboard/index', $view);
    }
    function tr_edit($transferNo)
    {
        $data = array();
        $data['warehouses'] = $this->SETTINGS->warehouses();
        $data['edit_info'] = $this->COMMON_MODEL->get_single_data_by_single_column('tbl_pos_transfers', 'transferNo', $transferNo);
        $data['edit_infos'] = $this->COMMON_MODEL->get_single_data_by_single_columns('tbl_pos_transfer_products', 'transferNo', $transferNo);
        $view = array();
        $view['content'] = $this->load->view('dashboard/transfers/tr_edit', $data, true);
        $this->load->view('dashboard/index', $view);
    }
    function update()
    {
        $data['fromWarehouseID'] = $this->input->post('fromwhouse');
        $data['toWarehouseID'] = $this->input->post('towahouse');
        $data['transferDate'] = $this->input->post('transferDate');
        $data['transferNo'] = $this->input->post('transferNo');
        $data['note'] = $this->input->post('note');
        $this->COMMON_MODEL->update_data('tbl_pos_transfers', $data, 'transferNo', $data['transferNo']);
        $this->COMMON_MODEL->delete_data('tbl_pos_transfer_products', 'transferNo', $data['transferNo']);
        extract($_POST);
        $fildCnt = count($productID);
        if ($fildCnt >= 1)
        {
            for ($i = 0;$i < $fildCnt;$i++)
            {
                $datas = array(
                    'productID' => $productID[$i],
                    'quantity' => $quantity[$i],
                    'transferNo' => $data['transferNo']
                );
                $this
                    ->COMMON_MODEL
                    ->insert_data('tbl_pos_transfer_products', $datas);
            }
        }
        $this->COMMON_MODEL->delete_data('tbl_pos_inventory', 'refNo', $data['transferNo']);
        extract($_POST);
        $fildCnt = count($productID);
        if ($fildCnt >= 1)
        {
            for ($i = 0;$i < $fildCnt;$i++)
            {
                $datas = array(
                    'productID' => $productID[$i],
                    'quantity' => $quantity[$i],
                    'refNo' => $data['transferNo'],
                    'warehouseID' => $data['toWarehouseID'],
                    'type' => 'TRANSFER-IN'
                );
                $this
                    ->COMMON_MODEL
                    ->insert_data('tbl_pos_inventory', $datas);
            }
        }
        redirect(base_url('transfers/index'));
    }
    function destroy($transferNo)
    {
        $this->COMMON_MODEL->delete_data('tbl_pos_transfer_products', 'transferNo', $transferNo);
        $this->COMMON_MODEL->delete_data('tbl_pos_transfers', 'transferNo', $transferNo);
        $this->COMMON_MODEL->delete_data('tbl_pos_inventory', 'refNo', $transferNo);
        redirect(base_url('transfers/index'));
    }
    function askWarehouses()
    {
        echo json_encode($this->SETTINGS->warehouses());
    }
    function askInventory()
    {
        if (!empty($this
            ->input
            ->post('productID')))
        {
            echo json_encode($this->INVENTORY->inventory($this
                ->input->post('warehouse') , $this->input->post('productID')));
        }
        else
        {
            echo json_encode($this->INVENTORY->inventory($this->input->post('warehouse')));
        }
    }
    function serializeTransferProduct($transferNo, $productID, $quantity)
    {
        $group = array();
        foreach ($productID as $k => $name)
        {
            if (is_numeric($quantity[$k]))
            {
                $group[] = array(
                    'productID' => $name,
                    'transferNo' => $transferNo,
                    'quantity' => $quantity[$k],
                );
            }
        }
        return $group;
    }
}
