<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inventory extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;">Please Login!!!</div>');
            redirect(base_url('login'));
        }
        $this->load->model('Inventory_model', 'INVENTORY', TRUE);
        $this->load->model('Settings_model', 'SETTINGS', TRUE);
        $this->load->model('Products_model', 'PRODUCTS', TRUE);
        $this->load->model('Common_model', 'COMMON_MODEL', TRUE);
    }
    function index()
    {

        $data = array();
        $data['inventory'] = $this->INVENTORY->inventory();

        $view = array();
        $data['title'] = "Inventory List";
        $view['content']  = $this->load->view('dashboard/inventory/index', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function serarchInventroy()
    {
        $data                         = array();
        $data['warehouseID']          = $this->input->post('warehouseID');
        $data['product_catagoriesID'] = $this->input->post('product_catagoriesID');
        $data['productID']            = $this->input->post('productID');
        if (!empty($data['warehouseID']) && empty($data['productID']) && empty($data['product_catagoriesID'])) {
            $data['inventroyseracrdatabyhouse'] = $this->INVENTORY->searchdataHistory1($data['warehouseID']);
        } elseif (!empty($data['productID'])) {
            $data['inventroyseracrdatabyhouse'] = $this->INVENTORY->searchdataHistory2($data['productID']);
        } elseif (!empty($data['product_catagoriesID'])) {
            $data['inventroyseracrdatabyhouse'] = $this->INVENTORY->searchdataHistory3($data['product_catagoriesID']);
        } else {
            $data['inventroyseracrdatabyhouse'] = $this->INVENTORY->searchdataHistoryall($data['warehouseID'], $data['productID']);
        }
        $view            = array();
        $data['title']   = "Report || Search Inventory";
        $view['content'] = $this->load->view('dashboard/inventory/search_result', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function adjustment()
    {
        $data               = array();
        $data['warehouses'] = $this->SETTINGS->warehouses();
        $data['products']   = $this->PRODUCTS->products();
        $view               = array();
        $data['title']      = "Inventory Adjustment";
        $view['content']    = $this->load->view('dashboard/inventory/adjustmentForm', $data, TRUE);
        $this->load->view('dashboard/index', $view);
    }
    function adjustmentUpdate()
    {
        echo "<pre>";
        print_r($this->input->post());
        if ($this->input->post('type') == 'DAMAGE-OUT') {
            if ($this->INVENTORY->checkProductExistInWarehouse($this->input->post()) == 0) {
                $this->session->set_flashdata('msg', '<div style="text-align: center;font-weight:bold;padding-bottom: 5px;padding-top:10px;">Please Add Some Product For Deduction!!!</div>');
                redirect(base_url('inventory/adjustment'));
            }
            if ($this->INVENTORY->adjustProduct($this->input->post())) {
                redirect(base_url('inventory/index'));
            }
        } elseif ($this->input->post('type') == 'IN') {
            if ($this->INVENTORY->adjustProductin($this->input->post())) {
                redirect(base_url('inventory/index'));
            }
        }
    }
}
